#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu
#SBATCH --account=merlin
#SBATCH --gpus=1
#SBATCH --constraint=gpumem_11gb
#SBATCH --job-name=Train$1
#SBATCH --time=24:00:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=5
#SBATCH --mem-per-cpu=10G
#SBATCH --exclude=merlin-g-0[01-10]
#SBATCH --output=scratch/models/logs/$1_output.log
#SBATCH --error=scratch/models/logs/$1_error.log

module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun --cpus-per-task=\$SLURM_CPUS_PER_TASK python src/models/train_model.py wandb.setup.job_type=train wandb.setup.name=$1 dataset=$2 model=stcn_deep paths.base=null dataset.train_labels=$3
EOT
