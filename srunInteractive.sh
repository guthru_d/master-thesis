#!/bin/bash
s <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu-short
#SBATCH --gpus=1
#SBATCH --constraint=gpumem_11gb
#SBATCH --job-name=$1$2
#SBATCH --time=02:00:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=8
#SBATCH --nodelist=merlin-g-014
#SBATCH --mem-per-cpu=4G
#SBATCH --pty bash -i

module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate

EOT
srun --cluster=gmerlin6 --partition=gpu-short --gpus=1 --constraint=gpumem_11gb --job-name=interactivejob --time=02:00:00 --hint=multithread --cpus-per-task=4 --mem-per-cpu=8G --pty bash -i