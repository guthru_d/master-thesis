import os
import ast
import shutil
import wandb
import torch
import argparse
from datetime import datetime
from config import *
from variables import *
import yaml
import time
import datetime
from pathlib import Path
import pprint
import omegaconf
import hydra
import bz2
import json
import pickle
import _pickle as cPickle
def asLiteral(value):
    try:
        return ast.literal_eval(value)
    except:
        return value

def makeFolderMaybe(folderPath):
    if not os.path.isdir(folderPath):
        os.mkdir(folderPath)

def replaceFolder(folderPath):
    if os.path.isdir(folderPath):
        shutil.rmtree(folderPath)	
    makeFolderMaybeRecursive(folderPath)    

def makeFolderMaybeRecursive(folderPath, maxDepth=3):
    if not os.path.isdir(folderPath):
        if maxDepth > 0:
            makeFolderMaybeRecursive(os.path.split(folderPath)[0], maxDepth - 1)
            makeFolderMaybe(folderPath)
        else:
            raise RecursionError
        
def getCurrentTime():
    return datetime. now(). strftime("%Y_%m_%d-%I:%M:%S_%p")
     
def makeSessionFolder(args):
    ARCHITECTURE_DIR=os.path.join(MODEL_DIR,args.model)
    args.id = wandb.util.generate_id()
    if args.mode==RESUME and args.mode == EVALUATE:
        SESSION_DIR = os.path.join(ARCHITECTURE_DIR,f'{args.folderName}')        
    else:    
        if args.folderName:
            SESSION_DIR = os.path.join(ARCHITECTURE_DIR,f'{args.folderName}')
            if args.folderName == 'tmp':
                replaceFolder(SESSION_DIR)
        else:
            SESSION_DIR = os.path.join(ARCHITECTURE_DIR,str(args.id))
            args.folderName=str(args.id)
        if args.mode==FINETUNING:
            SESSION_DIR=os.path.join(ARCHITECTURE_DIR,args.folderName+args.mode+f"{args.id}")  
            # SESSION_DIR=os.path.join(SESSION_DIRargs.mode)
        if os.path.isdir(SESSION_DIR):
            assert(f"Already existing folder: {SESSION_DIR}")
        makeFolderMaybeRecursive(SESSION_DIR)
    args.SESSION_DIR = SESSION_DIR
    return args

def yaml_loader(filepath):
    #Loads a yaml file
    with open(filepath,'r') as file_descriptor:
        data = yaml.safe_load(file_descriptor)
    return data
def updateConfig(args,configDict=None):

    defaultDict = configDict if configDict is not None else yaml_loader(DEFAULT_PARAMETER_DICT)
    effectiveDict = yaml_loader(os.path.join(MODEL_CONFIG_DIR,f"{args.config}.yaml"))
    defaultDict.update(effectiveDict)
    mergedDict = {}
    for key in defaultDict.keys():
        if isinstance(defaultDict[key],dict):
            mergedDict[key] = defaultDict[key]["value"]
        else:
            mergedDict[key] = defaultDict[key]
    # mergedDict['maxEpochs']=mergedDict['maxEpochs']//mergedDict['train'][1]
    
    
    return mergedDict
    
def initiateCacheDir(args):
    if args.mode==SWEEP:
        persistentCache = Path(CACHE_DIR, args.folderName+str(time.time()))
    else:
        persistentCache = Path(CACHE_DIR, args.folderName)
    if os.path.isdir(persistentCache):
        shutil.rmtree(persistentCache)
    persistentCache.mkdir(parents=True, exist_ok=True)
    return persistentCache
    
def initiateWandb(args):
    args.LAST_MODEL_PATH = os.path.join(args.SESSION_DIR,'last.pt')
    args.BEST_MODEL_PATH = os.path.join(args.SESSION_DIR,'best.pt')
    if args.mode==RESUME or args.mode==EVALUATE:
        print(f"Model: {args.model} with weights {args.checkpoint}")
        configDict = torch.load(args.LAST_MODEL_PATH)['configDict'] if args.checkpoint==LAST else  torch.load(args.BEST_MODEL_PATH)['configDict']
        args.id = configDict['id']
        if args.overwrite:
            print(f"Overwrite old YAML with {args.config}.yaml")
            configDict = updateConfig(args,configDict)
    elif args.mode==FINETUNING:
        configDict = torch.load(args.BEST_MODEL_PATH.replace(f"{args.mode}","").replace(f"{args.id}",""))['configDict']
        configDict = updateConfig(args,configDict)
    else:
        configDict = updateConfig(args)
    configDict['persistentCache']=initiateCacheDir(args)
    wandbRun = wandb.init(config=configDict,
        id = args.id,
        allow_val_change = True,
        anonymous = "allow",
        resume = "must" if args.mode==RESUME or args.mode==EVALUATE else False,
        dir = WANDB_DIR,
        name=args.folderName,
        )
    wandbRun.config.update({key:vars(args)[key] for key in [*vars(args).keys()] if vars(args)[key]!=None},
                           allow_val_change=True)
    print(f"Model: {wandbRun.config.model}")
    if args.mode==EVALUATE:
        print(f"EvaluationMetrics: {wandbRun.config.evaluateMetrics}")
        print(f"EvaluationDatasets: {wandbRun.config.evaluateDatasets}")
    else:
        print(f"BatchSize: {wandbRun.config.train[0]}")
        print(f"SubBatchSize: {wandbRun.config.train[1]}")
        print(f"CropSize: {wandbRun.config.train[2]}")
        print(f"Spacing: {wandbRun.config.train[3]}")
        print(f"Loss: {wandbRun.config.loss}")
        print(f"LearningRate: {wandbRun.config.learningRate[0]}")
        print(f"Metric: {wandbRun.config.metric}")

    return wandbRun
    
def compressed_pickle(title, data):
    with bz2.BZ2File(title + '.pbz2', 'w') as f: 
        cPickle.dump(data, f)

def decompress_pickle(file):
    data = bz2.BZ2File(file, 'rb')
    data = cPickle.load(data)
    return data

def savePickle(file,data):
    with open(file+'.pickle', 'wb') as f:
        pickle.dump(data, f)
def openPickle(file):
    with open(file,'rb') as f:
        loaded_obj = pickle.load(f)
    return loaded_obj
def saveJson(file, data):
    with open(f'{file}.json', 'w') as outfile:
        json.dump(data, outfile)
def openJson(file):
    with open(file,'r') as j:
        data = json.loads(j.read())
    return data