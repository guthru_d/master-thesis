import torch
from config import *
from src.stm.stcn.resnetSTCN import ResnetSTCN
from src.stm.stm.efficientSTM import EfficientSTM
from variables import *
from monai.networks import nets
from torchsummary import summary
from DL.Model.unet3d.model import UNet3D
from src.stm.stcn.efficientSTCN import EfficientSTCN
from src.stm.stcn.network import STCN
from src.stm.stm.model import STM
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
from src.utils.pytorch_modelsize import SizeEstimator
import hydra
def ModelWrapper(config):
    if config.model == SWIN_UNETR:
        model = getattr(nets,SWIN_UNETR)(
            img_size=tuple(config.SwinUNETR[3]),
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            feature_size=config.SwinUNETR[0],
            use_checkpoint=config.SwinUNETR[1],
            )
    elif config.model == BASIC_UNET:
        model = getattr(nets,BASIC_UNET)(
            features=tuple(config.BasicUNet[0]),
            norm=config.BasicUNet[1],
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            spatial_dims=config.modelGeneral[1],
            )
    elif config.model == UNET:
        model = getattr(nets,config.model)(
            channels=tuple(config.UNet[0]),
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            num_res_units=config.UNet[1],
            spatial_dims=config.modelGeneral[1],
            norm=config.UNet[2],
            adn_ordering=config.UNet[3],
            strides=tuple([2 for i in range(len(config.UNet[0])-1)]),
            act=config.modelGeneral[2],
            )
    elif config.model == VNET:
        model = getattr(nets,VNET)(
            dropout_prob=config.VNet[0],
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            dropout_dim=config.VNet[1],
            spatial_dims=config.modelGeneral[1],
            )
        
    elif config.model == BASIC_UNET_PLUS_PLUS:
        model = getattr(nets,config.model)(
            features=tuple(config.BasicUNetPlusPlus[0]),
            norm=config.BasicUNetPlusPlus[1],
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            spatial_dims=config.modelGeneral[1],
            )
    elif config.model == FLEXIBLE_UNET:
        model = getattr(nets,config.model)(
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            spatial_dims=config.modelGeneral[1],
            backbone=config.FlexibleUNet[0],
            decoder_channels=tuple(config.FlexibleUNet[1]),
            dropout=config.FlexibleUNet[2],
            act=config.modelGeneral[2],
            )
    elif config.model == SMOLDE_UNET:
        model = UNet3D(
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            f_maps=config.smoldeUNet[0],
            layer_order=config.smoldeUNet[1],
            num_groups=config.smoldeUNet[2],
            conv_padding=config.smoldeUNet[3],
            conv_kernel_size=config.smoldeUNet[4],
            pool_kernel_size=config.smoldeUNet[5],
        )
    elif config.model == STCNET:
        model = STCN(single_object=True,
                    out_channels=len(config.labelsToSegment)-1)
    elif config.model == STMNET:
        # model = STM()
        model = STM(
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            channels = config.STMNet[0],
            num_res_units=config.STMNet[1],
        )

    elif config.model == EFFICIENT_STM:
        model = EfficientSTM(
            in_channels=config.modelGeneral[0],
            out_channels=len(config.labelsToSegment),
            decoder_channels=config.EfficientSTMNet[1],
            backbone=config.EfficientSTMNet[0],
        )  
    elif config.model == EFFICIENT_STCN:
        model = EfficientSTCN(
            in_channels=config.modelGeneral[0],
            # out_channels=len(config.labelsToSegment),
            out_channels=1,
            decoder_channels=config.EfficientSTCNet[1],
            backbone=config.EfficientSTCNet[0],
        )
    elif "stcn" in config.model.name:
        model = hydra.utils.instantiate(config.model,num_images=config.train.num_images)      
        
        
    # print("Let's use", torch.cuda.device_count(), "GPUs!")
    # if torch.cuda.device_count() > 1:
        # print("Let's use", torch.cuda.device_count(), "GPUs!")
        # model = torch.nn.DataParallel(model)
    batchNorms = [k.split('.') for k, m in model.named_modules() if type(m).__name__ == 'BatchNorm3d']
    for *parent,k in batchNorms:
        features=getattr(model.get_submodule('.'.join(parent)),str(k)).num_features
        setattr(model.get_submodule('.'.join(parent)),str(k),torch.nn.GroupNorm(8,features))
    print("YOU ARE USING GROUP NORM!!!!!!!!!!!!!!!!!!")
    model.to(config.train.device)
    if "stm" in config.model.name:
        summary(model,(1,5,128,128,64),batch_dim=None)
    elif "stcn" in config.model.name:
        summary(model,(1,4,64,64,64),batch_dim=None)
    else:
        summary(model,(1,1,128,128,64),batch_dim=None)
    return model


