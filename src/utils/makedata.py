# -*- coding: utf-8 -*-
import os
from tkinter import SINGLE
import nibabel as nib
from config import *
from variables import *
from Utils.ioUtils import*

#TODO: ADD STRUCT_CONVERSION FILE DIRECTLY
def getFileList(dirPath,args):
    fileList = []
    if args.dataset==CT_DATA_G2:
        for scanID in os.listdir(dirPath):
            scanFolder = os.path.join(dirPath,scanID)
            CTFiles = [os.path.join(scanFolder,filename) for filename in os.listdir(scanFolder) if 'CT' in filename]
            STFiles = [os.path.join(scanFolder,filename) for filename in os.listdir(scanFolder) if 'RS' in filename]
            fileList.append([CTFiles,STFiles,scanID])
    elif args.dataset==NSCLC:
        for scanID in os.listdir(dirPath):
            scanFolder = os.path.join(dirPath,scanID)
            CTFile = os.path.join(scanFolder,'CT.nrrd')
            STFile = [os.path.join(scanFolder,'manualStructureSet.pkl.gz')]
            fileList.append([CTFile,STFile,scanID])
    elif args.dataset==HNC:
        for scanID in os.listdir(dirPath):
            scanFolder = os.path.join(dirPath,scanID)            
            CTFile = os.path.join(scanFolder,f"image_{scanID}.pkl.gz")
            STFile = [os.path.join(scanFolder,f"label_{scanID}.pkl.gz")]
            fileList.append([CTFile,STFile,scanID])
    elif args.dataset==CT_DATA_512:
        CTFiles=[os.path.join(dirPath,file) for file in os.listdir(dirPath) if IMAGE in file]
        STFiles=[[os.path.join(dirPath,file)]  for file in os.listdir(dirPath) if STRUCTURES in file]
        scanIDs = [str(number) for number in np.arange(len(CTFiles))]
        fileList = list(map(list, zip(*[CTFiles,STFiles,scanIDs])))
    return fileList

def getTargetImageName(targetFolderName, scanName):
    return getTargetName(targetFolderName, scanName, IMAGE)

def getTargetLabelName(targetFolderName, scanName):
    return getTargetName(targetFolderName, scanName, LABEL)

def getTargetName(targetFolderName, scanName, imageType, fileExtenson=NII_FILE_EXTENSION, zipFile=True):
    ext = ".gz" if zipFile else ""
    return os.path.join(
        targetFolderName,
        "{}_{}.{}{}".format(imageType, scanName, fileExtenson, ext),
    )

def saveAsNii(image,label,imagePath,labelPath):
    ni_img = nib.Nifti1Image(image, affine=np.eye(4))
    ni_lab = nib.Nifti1Image(label*1, affine=np.eye(4))

    nib.save(ni_img, imagePath)
    nib.save(ni_lab, labelPath)
    del ni_img, ni_lab

def prepareMakeDataset(args):
    args.sourcePath = os.path.join(RAW_DATA_DIR,args.dataset)
    args.targetPath = os.path.join(DATA_PROCESSED_DIR,f"{args.dataset}/{args.labelDict}/SingleNew") if args.toSingleChannel else os.path.join(DATA_PROCESSED_DIR,f"{args.dataset}/{args.labelDict}/{ONE_HOT}")
    if os.path.isdir(args.targetPath):
        goOn=input(f"TargetPath : {args.targetPath} already exists! Do you want to continue [Y/N]?")
        if goOn=="N":
            "Stopping MakeDataset!"
            exit()
    if args.labelDict==ALL:
        args.labelsToRetain=[*LABEL_DICT.keys()][0:74]
        print(f"Labels to retain:\n {args.labelsToRetain}")
    makeFolderMaybeRecursive(args.targetPath)
    return args

def checkIfLabelInStructureSet(labelName, structureSetNames):
    if isinstance(labelName, str):
        return labelName in structureSetNames
    elif isinstance(labelName, list):
        return all(elem in structureSetNames for elem in labelName)
    else:
        raise NotImplementedError

def getLabel(structures,labelsToRetain):
    labels = np.zeros_like(structures.getPixelArray()[0:1])*0
    for l, labelName in enumerate(labelsToRetain):
        l+=1
        if checkIfLabelInStructureSet(
            labelName, structures.getStructureNames()
        ):
            tmpLabel=np.any(structures.getPixelArray(structureName=labelName),0,keepdims=True)
            labels=np.where(labels==0,tmpLabel*l,labels)
    return labels

# def getLabel(structures,labelsToRetain):
    labels = []
    for labelName in labelsToRetain:
        if checkIfLabelInStructureSet(
            labelName, structures.getStructureNames()
        ):
            labels.append(
                np.any(
                    structures.getPixelArray(structureName=labelName),
                    0,
                    keepdims=True,
                )
            )  # any in case of combining labels into one
        else:
            labels.append(np.zeros_like(structures.getPixelArray()[0:1]))

    label = np.concatenate(labels, axis=0)
    return label