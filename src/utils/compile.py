import torch
from monai import losses, metrics
from monai.optimizers import LearningRateFinder
from torch.optim.lr_scheduler import ReduceLROnPlateau
from variables import *
from src.utils.metrics import SurfaceDice
from DL.Loss.pytorchLosses import CELoss
import ast
from matplotlib import pyplot as plt
import src
from src.utils.losses import BootstrappedCEDiceLoss
import hydra

class CompileWrapper():
    def __init__(self, config, model) -> None:
        self.loss = hydra.utils.instantiate(config.loss.parameters)
        self.optimiser = hydra.utils.instantiate(config.optimiser,params=model.parameters())
        self.metric = [*hydra.utils.instantiate(config.metric).values()]
        self.scheduler = hydra.utils.instantiate(config.scheduler,optimizer=self.optimiser)




    # def initiateLearningRateFinder(self):
    #     lrFinder = LearningRateFinder(self.model, self.optimiser, self.loss, device=self.config.device)
    #     lrFinder.range_test(self.trainLoader, self.validationLoader, end_lr=self.config.learningRate[1], num_iter=self.config.iterationLRFinder)
    #     steepestLR, _ = lrFinder.get_steepest_gradient()
    #     ax = plt.subplots(1, 1, figsize=(15, 15), facecolor="white")[1]
    #     _ = lrFinder.plot(ax=ax)
    #     plt.show()
    #     for g in self.optimiser.param_groups:
    #         g['lr'] = steepestLR
    #     print(f"New best inital LR: {steepestLR}!")


# def getLoss(config):
    # if config.loss==DICE_CE_LOSS:
    #     print(f"Loss: {config.loss} with: {config.DiceCELoss}")
    #     return getattr(losses,DICE_CE_LOSS)(to_onehot_y=config.lossGeneral[0], 
    #                                    softmax=config.lossGeneral[1], 
    #                                    sigmoid=config.lossGeneral[2],
    #                                    include_background=config.lossGeneral[3],
    #                                    lambda_dice=config.DiceCELoss[0],
    #                                    lambda_ce=config.DiceCELoss[1],
    #                                    jaccard=config.DiceCELoss[2],
    #                                    )
    # elif config.loss==DICE_FOCAL_LOSS:
    #     print(f"Loss: {config.loss} with: {config.DiceFocalLoss}")
    #     return getattr(losses,DICE_FOCAL_LOSS)(to_onehot_y=config.lossGeneral[0], 
    #                             softmax=config.lossGeneral[1], 
    #                             sigmoid=config.lossGeneral[2],
    #                             include_background=config.lossGeneral[3],
    #                             lambda_dice=config.DiceFocalLoss[0],
    #                             lambda_focal=config.DiceFocalLoss[1],
    #                             jaccard=config.DiceFocalLoss[2],
    #     )
    # elif config.loss==DICE_LOSS:
    #     print(f"Loss: {config.loss}")
    #     return getattr(losses,DICE_FOCAL_LOSS)(to_onehot_y=config.lossGeneral[0], 
    #                             softmax=config.lossGeneral[1], 
    #                             sigmoid=config.lossGeneral[2],
    #                             include_background=config.lossGeneral[3],
    #     )
    # elif config.loss==BOOTSTRAP_DICE_CE_LOSS:
    #     print(f"Loss: {config.loss} with: {config.DiceCELoss}")
    #     return BootstrappedCEDiceLoss(to_onehot_y=config.lossGeneral[0], 
    #                                    softmax=config.lossGeneral[1], 
    #                                    sigmoid=config.lossGeneral[2],
    #                                    include_background=config.lossGeneral[3],
    #                                    lambda_dice=config.DiceCELoss[0],
    #                                    lambda_ce=config.DiceCELoss[1],
    #                                    jaccard=config.DiceCELoss[2],
    #                                    start_warm=config.bootstrap[0],
    #                                    end_warm=config.bootstrap[1],
    #                                    top_p=config.bootstrap[2],
    #                                    )

    
                                
                          
# def getMetric(config,specificMetric=False):
#     if specificMetric:
#         metric=specificMetric
#     else:
#         metric=config.metric
        
#     if metric==DICE_METRIC:
#         print(f"Metric: {metric} with: {config.metricGeneral}")
#         return  getattr(metrics,DICE_METRIC)(include_background=config.metricGeneral[2], 
#                                              reduction=config.metricGeneral[0], 
#                                              get_not_nans=config.metricGeneral[1],
#                                              )
#     elif metric==AVERAGE_SURFACE_DISTANCE:
#         print(f"Metric: {metric} with: {config.metricGeneral}")
#         return  getattr(metrics,AVERAGE_SURFACE_DISTANCE)(include_background=config.metricGeneral[2],
#                                                           reduction=config.metricGeneral[0], 
#                                                           get_not_nans=config.metricGeneral[1],
#                                                           symmetric=config.SurfaceDistanceMetric[0],
#                                                           distance_metric=config.SurfaceDistanceMetric[1],
#                                                           )
#     elif metric==HAUSDORFF_DISTANCE_METRIC:
#         print(f"Metric: {metric} with: {config.metricGeneral}")
#         return  getattr(metrics,HAUSDORFF_DISTANCE_METRIC)(include_background=config.metricGeneral[2],
#                                                             reduction=config.metricGeneral[0], 
#                                                             get_not_nans=config.metricGeneral[1],
#                                                             percentile=config.HausdorffDistanceMetric[0],
#                                                             distance_metric=config.HausdorffDistanceMetric[1],
#                                                             directed=config.HausdorffDistanceMetric[2],
#                                                             )
#     elif metric==SURFACE_DICE:
#         print(f"Metric: {metric} with: {config.metricGeneral}")
#         return  SurfaceDice(include_background=config.metricGeneral[2],
#                             reduction=config.metricGeneral[0], 
#                             get_not_nans=config.metricGeneral[1],
#                             voxel_spacing=tuple(config.train[3]),
#                             tolerance=config.SurfaceDice[0],
#                             )
#     elif metric==MEAN_IOU:
#         print(f"Metric: {metric} with: {config.metricGeneral}")
#         return  getattr(metrics,MEAN_IOU)(include_background=config.metricGeneral[2], 
#                                              reduction=config.metricGeneral[0], 
#                                              get_not_nans=config.metricGeneral[1],
#                                              )


# def getOptimiser(config,model):
#     if config.optimiser==ADAM_W:
#         print(f"Optimiser: {config.optimiser} with: {config.AdamW}")
#         return getattr(torch.optim,ADAM_W)(model.parameters(), 
#                                            lr=config.learningRate[0], 
#                                            weight_decay=config.AdamW[0])
#     elif config.optimiser==ADAM:
#         print(f"Optimiser: {config.optimiser}")
#         return getattr(torch.optim,ADAM)(model.parameters(), 
#                                            lr=config.learningRate[0])
# def getLRScheduler(config,optimiser):
#     if config.lrScheduler==PLATEAU_DECAY_SCHEDULER:
#         print(f"LRScheduler: {config.lrScheduler} with: {config.PlateauDecayScheduler}")
#         mode = "min" if "Loss" in config.PlateauDecayScheduler[0] else "max"
#         return ReduceLROnPlateau(optimiser,
#                                     mode=mode,
#                                     factor=config.PlateauDecayScheduler[1],
#                                     patience=config.PlateauDecayScheduler[2],
#                                     cooldown=config.PlateauDecayScheduler[3],
#                                     )
        
    


























  
  
# class SurfaceDice:
#     def __init__(self, cfg):
#         self.voxelSpacing = readCfg(
#             cfg, VOXEL_SPACING, [0.9765625, 0.9765625, 2.0], literal_eval
#         )
#         self.tolerance = readCfg(cfg, TOLERANCE, 2.0, float)
#         self.labelNames = readCfg(cfg, LABEL_NAMES, "", literal_eval)
#         pass

#     def getName(self):
#         return ["SurfaceDice " + labelName for labelName in self.labelNames]

#     def forward(self, input, output, target, weights=None):
#         diceScores = []
#         for i in range(target.shape[1]):
#             binaryOutput = (output > 0)[0, i].detach().cpu().numpy()
#             binaryTarget = (target > 0)[0, i].detach().cpu().numpy()

#             diceScores.append(
#                 calculateSurfaceDice(
#                     binaryOutput,
#                     binaryTarget,
#                     self.voxelSpacing,
#                     tolerance=self.tolerance,
#                 )
#             )
#         return diceScores
    
          
# def calculateSurfaceDice(array1, array2, voxelSpacing, tolerance=2):
#     surfaceDistances = computeSurfaceDistances(array1, array2, voxelSpacing)
#     return compute_surface_dice_at_tolerance(surfaceDistances, tolerance)


# def computeSurfaceDistances(array1, array2, voxelSpacing):
#     if len(array1.shape) > 3:
#         if array1.shape[0] == 1:
#             array1 = array1[0]
#         else:
#             raise ValueError("Functions do not work with multiple channels")
#     if len(array2.shape) > 3:
#         if array2.shape[0] == 1:
#             array2 = array2[0]
#         else:
#             raise ValueError("Function does not work with multiple channels")
#     return compute_surface_distances(array1, array2, voxelSpacing)
    