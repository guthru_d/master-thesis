import os
import gc
import torch
import hydra
import wandb
import numpy as np
from tqdm import tqdm
from monai.data.dataset import Dataset
from monai.data import DataLoader
from monai.transforms import *
from monai.inferers import sliding_window_inference
from src.stm.stcn.eval_network import EvalResnetSTCN
from src.utils.labels import getLabelsToSegment
import traceback
from variables import *
from config import *
from collections import Counter
from src.visualization.visualize import tensorboardPlots
from src.utils.general import savePickle
import json

def modelInference(cfg,dataType=VALIDATION):
    # metrics= [*hydra.utils.instantiate(cfg.metric)]#-----------
    metrics= [*hydra.utils.instantiate(cfg.metric).values()]#-----------

    dtype = torch.float32 if cfg.inference=="float32" else torch.float16
    allWantedLabels= getLabelsToSegment(cfg.dataset.validation_labels)
    transforms = getTransforms(cfg)
    patientDict = getPatientDict(cfg,allWantedLabels,dataType)
    model = initalizeModel(cfg,dtype)
    results = runInference(cfg,transforms,patientDict,model,dtype,allWantedLabels)
    results_pp = inferencePostProcessor(results,cfg.validation.num_components,cfg.validation.connectivity)
    # if dataType==TEST:
        # saveRawResults(results_pp,allWantedLabels,cfg.wandb.setup.id,cfg.inference.eval_mode)
    dictTable = calculateMetrics(metrics,results_pp,allWantedLabels)
    metricDictionary={label:np.nanmean(dictTable.get_column(label)) for label in dictTable.columns[2:]}
    model.to(dtype=torch.float32)
    # if dataType==TEST:
    #     tensorboardPlots(results,cfg.paths.session,cfg.inference.plot_mode,cfg.inference)
    return metricDictionary,dictTable

def saveRawResults(results,allWantedLabels,id,eval_mode):
    for patient in [*results.keys()]:
        labels = {"wanted":allWantedLabels,
                  "missing":results[patient]["missing"]}
        for scan in [*results[patient]["gt"].keys()]:
            if scan != "scan_0":
                prediction = results[patient]["pred"][scan]
                outpath = f"{PREDICTIONS_DIR}/{patient}/PREDS/{id}/{eval_mode}/{scan}"
                if not os.path.exists(outpath): os.makedirs(outpath)
                np.savez(f"{outpath}/preds.npz",prediction)
                
            GTPath = f"{PREDICTIONS_DIR}/{patient}/GTS/{scan}"
            if not os.path.exists(GTPath): 
                os.makedirs(GTPath)
                np.savez(f"{GTPath}/gts.npz",results[patient]["gt"][scan])
        with open(f"{PREDICTIONS_DIR}/{patient}/labels.json","w") as f:
            json.dump(labels,f)
        CTPath = f"{PREDICTIONS_DIR}/{patient}/CTS"
        if not os.path.exists(CTPath): 
            os.makedirs(CTPath)
            np.savez(f"{CTPath}/cts.npz",results[patient]["ct"])
       

def getTransforms(cfg):
    transforms = [
        LoadImaged(
            keys=[IMAGE,LABEL],
            allow_missing_keys=True,
        ),
        Orientationd(
            keys=[IMAGE, LABEL], 
            axcodes="RAS",
            allow_missing_keys=True),
        ScaleIntensityRanged(
            keys=[IMAGE],
            a_min=-1000,
            a_max=3000,
            b_min=0.0,
            b_max=1.0,
            clip=True,
            allow_missing_keys=True),
        EnsureTyped(keys=[LABEL], device="cpu",track_meta=False,dtype=torch.bool, allow_missing_keys=True),
        EnsureTyped(keys=[IMAGE], device="cpu",track_meta=False,dtype=torch.float16, allow_missing_keys=True),
    ]
    # crop_size=[324,324,144] #
    # crop_size=[432,432,166] #

    crop_size=[432,432,176] #
    # if cfg.wandb.setup.job_type!=EVALUATE:
    transforms.append(
        CenterSpatialCropd(
                    keys=[IMAGE, LABEL],
                    roi_size=tuple(crop_size),
                    allow_missing_keys=True,
                    ))

    transforms.append(
        DivisiblePadd([IMAGE, LABEL], k=16, allow_missing_keys=True))
    
    return Compose(transforms)

def getPatientDict(cfg,allWantedLabels,dataType):
    dataPaths=[]
    dataPaths.append(os.path.join(DATA_PROCESSED_DIR,"HNC"))
    dataPaths.append(os.path.join(DATA_PROCESSED_DIR,"NSCLC"))
    dataPaths.append(os.path.join(DATA_PROCESSED_DIR,"SCT"))
    patientDict = []
    for dataPath in dataPaths:
        for p,patient in enumerate(os.listdir(dataPath)):
            # if "SCT" in dataPath:
            #     if dataType==VALIDATION:
            #         if not (p>cfg.dataset.train_split*len(os.listdir(dataPath)) and p<(cfg.dataset.train_split+cfg.dataset.validation_split)*len(os.listdir(dataPath))):continue
            #     elif dataType==TEST:
            #         if not (p>(cfg.dataset.train_split+cfg.dataset.validation_split)*len(os.listdir(dataPath))):continue
            # else:
            if dataType==VALIDATION:
                if patient not in cfg.dataset.validation: continue
            elif dataType==TEST:
                if patient not in cfg.dataset.test: continue
            imageDict=[]
            labelDict=[]
            patientPath = os.path.join(dataPath,patient)
            imageFiles = [file for file in os.listdir(patientPath) if "image" in file]
            imageFiles.sort()
            for imageFile in imageFiles:
                scan=[int(x) for x in list(imageFile) if x.isdigit()][0]
                imageFilePath=os.path.join(patientPath,imageFile)
                labelsFilePath=imageFilePath.replace("image","label").replace(".npz","")
                imageDict.append({"image":imageFilePath,"scan":scan,"patient":patient})
                availLabels=[]
                for label in allWantedLabels:
                    if any(labelFile.replace(".npz","")==label for labelFile in os.listdir(labelsFilePath)):
                        labelFilePath=os.path.join(labelsFilePath,f"{label}.npz")
                        labelDict.append({"label":labelFilePath,"scan":scan,"patient":patient,"name":label})
                        availLabels.append(label)
                missing = list(set(allWantedLabels).difference(availLabels))
            patientDict.append({"images":imageDict,"labels":labelDict,"patient":patient,"missing":missing})
    
    
    return patientDict
        
def initalizeModel(cfg,dtype):
    model = EvalResnetSTCN(
        in_channels=cfg.model.in_channels,
        out_channels=cfg.model.out_channels,
        decoder_channels=cfg.model.decoder_channels,
        key_backbone=cfg.model.key_backbone,
        value_backbone=cfg.model.value_backbone,
        key_dimension=cfg.model.key_dimension,
    )
    batchNorms = [k.split('.') for k, m in model.named_modules() if type(m).__name__ == 'BatchNorm3d']
    for *parent,k in batchNorms:
        features=getattr(model.get_submodule('.'.join(parent)),str(k)).num_features
        setattr(model.get_submodule('.'.join(parent)),str(k),torch.nn.GroupNorm(8,features))
    model.to(device=cfg.train.device,dtype=dtype)
    checkpoint = torch.load(cfg.paths.last) if cfg.inference.checkpoint==LAST else torch.load(cfg.paths.best)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    model.evaluation=True
    return model

def postProcessing(num_components=2,connectivity=3,good={}):
    # return Compose([FillHoles(connectivity=connectivity)])
    fill = ['spinal_cord','esophagus','parotid_L','parotid_R','eye_L','eye_R','heart',"thyroid","lung_L","lung_R",'GTV','CTV','CTV_5400','CTV_6000_correct','CTV_7200',]
    keep = ['spinal_cord','esophagus','parotid_L','parotid_R','eye_L','eye_R','heart','lung_L','lung_R']
    #'GTV','CTV','CTV_5400','CTV_6000_correct','CTV_7200',
    applied_labels_fill=[]
    applied_labels_keep=[]
    print(good)
    for go in [*good.keys()]:
        if go in fill:
            applied_labels_fill.append(good[go])
    for go in [*good.keys()]:
        if go in keep:
            applied_labels_keep.append(good[go])
    return Compose([FillHoles(connectivity=connectivity,applied_labels=applied_labels_fill),
                    RemoveSmallObjects(min_size=1500,connectivity=connectivity),
                    KeepLargestConnectedComponent(num_components=1,applied_labels=applied_labels_keep,connectivity=connectivity)])

def calculateMetrics(metrics,results,labels):
    columns = ['id','scan']
    for metric in metrics:
        metricName=metric.__class__.__name__
        columns.append("Mean"+metricName)
        columns.extend([metricName+label for label in labels])
    dictTable = wandb.Table(columns=columns)

    for patient in [*results.keys()]:
        if not results[patient]: continue
        for scan_key in tqdm([*results[patient]["pred"].keys()]):
            metricValuesList = [patient,scan_key[-1]]
            for metric in metrics:
                metric(y_pred   = results[patient]["pred"][scan_key],
                       y        = results[patient]["gt"][scan_key],
                )
                metricValues = metric.aggregate()
                metricValues = metricValues[0].cpu().numpy().tolist()
                if len(results[patient]["missing"])>0:
                    for missingLabel in results[patient]["missing"]:
                        metricValues.insert(labels.index(missingLabel),np.nan)
                metricValuesList.append(np.nanmean(metricValues))
                metricValuesList.extend(metricValues)
                metric.reset()
            dictTable.add_data(*metricValuesList)
    return dictTable


def runInference(cfg,transforms,patientDict,model,dtype,allWantedLabels):
    results = {}
    for p in tqdm(patientDict):
        results[p['patient']]={}
        imageDataset = Dataset(data=p['images'],transform=transforms)
        labelDataset = Dataset(data=p['labels'],transform=transforms)
        imageLoader = DataLoader(dataset=imageDataset,num_workers=2, batch_size=1, shuffle=False)
        labelLoader = DataLoader(dataset=labelDataset,num_workers=2, batch_size=1, shuffle=False)
        images = []
        labels = []
        for image in imageLoader:
            images.append(image['image'].to(cfg.validation.device,dtype=dtype))
        tmpLabelList=[]
        k=0
        for l, label in enumerate(labelLoader):
            tmpLabelList.append(label['name'][0])
            labels.append(label['label'].to(cfg.validation.device,dtype=dtype))
            
            
        countLabels = Counter(tmpLabelList)
        numL = len(images)
        print(countLabels)
        goodLabels = np.array([*countLabels.keys()])[np.array([*countLabels.values()])==numL].tolist()
        badLabels = list(set(allWantedLabels) - set(goodLabels))
        print(f"Complet Labels: {goodLabels}")
        print(tmpLabelList)
        print(type(labels))
        if badLabels: labels = np.array(labels)[np.isin(np.array(tmpLabelList),goodLabels)].tolist()
        index = len(goodLabels)
        
        zeros = torch.zeros(1,1,*label['label'].shape[-3:]).to(cfg.validation.device,dtype=dtype) 
        cts = torch.cat(images,dim=1)
        groundtruths = {f"scan_{i}":torch.cat([zeros,*labels[i*len(goodLabels):(i+1)*len(goodLabels)]],dim=1).to(dtype=torch.int) for i in range(cts.shape[1])}
        if cfg.inference.eval_mode=="first": labels = labels[0:index]
        if not labels: continue
        masks = torch.cat(labels,dim=1)
        info_dict = {
            "num_scans":cts.shape[1],
            "num_labels":masks.shape[1],
            "eval_mode":cfg.inference.eval_mode,
            "top_k":cfg.inference.top_k,
            "index":index,
        }
        print(info_dict)
        model_input =torch.cat([cts,masks],dim=1)
        with torch.no_grad():
            raw_preds = sliding_window_inference(
                inputs=model_input,
                roi_size=tuple(cfg.train.crop_size),
                # roi_size=(96,96,48),
                sw_batch_size=cfg.validation.window_batch_size,
                predictor=model,
                overlap=cfg.validation.overlap,
                # overlap=0.125,
                sw_device=cfg.train.device,
                progress=True,
                device=cfg.validation.device,
                **info_dict)
        preds = {}
        cut_gts = {}       
        
        if "spinal_cord" in goodLabels:
            spinalIndex=[goodLabels.index("spinal_cord")]
        else:
            spinalIndex=[]
        if "esophagus" in goodLabels:
            esophagusIndex=[goodLabels.index("esophagus")]
        else:
            esophagusIndex=[]
        for scan in tqdm([*raw_preds.keys()]):
            zeros = torch.zeros(1,1,*raw_preds[scan].shape[-3:]).to(cfg.validation.device,dtype=dtype)
            catZP = torch.cat([zeros,raw_preds[scan]],dim=1)
            catZP = torch.where(catZP>0.35,1,0)#Binarize
            if cfg.inference.eval_mode=="first":
                if spinalIndex: catZP,cutGT = correctLabel(catZP,groundtruths["scan_0"],groundtruths[scan],spinalIndex[0]+1) 
                if esophagusIndex: catZP,cutGT = correctLabel(catZP,groundtruths["scan_0"],groundtruths[scan],esophagusIndex[0]+1)
            else:
                s = int(scan[-1])-1
                if spinalIndex: catZP,cutGT = correctLabel(catZP,groundtruths[f"scan_{s}"],groundtruths[scan],spinalIndex[0]+1) 
                if esophagusIndex: catZP,cutGT = correctLabel(catZP,groundtruths[f"scan_{s}"],groundtruths[scan],esophagusIndex[0]+1)
            print(catZP.shape)
            print(groundtruths[scan].shape) 
            # preds[scan]=post_processing(catZP[0]).unsqueeze(0)
            preds[scan]=catZP.to(dtype=torch.int32)
            cut_gts[scan]=cutGT.to(dtype=torch.int32)     

        
        results[p['patient']]['pred']=preds
        results[p['patient']]['gt']=cut_gts
        results[p['patient']]['ct']=cts
        results[p['patient']]["missing"] = badLabels
        results[p['patient']]["good"] = {good:i+1 for i,good in enumerate(goodLabels)}

        # results[p['patient']]["cfg"] = cfg

        del preds,groundtruths,cts,p,catZP,zeros,scan,raw_preds,model_input,masks,imageDataset,labelDataset,labelLoader,imageLoader,images,labels,cut_gts,cutGT
        gc.collect()
    return results



def correctLabel(pred,ref,gt,index):
    try:
        mi = torch.where(ref[:,index,:,:,:]==1)[-1].min()
        ma = torch.where(ref[:,index,:,:,:]==1)[-1].max()
        pred[:,index,:,:,:mi]=0
        pred[:,index,:,:,ma:]=0
        gt[:,index,:,:,:mi]=0
        gt[:,index,:,:,ma:]=0    
    except:
        print(f"pred_shape: {pred.shape}")
        print(f"gt_shape: {gt.shape}")
        print(f"index:{index}")
        print("There was an error with cutting Label Region, we proceed without any adjustments!")
    return pred,gt


def inferencePostProcessor(results,num_components,connectivity):
    for patient in [*results.keys()]:
        goodDict=results[patient]['good']
        # pp1 = postProcessing(num_components=1,connectivity=connectivity,good=goodDict)
        pp2 = postProcessing(num_components=1,connectivity=connectivity,good=goodDict)
        print(f"Postprocessing of patient {patient}")
        for scan in tqdm([*results[patient]['pred'].keys()]):
            # results[patient]["pred"][scan]=pp1(results[patient]["pred"][scan][0]).unsqueeze(0)
            results[patient]["pred"][scan]=pp2(results[patient]["pred"][scan][0]).unsqueeze(0)

    return results






# class EvaluateWrapper():
#     def __init__(self,cfg,model,dataLoader,metrics,run) -> None:
#         self.cfg = cfg
#         self.dataLoader = dataLoader
#         self.model = model
#         self.modelName = self.model.__class__.__name__
#         self.run = run
#         self.checkpoint = torch.load(self.cfg.paths.last) if cfg.inference.checkpoint==LAST else torch.load(self.cfg.paths.best)
#         self.model.load_state_dict(self.checkpoint['model_state_dict'])
#         self.metrics = metrics

#     def windowEvaluate(self):
#         metricDictionary,dictTable=modelInference(model=self.model,
#                                             modelName=self.modelName,
#                                             dataLoader=self.dataLoader,
#                                             labelsToSegment= getLabelsToSegment(self.cfg.dataset.validation_labels),
#                                             inputs_device=self.cfg.validation.device,
#                                             window_size=self.cfg.train.crop_size,
#                                             sw_device=self.cfg.train.device,
#                                             metrics=self.metrics,
#                                             SESSION_DIR=self.cfg.paths.session,
#                                             runMode=self.cfg.wandb.setup.mode,
#                                             post_device="cpu",
#                                             )
#         self.run.log(metricDictionary)
#         self.run.log({"Inference"+"Metrics":dictTable})
#         self.run.finish()
#         print("We done!")
     
     
# def modelInference(
#             model,
#             modelName,
#             dataLoader,
#             labelsToSegment,
#             inputs_device,
#             window_size,
#             sw_device,
#             metrics,
#             SESSION_DIR,
#             globalValidationStep=-1,
#             runMode=TRAIN,
#             overlap=0.25,
#             post_device='cuda:0'):
#     model.eval()
#     model.evaluation=True
#     columns = ['id']
#     for metric in metrics:
#         metricName=metric.__class__.__name__
#         columns.append("Mean"+metricName)
#         columns.extend([metricName+label for label in labelsToSegment])
#     dictTable = wandb.Table(columns=columns)
#     step=0
#     with torch.no_grad(): 
#         val_outputs_list=[]
#         val_labels_list=[]
#         gt_list = []
#         lastPatient="first"
#         labelListStr=[]
#         for i, val_data in tqdm(enumerate(dataLoader, 0)):
#             step += 1
#             val_inputs, val_labels = (
#                 val_data[IMAGE].to(inputs_device),
#                 val_data[LABEL].to(inputs_device),
#             )
#             val_inputs, val_labels = prepareInputs(modelName,val_inputs,val_labels) # [1,4,X,Y,Z]
#             val_outputs = sliding_window_inference(
#                     inputs=val_inputs.to(device=inputs_device,dtype=torch.float16),#put on CPU
#                     roi_size=tuple(window_size),
#                     sw_batch_size=1,
#                     predictor=model.to(dtype=torch.float16),
#                     overlap=overlap,
#                     sw_device=sw_device,
#                     progress=True,
#                     device=inputs_device)
#             if lastPatient==val_data['patient'][0]+str(val_data["scan"]) or len(val_outputs_list)==0:
#                 val_outputs=prepareOutputs(modelName,val_outputs.detach().to(device=post_device))
#                 val_outputs=[activationAndArgmax()(i) for i in decollate_batch(val_outputs)]
#                 val_outputs=[postProcessing(num_componets=2,connectivity=3)(i) for i in val_outputs]
#                 val_outputs_list.append(torch.stack(val_outputs,axis=0))
#                 val_labels_list.append(val_labels.detach().to(device=post_device))
#                 gt_list.append(val_inputs[:,-1].detach().to(device=post_device))
#                 pct=val_inputs[:,0:-1].detach().to(device=post_device)
#                 info = [val_data['patient'][0],val_data['scan']]
#                 labelListStr.append(val_data['name'][0])
#                 background = torch.zeros((1,1)+val_labels.shape[2:]).to(device=post_device)
#             else:
#                 cat_outputs = torch.cat(val_outputs_list,axis=1)
#                 cat_labels = torch.cat(val_labels_list,axis=1)
#                 cat_gt=torch.cat((background,torch.stack(gt_list,axis=1).detach().to(device=post_device)),axis=1)
#                 cat_gt=torch.stack([argMax()(i) for i in cat_gt],axis=0)
#                 cat_val_inputs=torch.cat((pct,cat_gt),axis=1)
#                 del background,val_outputs_list,val_labels_list,gt_list,pct,cat_gt
#                 gc.collect()
#                 dictTable=getPlotsAndCalcMetrics(cat_outputs,cat_val_inputs,cat_labels,dictTable,info,metrics,modelName,globalValidationStep,SESSION_DIR,runMode,labelListStr,post_device)
#                 val_outputs=prepareOutputs(modelName,val_outputs.detach().to(device=post_device))
#                 val_outputs=[activationAndArgmax()(i) for i in decollate_batch(val_outputs)]
#                 val_outputs=[postProcessing(num_componets=2,connectivity=3)(i) for i in val_outputs]
#                 val_outputs_list=[torch.stack(val_outputs,axis=0)]
#                 val_labels_list=[val_labels.detach().to(device=post_device)]
#                 gt_list=[val_inputs[:,-1].detach().to(device=post_device)]
#                 labelListStr=[val_data['name'][0]]
#             lastPatient=val_data['patient'][0]+str(val_data["scan"])
#             del val_inputs,val_labels,val_outputs

#     metricDictionary={label:np.nanmean(dictTable.get_column(label)) for label in columns[1:]}
#     model.to(dtype=torch.float32)
#     return metricDictionary,dictTable
            
def prepareInputs(inputs,labels):
    # if "stc" in modelName:
    model_input =torch.cat([inputs,labels[:,0].unsqueeze(1)],dim=1)
    gt = labels[:,1:].flatten(0,1).unsqueeze(1)
    # else:
    #     model_input = inputs
    #     gt = labels
    return model_input,gt

def prepareOutputs(outputs):
    if outputs.shape[1]==4:
        outputs =torch.cat(torch.chunk(outputs,2,dim=1),dim=0)
    return outputs
def activationAndArgmax():
    return Compose([Activations(softmax=True),
                    AsDiscrete(argmax=True),
        ])

def argMax():   
    return Compose([AsDiscrete(argmax=True)])


def getPlotsAndCalcMetrics(val_outputs,val_inputs,val_labels,dictTable,info,metrics,modelName,globalValidationStep,SESSION_DIR,runMode,labelListStr,post_device,alpha=0.2):
    background = torch.zeros((2,1)+val_labels.shape[2:]).to(device=post_device)
    val_labels = torch.cat((background,val_labels),axis=1)
    val_outputs= torch.cat((background,val_outputs),axis=1)
    for i in range(2):
        keyName= info[0]+"-"+info[1][i+1][0]
        keyName =keyName+"-inital" if i==0 else keyName+"-secondary"
        tableList=[keyName]         
        for metric in metrics:
            metric(y_pred=val_outputs[i].unsqueeze(0).to(device="cuda:0"), y=val_labels[i].unsqueeze(0).to(device="cuda:0"))
            metricValues = metric.aggregate()
            tableList.append(np.nanmean(metricValues.cpu().numpy()))
            tableList.extend(metricValues.cpu().numpy()[0].tolist())
            metric.reset()
        tableList=updateTalbeForMissing(labelListStr,dictTable.columns,tableList)
        dictTable.add_data(*tableList)
    if "ST" in modelName and runMode!=SWEEP:
        saveName= info[0]+"-"+str(info[1])+runMode
        val_outputs=torch.stack([argMax()(i) for i in val_outputs],axis=0)
        val_labels =torch.stack([argMax()(i) for i in val_labels],axis=0)
        makeSequencePlots(val_inputs,val_labels,val_outputs,SESSION_DIR,saveName,globalValidationStep,alpha=alpha)
    del val_inputs,val_labels,val_outputs
    gc.collect()
    return dictTable
def updateTalbeForMissing(labels,columns,old):
    new=[]
    step=0
    for c in columns:
        tmpStep=step
        for label in labels:
            if label in c or 'id'==c or "Mean" in c:
                new.append(old[step])
                step+=1
                break
        if step==tmpStep:
            new.append(np.nan)
    return new