from typing import Callable, List, Optional, Sequence, Union
from monai import losses
import torch

class BootstrappedCEDiceLoss(losses.DiceCELoss):
    def __init__(self,
            start_warm: int = 20000, 
            end_warm: int = 70000, 
            top_p: float = 0.15, 
            include_background: bool = True,
            to_onehot_y: bool = False,
            sigmoid: bool = False,
            softmax: bool = False,
            other_act: Optional[Callable] = None,
            squared_pred: bool = False,
            jaccard: bool = False,
            reduction: str = "mean",
            smooth_nr: float = 1e-5,
            smooth_dr: float = 1e-5,
            batch: bool = False,
            ce_weight: Optional[torch.Tensor] = None,
            lambda_dice: float = 1.0,
            lambda_ce: float = 1.0,
    ) -> None:
        super().__init__(
                         include_background=include_background,
                         to_onehot_y=to_onehot_y,
                         sigmoid=sigmoid,
                         softmax=softmax,
                         other_act=other_act,
                         squared_pred=squared_pred,
                         jaccard=jaccard,
                         reduction=reduction,
                         smooth_dr=smooth_dr,
                         smooth_nr=smooth_nr,
                         batch=batch,
                         ce_weight=ce_weight,
                         lambda_dice=lambda_dice,
                         lambda_ce=lambda_ce,
                         )
        self.start_warm = start_warm
        self.end_warm = end_warm
        self.top_p = top_p
        self.cross_entropy = torch.nn.CrossEntropyLoss(weight=ce_weight, reduction="none")
        self.dice = losses.DiceLoss(
            include_background=include_background,
            to_onehot_y=to_onehot_y,
            sigmoid=sigmoid,
            softmax=softmax,
            other_act=other_act,
            squared_pred=squared_pred,
            jaccard=jaccard,
            reduction="none",
            smooth_nr=smooth_nr,
            smooth_dr=smooth_dr,
            batch=batch,
        )
        
    def forward(self, input: torch.Tensor, target: torch.Tensor, it) -> torch.Tensor:
        """
        Args:
            input: the shape should be BNH[WD].
            target: the shape should be BNH[WD] or B1H[WD].

        Raises:
            ValueError: When number of dimensions for input and target are different.
            ValueError: When number of channels for target is neither 1 nor the same as input.

        """
        
        if len(input.shape) != len(target.shape):
            raise ValueError("the number of dimensions for input and target should be the same.")
        if it < self.start_warm:
            dice_loss = self.dice(input, target)
            ce_loss = self.ce(input, target).mean()
            total_loss: torch.Tensor = self.lambda_dice * dice_loss + self.lambda_ce * ce_loss
            return total_loss.mean(), 1.0 
         
         
        dice_loss = self.dice(input, target).view(-1)
        num_pixels_dice = dice_loss.numel()
        ce_loss = self.ce(input, target).view(-1)
        num_pixels_ce = ce_loss.numel()

        if it > self.end_warm:
            this_p = self.top_p
        else:
            this_p = self.top_p + (1-self.top_p)*((self.end_warm-it)/(self.end_warm-self.start_warm))
        ce_loss, _ = torch.topk(ce_loss, int(num_pixels_ce * this_p), sorted=False)
        # dice_loss, _ = torch.topk(dice_loss, int(num_pixels_dice * this_p), sorted=False)
        
        raw_loss: torch.Tensor = self.lambda_dice * dice_loss.mean() + self.lambda_ce * ce_loss.mean()     
        return raw_loss,this_p    