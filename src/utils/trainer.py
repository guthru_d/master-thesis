import time
import gc
import torch
import warnings
from config import *
from variables import *
import numpy as np
from monai.transforms import *
import wandb
from tqdm import tqdm
import numpy as np
from monai.transforms import LabelFilter,MapLabelValue
from monai.inferers import sliding_window_inference
from monai.data import decollate_batch
from src.utils.evaluator import modelInference,prepareInputs,prepareOutputs
from src.utils.labels import getLabelsToSegment
# import matplotlib
CUDA_LAUNCH_BLOCKING=1
# matplotlib.use('tkagg')
import random
class TrainerWrapper():
    # def __init__(self,model,trainDS,validationDS,trainLoader,validationLoader,cfg,compile,run) -> None:
    def __init__(self,model,trainDS,trainLoader,cfg,compile,run) -> None:

        self.cfg = cfg
        self.loss = compile.loss
        self.model = model
        self.optimiser = compile.optimiser
        self.metric = compile.metric
        self.scheduler = compile.scheduler
        self.trainDS = trainDS
        # self.validationDS = validationDS
        self.trainLoader = trainLoader
        # self.validationLoader = validationLoader
        self.scaler = torch.cuda.amp.GradScaler()
        self.lossName = self.loss.__class__.__name__
        self.metricName = self.metric[0].__class__.__name__
        self.schedulerName = self.scheduler.__class__.__name__
        self.monaiDatasetName = self.trainDS.__class__.__name__
        self.modelName= self.model.__class__.__name__
        self.run = run
        self.bestMetric = -1
        self.bestMetricEpoch = -1
        self.epoch=0
        self.batch_idx=0
        self.step=0
        self.it=0
        self.globalValidationStep=0
        self.modelWeights()

    def train(self):
        if self.cfg.train.profiler:
            prof = self.initProfiler()
            prof.start()
        if self.monaiDatasetName==SMART_CACHE_DATASET: 
            self.trainDS.start()
            print("SmartCache started!")
        for epoch in range(self.epoch, self.cfg.train.epochs):
            epoch_start = time.time()
            self.epoch = epoch
            print("-" * 10)
            print(f"epoch {epoch + 1}/{self.cfg.train.epochs}")
            self.model.train()
            epoch_loss = 0
            self.step=0
            tmptotalStepDuration=time.time()
            for batch_data in self.trainLoader:
                tmpModelDuration=time.time()
                totalstepDuration = time.time()-tmptotalStepDuration
                tmptotalStepDuration=time.time()

                self.step += 1
                inputs, labels = (
                    batch_data[IMAGE].to(self.cfg.train.device),
                    batch_data[LABEL].to(self.cfg.train.device),
                )
                model_input,groundtruth=prepareInputs(inputs,labels)
                loss = self.backPropagation(model_input,groundtruth)
                epoch_loss += loss.item()
                loss=loss.item()
                self.run.log({"TrainLoss"+batch_data['name'][0]:loss})
                

                print(
                f"{self.step}/{len(self.trainDS) // self.trainLoader.batch_size},"
                f" train_loss: {loss:.4f}"
                f" Total step time: {(totalstepDuration):.4f}"
                f" Model step time: {(time.time()-tmpModelDuration):.4f}"

            )
                self.batch_idx+=1

            epoch_loss /= self.step
            print(f"epoch {epoch + 1} average loss: {epoch_loss:.4f}")
            print(f"time of epoch {epoch + 1} is: {(time.time() - epoch_start):.4f}")
            self.run.log({EPOCH: epoch,
                          f"{TRAIN_LOSS}{self.lossName}": epoch_loss,
                          TRAIN_DURATION: time.time() - epoch_start,
                          LEARNING_RATE: self.getLr(),
                })

            self.saveModel(self.cfg.paths.last)
            torch.cuda.empty_cache()
            gc.collect()

            if (epoch + 1) % self.cfg.train.interval == 0:
                startTime=time.time()
                metricDictionary,dictTable = modelInference(self.cfg,dataType=VALIDATION)
                # metricDictionary,dictTable=modelInference(model=self.model,
                #                                     modelName=self.cfg.model.name,
                #                                     dataLoader=self.validationLoader,
                #                                     labelsToSegment= getLabelsToSegment(self.cfg.dataset.validation_labels),
                #                                     inputs_device=self.cfg.validation.device,
                #                                     window_size=self.cfg.train.crop_size,
                #                                     sw_device=self.cfg.train.device,
                #                                     metrics=self.metric,
                #                                     SESSION_DIR=self.cfg.paths.session,
                #                                     globalValidationStep=self.globalValidationStep,
                #                                     runMode=self.cfg.wandb.setup.job_type,
                #                                     )
                validationDuration, metricDict, bestMetricValue = self.logValidation(metricDictionary,epoch,dictTable,startTime)
                                                                                
                self.run.log({
                          VALIDATION_DURATION: validationDuration,
                          BEST+self.metricName: bestMetricValue,
                })
                self.run.log(metricDict)
                
            
            if self.schedulerName==PLATEAU_DECAY_SCHEDULER:
                if self.cfg.scheduler.tracker == TRAIN_LOSS:
                    self.scheduler.step(epoch_loss)
                elif "metric" in self.cfg.scheduler.tracker:
                    self.scheduler.step(bestMetricValue)
            else:
                self.scheduler.step()
                                                       

            if self.cfg.train.profiler:
                    prof.step()

                    
                    
            if self.monaiDatasetName==SMART_CACHE_DATASET:
                self.trainDS.update_cache()
                print("SmartCache updated!")

        if self.cfg.train.profiler: prof.stop()
        if self.monaiDatasetName==SMART_CACHE_DATASET: self.trainDS.shutdown()
            
             
    def logValidation(self,metricDictionary,epoch,dictTable,startTime):
        best = metricDictionary[[*metricDictionary.keys()][0]]
        if best > self.bestMetric:
            self.bestMetric = best
            self.bestMetricEpoch += 1
            self.saveModel(self.cfg.paths.best)
            print(f"New Best Model Saved with {self.bestMetric:.4f}")
        print(
            f"current epoch: {epoch + 1} current"
            f" mean dice: {best:.4f}"
            f" best mean dice: {self.bestMetric:.4f}"
            f" at epoch: {self.bestMetricEpoch}"
        )
        self.run.log({"validation"+"Metrics":dictTable}) 
        self.globalValidationStep+=1
        return time.time() - startTime, metricDictionary, self.bestMetric
              
    def initProfiler(self):
        print(f"Profiler initialzed at {PROFILER_PATH}")

        prof =  torch.profiler.profile(
            activities=[torch.profiler.ProfilerActivity.CPU, torch.profiler.ProfilerActivity.CUDA],
            schedule=torch.profiler.schedule(wait=0, warmup=1, active=3, repeat=2),
            on_trace_ready=torch.profiler.tensorboard_trace_handler(PROFILER_PATH),
            record_shapes=True,
            profile_memory=True,
            with_stack=True)
        return prof

    def getLr(self):
        return self.optimiser.param_groups[0]['lr']
    
    
    def backPropagation(self,model_input,groundtruth):
        with torch.cuda.amp.autocast():
            logit_map = self.model(model_input)
            logit_map = prepareOutputs(logit_map)
            if 'top_p' in self.cfg.loss.parameters:
                loss,top_p = self.loss(logit_map,groundtruth,self.it)
                self.run.log({"top_p":top_p})
            else:
                loss = self.loss(logit_map, groundtruth)
        
        showLoss = loss.detach().cpu()
        loss = loss/self.cfg.train.accumulated_batch_size
        self.scaler.scale(loss).backward()
        
        if ((self.batch_idx + 1) % self.cfg.train.accumulated_batch_size == 0):# or (self.step == len(self.trainLoader)):
            self.scaler.step(self.optimiser)
            self.scaler.update()
            self.optimiser.zero_grad()
            self.batch_idx=0
            print("Weights update Now!")
        self.it+=1
        self.run.log({"it":self.it})
        return showLoss
    
    
    def loadModel(self, modelPath):
        self.loadStateDict(self.model, modelPath)

    def loadStateDict(self, object, statePath):
        if os.path.isfile(statePath):
            object.load_state_dict(torch.load(statePath))
        else:
            warnings.warn(
                f"No state dict found at {statePath}, initial parameters are used from cfg file"
            )


    def saveModel(self,path):
        torch.save({
                'model_state_dict':       self.model.state_dict(),
                'optimiser_state_dict':   self.optimiser.state_dict(),
                'configDict':             dict(self.run.config),
                'scheduler':              self.scheduler.state_dict(),
                'epoch':                  self.epoch,
                'lr':                     self.getLr(),
                'bestMetric':             self.bestMetric,          
                }, path,
                )
        
    def modelWeights(self):    # elif self.modelName==RESNET_STCN and self.cfg.model.pretrained:
        #     keyWeights = torch.load(os.path.join(WEIGHT_DIR,f"{self.config.ResnetSTCNet[0][0]}.pth"))['state_dict']
        #     keyModelDict = self.model.key_encoder.state_dict()
        #     pretrainedKeyDict = {k: v for k, v in keyWeights.items() if k in keyModelDict}
        #     keyModelDict.update(pretrainedKeyDict)
        #     self.model.key_encoder.load_state_dict(keyModelDict)

        #     valueWeights = torch.load(os.path.join(WEIGHT_DIR,f"{self.config.ResnetSTCNet[0][1]}.pth"))['state_dict']
        #     valueModelDict = self.model.value_encoder.state_dict()
        #     pretrainedValueDict = {k: v for k, v in valueWeights.items() if k in valueModelDict}
        #     valueModelDict.update(pretrainedValueDict)
        #     del valueModelDict['module.conv1.weight']
        #     self.model.value_encoder.load_state_dict(valueModelDict,strict=False)
        #     print("You have loaded weights for the Keyencoder and ValueEncoder")
        if self.run.resumed and self.run.job_type!=SWEEP:
            checkpoint = torch.load(self.cfg.paths.last) if self.cfg.wandb.setup.notes == LAST else torch.load(self.cfg.paths.last)
            self.model.load_state_dict(checkpoint['model_state_dict'])
            self.optimiser.load_state_dict(checkpoint['optimiser_state_dict'])
            self.scheduler.load_state_dict(checkpoint['scheduler'])
            self.epoch = checkpoint['epoch']
            self.bestMetric = checkpoint['bestMetric']
            for g in self.optimiser.param_groups:
                g['lr'] = checkpoint['lr']
            print("Using checkpoint via wandb resumed !")
    
        elif self.cfg.wandb.setup.job_type==FINETUNING:
            print(self.cfg.paths.base)
            checkpoint = torch.load(self.cfg.paths.base)
            self.model.load_state_dict(checkpoint['model_state_dict'])
            print(f"You finetune on the dataset {self.cfg.dataset.name} with model from {self.cfg.paths.base}")
        else:
            print("Training from Scratch initiallized !")



