from config import *
from variables import *


def getLabelsToSegment(labels):
    if isinstance(labels,list):
        labelsAsStrs=labels
    else:
        labelsAsInts = LABEL_DICT[labels]
        if not isinstance(labelsAsInts,list): labelsAsInts=[labelsAsInts]
        labelsAsStrs = [REVERSED_LABEL_DICT[labelInt] for labelInt in labelsAsInts]
    return labelsAsStrs 