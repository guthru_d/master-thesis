from distutils.command.config import config
import os
import shutil
import json
import numpy as np
from pathlib import Path
import hydra
import monai
from time import time
from config import *
from variables import *
import random
from monai.transforms import *
from monai.data.dataset import CacheDataset,Dataset,PersistentDataset,SmartCacheDataset,CacheNTransDataset
import torch
from monai.data import DataLoader
from Utils.ioUtils import*
import itertools
from tqdm import tqdm
from collections import Counter



class DatasetWrapper():
    def __init__(self,cfg,dataType):
        self.cfg = cfg
        self.dataType=dataType
        self.dataSplitDict = self.getDatasplit()
        self.labelsToSegment = cfg.dataset.train_labels if dataType==TRAIN else cfg.dataset.validation_labels

    def balancePathDict(self, pathDict):
        countObject = Counter([example["name"] for example in pathDict])
        maxLabels = max([*countObject.values()])
        for label in [*countObject.keys()]:
            filteredList = [example for example in pathDict if example["name"]==label]
            diff = maxLabels-len(filteredList)
            while(diff>0):
                pathDict.extend(filteredList[0:diff])
                diff -= len(filteredList)
        print(f"Count/Label={len(pathDict)/len([*countObject.keys()])}")
        print(f"Count={len(pathDict)}")
        return pathDict
    def getDataset(self,transforms=None):
        pathDict,isHNCorNSCLC,readAsSequence = self.getPathDict()
        if self.dataType==TRAIN:
            pathDict = self.balancePathDict(pathDict=pathDict)
        if not transforms:
            transforms = self.getTransforms(isHNCorNSCLC,readAsSequence)
        # dataset=hydra.utils.instantiate(self.cfg.monai_dataset,data=pathDict)
        if self.dataType==TRAIN:
            dataset = SmartCacheDataset(
                data=pathDict,
                transform=transforms,
                replace_rate=self.cfg.monai_dataset.replace_rate,
                cache_num=self.cfg.monai_dataset.cache_num,
                num_init_workers=self.cfg.monai_dataset.num_init_workers,
                num_replace_workers=self.cfg.monai_dataset.num_replace_workers,
                shuffle=self.cfg.monai_dataset.shuffle,
                progress=self.cfg.monai_dataset.progress,
                )
        else:
            dataset = Dataset(
                data=pathDict,
                transform=transforms
            )    
        return dataset
    
    
        
    def getDataLoader(self,dataset):
        shuffle=True if self.dataType==TRAIN else False
        print(f"Your CPU Count: {os.cpu_count()}")
        if self.dataType==TRAIN:
            dataLoader = DataLoader(
                dataset,
                batch_size=self.cfg.train.batch_size if self.dataType == TRAIN else self.cfg.validation.batch_size,
                shuffle=shuffle,
                num_workers=self.cfg.dataloader.num_workers,
                pin_memory=self.cfg.dataloader.pin_memory,
            )
        else:
            dataLoader= DataLoader(
                dataset,
                batch_size=self.cfg.validation.batch_size,
                shuffle=False,
                num_workers=self.cfg.dataloader.num_workers,
                )
        return dataLoader
    def getLabelsToSegment(self):
        if isinstance(self.labelsToSegment,list):
            labelsAsStrs=self.labelsToSegment
        else:
            labelsAsInts = LABEL_DICT[self.labelsToSegment]
            if not isinstance(labelsAsInts,list): labelsAsInts=[labelsAsInts]
            labelsAsStrs = [REVERSED_LABEL_DICT[labelInt] for labelInt in labelsAsInts]
        return labelsAsStrs 
    
    
   
    
    def getTransforms(self,isHNCorNSCLC,readAsSequence):
        if isHNCorNSCLC and readAsSequence: #if HNC Data and asSequence=True
            if self.cfg.train.num_images==3:
                transforms = [
                    LoadImaged(keys=[IMAGE, LABEL,IMAGE_0, LABEL_0,IMAGE_1, LABEL_1]),
                    ConcatItemsd(
                                keys=[IMAGE,IMAGE_0,IMAGE_1],
                                dim =0,
                                name=IMAGE,
                ),
                    ConcatItemsd(
                                keys=[LABEL,LABEL_0,LABEL_1],
                                dim =0,
                                name=LABEL,
                ),
                    DeleteItemsd(
                                keys=[LABEL_0,LABEL_1,IMAGE_0,IMAGE_1],
                )
                ]
            elif self.cfg.train.num_images==2:
                transforms = [
                    LoadImaged(keys=[IMAGE, LABEL,IMAGE_0, LABEL_0]),
                    ConcatItemsd(
                                keys=[IMAGE,IMAGE_0],
                                dim =0,
                                name=IMAGE,
                ),
                    ConcatItemsd(
                                keys=[LABEL,LABEL_0],
                                dim =0,
                                name=LABEL,
                ),
                    DeleteItemsd(
                                keys=[LABEL_0,LABEL_1,IMAGE_0],
                )
                ]
        else:
            transforms =[
                LoadImaged(keys=[IMAGE, LABEL])
                
            ]
        transforms.append(
            Orientationd(keys=[IMAGE, LABEL], axcodes="RAS"))
        if not all(spacing==1 for spacing in [self.cfg.train.spacing_x,self.cfg.train.spacing_y,self.cfg.train.spacing_z]):
            transforms.append(
                Spacingd(
                    keys=[IMAGE, LABEL],
                    pixdim=tuple([self.cfg.train.spacing_x,
                                  self.cfg.train.spacing_y,
                                  self.cfg.train.spacing_z]),
                    mode=("bilinear", "nearest")))
        transforms.append(
            ScaleIntensityRanged(
                keys=[IMAGE],
                a_min=-1000,
                a_max=3000,
                b_min=0.0,
                b_max=1.0,
                clip=True)
        )
        # transforms.append(LabelFilterd(keys=LABEL,applied_labels=tuple(origLabels)))
        # if origLabels!=targetLabels:
        #     transforms.append(MapLabelValued(keys=LABEL, orig_labels=origLabels, target_labels=targetLabels))
            
        transforms.extend([
            EnsureTyped(keys=[LABEL], device="cpu",track_meta=False,dtype=torch.bool),
            EnsureTyped(keys=[IMAGE], device="cpu",track_meta=False,dtype=torch.float16,),
        ])




        if self.dataType==TRAIN:
            transforms.append(
                hydra.utils.instantiate(self.cfg.train.crop_by_pos_neg,spatial_size=self.cfg.train.crop_size,num_samples=self.cfg.train.num_crops)
            )
            if "augmentation" in self.cfg:
                for _, conf in self.cfg.augmentation.items():
                    if "_target_" in conf:
                        transforms.append(hydra.utils.instantiate(conf))
                
        if self.dataType==VALIDATION and self.cfg.wandb.setup.job_type!=EVALUATE:
            transforms.append(
                CenterSpatialCropd(
                    keys=[IMAGE, LABEL],
                    roi_size=tuple(self.cfg.validation.crop_size),
                )
            )

        if "synthetic" in self.cfg.dataset.name:
            transforms.extend([
                CopyItemsd(keys=IMAGE,
                    times = self.cfg.train.num_images-1),
                CopyItemsd(keys=LABEL,
                    times = self.cfg.train.num_images-1)]
            )
            if "tovideo" in self.cfg.dataset:
                transforms.append(hydra.utils.instantiate(self.cfg.dataset.tovideo))
            transforms.extend([
            ConcatItemsd(
                keys=[IMAGE,IMAGE_0,IMAGE_1],
                dim = 0,
                name=IMAGE,
                allow_missing_keys=True,
                 ),
            ConcatItemsd(
                keys=[LABEL,LABEL_0,LABEL_1],
                dim = 0,
                name=LABEL,
                allow_missing_keys=True
                )]
            )

        transforms.append(
                    DivisiblePadd([IMAGE, LABEL], self.cfg.train.divisible_padd))
        print(transforms)
        return Compose(transforms)
    def balanceSequence(self,combinations,num):
        new=[]
        for m in np.arange(1,num):
            import random
            t = []
            for c in combinations:
                one = int(c[0].split("image_")[1][0])
                two = int(c[1].split("image_")[1][0])
                if two-one==m:
                    t.append(c)
            for i in range(m-1):
                new.append(random.choice(t)) 
        combinations.extend(new)
        return combinations
    def getPathDict(self):
        readAsSequence = True if "stcn" in self.cfg.model.name else False
        pathDictList = []
        patientPaths = self.dataSplitDict[self.dataType]
        isHNCorNSCLC=any(d in path for d in ["HNC", "NSCLC, SCT"] for path in patientPaths)
        for patientPath in tqdm(patientPaths):
            allPatientFiles=[]
            for path in glob.glob(f"{patientPath}/**", recursive=True):
                allPatientFiles.append(path)
            imagePaths = [path for path in allPatientFiles if IMAGE in path]
            imagePaths.sort()
            if readAsSequence and isHNCorNSCLC:
                if self.dataType==VALIDATION or self.dataType==TEST:
                    if self.cfg.validation.eval_mode=="first":
                        combinations=[]
                        for i in range(len(imagePaths)-(self.cfg.train.num_images-1)):
                            if self.cfg.train.num_images==2:
                                combinations.append((imagePaths[0],imagePaths[i+1]))
                            elif self.cfg.train.num_images==3:
                                combinations.append((imagePaths[0],imagePaths[i+1],imagePaths[i+2]))
                    elif self.cfg.validation.eval_mode=="previous":
                        combinations=[]
                        for i in range(len(imagePaths)-(self.cfg.train.num_images-1)):
                            if self.cfg.train.num_images==2:
                                combinations.append((imagePaths[i],imagePaths[i+1]))
                            elif self.cfg.train.num_images==3:
                                combinations.append((imagePaths[i],imagePaths[i+1],imagePaths[i+2]))
                else:
                    combinations = list(itertools.combinations(imagePaths,self.cfg.train.num_images))
                    combinations = self.balanceSequence(combinations=combinations,num=len(imagePaths))    
                for combIm in combinations:
                    labelFolders = [imagePath.replace(IMAGE,LABEL).replace(".npz","/") for imagePath in combIm]
                    for label in self.getLabelsToSegment():
                        combLa = [os.path.join(labelFolder,f"{label}.npz") for labelFolder in labelFolders]
                        if all([os.path.exists(combL) for combL in combLa]):
                            if self.cfg.train.num_images==3:
                                pathDictList.append({IMAGE: combIm[0], IMAGE_0:combIm[1], IMAGE_1:combIm[2],
                                                    LABEL: combLa[0],  LABEL_0:combLa[1],  LABEL_1:combLa[2],
                                                    "name":label,
                                                    "patient":patientPath.split("/")[-1],
                                                    "scan":[combIm[0].split("/")[-1].split(".")[0],combIm[1].split("/")[-1].split(".")[0],combIm[2].split("/")[-1].split(".")[0]]})
                            else:
                                pathDictList.append({IMAGE: combIm[0], IMAGE_0:combIm[1],
                                                LABEL: combLa[0],  LABEL_0:combLa[1],
                                                "name":label,
                                                "patient":patientPath.split("/")[-1],
                                                "scan":[combIm[0].split("/")[-1].split(".")[0],combIm[1].split("/")[-1].split(".")[0]]})
                        else:
                            continue
            else:
                for imagePath in imagePaths:
                    labelFolder = imagePath.replace(IMAGE,LABEL).replace(".npz","/")
                    for label in self.getLabelsToSegment():
                        labelPath = os.path.join(labelFolder,f"{label}.npz")
                        if os.path.isfile(labelPath):
                            pathDictList.append({IMAGE: imagePath,
                                                LABEL: labelPath,
                                                "name":label,
                                                "patient":patientPath.split("/")[-1],
                                                "scan":imagePath.split("/")[-1].split(".")[0]})   
        return pathDictList,isHNCorNSCLC,readAsSequence
    def getDatasplit(self):
        dataSplit = {
                    TRAIN:[],
                    VALIDATION :[],
                    TEST:[]}
        
        datasets = [HNC,NSCLC,CT_DATA_512,SCT]        
        splits   = [TRAIN,VALIDATION,TEST]
        for dataset in datasets:
            dataPath =  os.path.join(DATA_PROCESSED_DIR,dataset)
            folderNames = os.listdir(dataPath)
            if (dataset==CT_DATA_512 and self.cfg.dataset.name=="synthetic"):
                for i, folder in enumerate(folderNames):
                    if i < len(folderNames)*self.cfg.dataset.train_split:
                        dataSplit["train"].append(os.path.join(dataPath,folder))
                    elif i >= len(folderNames)* self.cfg.dataset.train_split and i <= len(folderNames)*(self.cfg.dataset.train_split+self.cfg.dataset.validation_split):
                        dataSplit["validation"].append(os.path.join(dataPath,folder))
                    else:
                        dataSplit["test"].append(os.path.join(dataPath,folder))
            else:
                for split in splits:
                    for patient in self.cfg.dataset[split]:
                        if patient in folderNames:
                            dataSplit[split].append(os.path.join(dataPath,patient))

        return dataSplit
    
def getAugmentations(transforms,augmentation,subCropSize,subSampleNumber):    
    if augmentation[0]=="cropByPosNeg":                             
        transforms.append(
                RandCropByPosNegLabeld(
                    keys=[IMAGE, LABEL],
                    label_key=augmentation[3] if augmentation[3] else None,
                    spatial_size=tuple(subCropSize),
                    pos=augmentation[1],
                    neg=augmentation[2],
                    num_samples=subSampleNumber,
                    image_key=augmentation[4] if augmentation[4] else None,
                    image_threshold=0.1,
                    allow_smaller=True,
                ))
    elif augmentation[0]=="spatialCrop":
        transforms.append(
                RandSpatialCropSamplesd(
                    keys=[IMAGE, LABEL],
                    num_samples=subSampleNumber,
                    roi_size=tuple(subCropSize),
                    random_size=augmentation[1],
                )
        )
    elif augmentation[0]=="cropByLabelClass":                             
        transforms.append(
                RandCropByLabelClassesd(
                    keys=[IMAGE, LABEL],
                    label_key=LABEL,
                    spatial_size=tuple(subCropSize),
                    ratios=augmentation[1],
                    num_classes=len(augmentation[1]),
                    num_samples=subSampleNumber,
                    image_key=IMAGE,
                    image_threshold=0,
                    allow_smaller=True,
                ))
    elif augmentation[0] == "gaussianNoise":
        transforms.append(
            RandGaussianNoised(
                keys=IMAGE,
                prob=augmentation[1],
                std=augmentation[2],
                mean = augmentation[3],
            )
        )
    elif augmentation[0] == "adjustContrast":
        transforms.append(
            RandAdjustContrastd(
                keys=IMAGE,
                prob=augmentation[1],
                gamma=tuple(augmentation[2]),
            )
        )
    elif augmentation[0]=="gaussianSmooth":
        transforms.append(
            RandGaussianSmoothd(
                keys=IMAGE,
                prob=augmentation[1],
                sigma_x=tuple(augmentation[2]),
                sigma_y=tuple(augmentation[2]),
                sigma_z=tuple(augmentation[2]),
            )
        )
    elif augmentation[0]=="histogramShift":
        transforms.append(
            RandHistogramShiftd(
                keys=IMAGE,
                prob=augmentation[1],
                num_control_points=augmentation[2],
            )
        )
    elif augmentation[0]=="coarseDropout":
        transforms.append(
            RandCoarseDropoutd(
                keys=IMAGE,
                prob=augmentation[1],
                holes=augmentation[2],
                spatial_size=augmentation[3]
            )
        )
    elif augmentation[0]=="coarseShuffled":
        transforms.append(
            RandCoarseShuffled(
                keys=IMAGE,
                prob=augmentation[1],
                holes=augmentation[2],
                spatial_size=augmentation[3]
            )
        )
    elif augmentation[0]=="flip":
        transforms.append(
            RandAxisFlipd(
                keys=[IMAGE,LABEL],
                prob=augmentation[1],
            )
        )
    elif augmentation[0]=="rotate":
        transforms.append(
            RandRotated(
                keys=[IMAGE,LABEL],
                prob=augmentation[1],
                range_x=augmentation[2],
                range_y=augmentation[2],
                range_z=augmentation[2],
                mode=('bilinear','nearest')
            )
        )
    elif augmentation[0]=="zoom":
        transforms.append(
            RandZoomd(
                keys=[IMAGE,LABEL],
                prob=augmentation[1],
                min_zoom=tuple([augmentation[2][0]]*2),
                max_zoom=tuple([augmentation[2][1]]*2),
                mode=('bilinear','nearest')
            )
        )
    elif augmentation[0]=="affine":
        transforms.append(
            RandAffined(
                keys=[IMAGE,LABEL],
                prob=augmentation[1],
                mode=("bilinear", "nearest"),
                shear_range=tuple([augmentation[2]]*6),
                rotate_range=tuple([augmentation[3]]*3),
                scale_range=tuple([augmentation[4]]*3),
                translate_range=tuple([augmentation[5]]*3),
            )
        )
    elif augmentation[0]=="elastic":
        transforms.append(
            Rand3DElasticd(
                prob=augmentation[1],
                keys=augmentation[8],
                shear_range=tuple([augmentation[2]]*6),
                rotate_range=tuple([augmentation[3]]*3),
                scale_range=tuple([augmentation[4]]*3),
                translate_range=tuple([augmentation[5]]*3), 
                sigma_range=tuple(augmentation[6]),
                magnitude_range=tuple(augmentation[7]),
                mode=("bilinear", "nearest"),

            )
        )
    elif augmentation[0]=="distortion":
        transforms.append(
            RandGridDistortiond(
                keys=augmentation[4],
                prob=augmentation[1],
                num_cells=tuple([augmentation[2]]*3),
                distort_limit=tuple(augmentation[3]),
                mode=("bilinear", "nearest"),

                )
        )
    elif augmentation[0]=="copy":
        transforms.append(
            CopyItemsd(keys=augmentation[2],
                       times = augmentation[1]))
    elif augmentation[0]=="affineSequence":
        transforms.append(
            RandAffined(
                prob=augmentation[1],
                keys=augmentation[6],
                shear_range=tuple([augmentation[2]]*6),
                rotate_range=tuple([augmentation[3]]*3),
                scale_range=tuple([augmentation[4]]*3),
                translate_range=tuple([augmentation[5]]*3),
                mode=("bilinear", "nearest"),
            )
        )
    elif augmentation[0]=="deleteItems":
        transforms.append(
            DeleteItemsd(
                keys=augmentation[1],
            )
        )
    
    elif augmentation[0]=="stackSequence":
        transforms.append(
            ConcatItemsd(
                keys=augmentation[3],
                dim = augmentation[1],
                name=augmentation[2],
            )
        )
    elif augmentation[0]=="cropByPosNegSequence":                             
        transforms.append(
                RandCropByPosNegLabeld(
                    keys=augmentation[3],
                    label_key=LABEL,
                    spatial_size=tuple(subCropSize),
                    pos=augmentation[1],
                    neg=augmentation[2],
                    num_samples=subSampleNumber,
                    image_key=IMAGE,
                    image_threshold=0,
                    allow_smaller=True,
                ))
    elif augmentation[0]=="weightedCrop":
        transforms.append(
            RandWeightedCropd(
                keys=[IMAGE,LABEL],
                spatial_size=tuple(subCropSize),
                num_samples=subSampleNumber,
                w_key=LABEL,
            )
        )
    elif augmentation[0]=="addChannel":                             
        transforms.append(
                AddChanneld(
                    keys=[IMAGE,LABEL],
                ))
    return transforms
