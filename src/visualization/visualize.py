from this import d
import torch
import math
import numpy as np
import seaborn as sns
import pandas as pd
from tensorboardX import SummaryWriter
from monai.visualize import blend_images, matshow3d, plot_2d_or_3d_image
from config import *
from variables import *
import gc
from monai.transforms import (
    Compose,
    AsDiscrete,
    LabelToContour,
)

from matplotlib import pyplot as plt
from monai.transforms import Spacing,CropForeground
from matplotlib import gridspec
import warnings
warnings.filterwarnings("ignore")
from tqdm import tqdm

def tensorboardPlots(results,sessionDir,modes,plots):
    for patient in [*results.keys()]:
        for mode in modes:
            newResults={}
            newResults[patient]=patientToSingleChannel(results[patient],mode)
            # if plots.plots_3d:
            #     pass
            # if plots.plots_gif:
            #     pass
            # if plots.plots_sequence:
            # sw = SummaryWriter(log_dir=os.path.join(cfg.paths.session,f"figures/sequence"))
            sw = SummaryWriter(log_dir=os.path.join(sessionDir,f"figures/{patient}/sequence/{mode}"))
            makeSequencePlots(newResults,sw,everyX=10,alpha=0.5)
    return




# def findROI(image,groundtruth,prediction,name,roiDict,tolerance=2):
#     # if name in roiDict.keys():
#     #     minDepth,maxDepth = roiDict[name]
#     # else:
#     flattGroundtruth = groundtruth.reshape([math.prod(groundtruth.shape[:-1]),groundtruth.shape[-1]])
#     _,depthIndex = np.where(flattGroundtruth>0)
#     minDepth = min(depthIndex)-tolerance if min(depthIndex)-tolerance >0 else 0 
#     maxDepth = max(depthIndex)+tolerance if max(depthIndex)+tolerance <groundtruth.shape[-1] else groundtruth.shape[-1]
#     roiDict[name] = (minDepth,maxDepth)
#     image = image[:,:,:,:,minDepth:maxDepth]
#     groundtruth = groundtruth[:,:,:,:,minDepth:maxDepth]
#     prediction = prediction[:,:,:,:,minDepth:maxDepth]
#     return image,groundtruth,prediction,maxDepth-minDepth,roiDict

def makeGridPlot(image,groundtruth,prediction,step,everyX=1,sw=False,name="test",cmap='viridis'):
    nrow = image.shape[-1]
    ncol=5
    indexes=[]
    for i in range(nrow//everyX):
        if groundtruth[0, 0, :, :, i].max()>0:
            indexes.append(i) 
    nrow = 1 if len(indexes)==0 else len(indexes) 
    gtBlend = blend_images(image=image[0], label=groundtruth[0], alpha=groundtruth[0], cmap=cmap, rescale_arrays=True)
    predBlend = blend_images(image=image[0], label=prediction[0], alpha=prediction[0], cmap=cmap, rescale_arrays=True)
    fig = plt.figure(figsize=(ncol, nrow+2),dpi=200) 
    gs = gridspec.GridSpec(nrow, ncol,
            wspace=0.0, hspace=0.0, 
            top=1.-0.5/(nrow+1), bottom=0.5/(nrow+1), 
            left=0.5/(ncol+1), right=1-0.5/(ncol+1))
    plt.subplot(gs[0,0]).set_title(f"Image")
    plt.subplot(gs[0,1]).set_title(f"GT")
    plt.subplot(gs[0,2]).set_title(f"Pred")
    plt.subplot(gs[0,3]).set_title(f"GT")
    plt.subplot(gs[0,4]).set_title(f"Pred")

    for i, index in enumerate(indexes):
        plt.subplot(gs[i,0]).imshow(image[0, 0, :, :, index], cmap="gray")
        plt.subplot(gs[i,1]).imshow(groundtruth[0, 0, :, :, index])
        plt.subplot(gs[i,2]).imshow(prediction[0, 0, :, :, index])
        # switch the channel dim to the last dim
        plt.subplot(gs[i,3]).imshow(torch.moveaxis(gtBlend[:, :, :, index], 0, -1))
        # switch the channel dim to the last dim
        plt.subplot(gs[i,4]).imshow(torch.moveaxis(predBlend[:, :, :, index], 0, -1))
        plt.subplot(gs[i,0]).set_yticks([])
        plt.subplot(gs[i,0]).set_xticks([])
        for j in range(ncol):
            if j>0:
                plt.subplot(gs[i,j]).axis('off')
            plt.subplot(gs[i,j]).set_aspect('equal')
        plt.subplot(gs[i,0]).set_ylabel(f"{index}")


    if sw:
        sw.add_figure(tag=name,global_step=step, figure=fig)
    else:
        plt.show()


def makeGifPlot(image,groundtruth,prediction,step,sw,cmap='viridis'):
    gtBlend = blend_images(image=image[0], label=groundtruth[0], alpha=groundtruth[0], cmap=cmap, rescale_arrays=True).unsqueeze(0)
    predBlend = blend_images(image=image[0], label=prediction[0], alpha=prediction[0], cmap=cmap, rescale_arrays=True).unsqueeze(0)
    plot_2d_or_3d_image(data=gtBlend, step=step, writer=sw, frame_dim=-1, tag="groundtruth", max_frames=gtBlend.shape[-1]//5, max_channels=3)
    plot_2d_or_3d_image(data=predBlend, step=step, writer=sw, frame_dim=-1, tag="prediction", max_frames=gtBlend.shape[-1]//5, max_channels=3)
    
def make3DPlot(image,groundtruth,prediction,step,sw,spacing=(2,2,2)):
    downSample = Spacing(
        pixdim=spacing        
    )
    
    image = downSample(image.numpy().squeeze(0)).unsqueeze(0)
    groundtruth = downSample(groundtruth.numpy().squeeze(0)).unsqueeze(0)
    prediction = downSample(prediction.numpy().squeeze(0)).unsqueeze(0)

    plot_2d_or_3d_image(data=image, step=step, writer=sw, frame_dim=-1, tag="image3D", max_frames=image.shape[-1])
    plot_2d_or_3d_image(data=groundtruth, step=step, writer=sw, frame_dim=-1, tag="labelGroundtruth3D", max_frames=image.shape[-1], max_channels=3)
    plot_2d_or_3d_image(data=prediction, step=step, writer=sw, frame_dim=-1, tag="labelPrediction3D", max_frames=image.shape[-1], max_channels=3)

def setTicksOff(axs):
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])


def scanToSingleChannel(labels,mode):
    contourLabelList=[]
    toContour = LabelToContour()
    makeSingle = AsDiscrete(argmax=True)
    if mode=="contours":
        for i in range(labels.shape[1]):
            contourLabelList.append(toContour(labels[0,i].to(dtype=torch.float32)))
        contourLabels = torch.stack(contourLabelList,dim=0)
        singleChannel = makeSingle(contourLabels)
    elif mode=="labels":
        singleChannel=makeSingle(labels[0])
    return singleChannel.cpu().detach().numpy()
def patientToSingleChannel(patientResults,mode):
    singleChannelResults={}
    for labelType in ["pred","gt"]:
        singleChannelResults[labelType]={}
        for scan in tqdm([*patientResults[labelType].keys()]):
            singleChannel = scanToSingleChannel(patientResults[labelType][scan],mode)
            singleChannelResults[labelType][scan]=singleChannel
    singleChannelResults["ct"]=patientResults["ct"]
    return singleChannelResults


def makeSequencePlots(patientDict,sw,everyX=1,alpha=0.5):
    blendedG=[]
    blendedP=[]
    patient = [*patientDict.keys()][0]
    patientDict = patientDict[patient]
    for s,scan in tqdm(enumerate([*patientDict["gt"].keys()])):
        CT = patientDict["ct"][0:1,s].cpu().numpy()
        blendedG.append(blend_images(image=CT, label=patientDict["gt"][scan], alpha=alpha,rescale_arrays=True))
        if s>0:
            blendedP.append(blend_images(image=CT, label=patientDict["pred"][scan], alpha=alpha,rescale_arrays=True))

# CT_P = [image[0,0].unsqueeze(0).detach().cpu().numpy(),#Planning CT
#         image[0,3].unsqueeze(0).detach().cpu().numpy(),#Planning Mask
#                ]
# CT_1 = [image[0,1].unsqueeze(0).detach().cpu().numpy(),#CT_1
#         groundtruth[0].detach().cpu().numpy(),#GT_1
#         prediction[0].detach().cpu().numpy(),#PRE_1
#                ]
# CT_2 = [image[0,2].unsqueeze(0).detach().cpu().numpy(),#CT_2
#         groundtruth[1].detach().cpu().numpy(),#GT_2
#         prediction[1].detach().cpu().numpy(),#PRE_2
#                ]
# CTP_BLEND =  blend_images(image=CT_P[0], label=CT_P[1], alpha=alpha,rescale_arrays=True)
# GT1_BLEND =  blend_images(image=CT_1[0], label=CT_1[1], alpha=alpha,rescale_arrays=True)
# PRE1_BLEND = blend_images(image=CT_1[0], label=CT_1[2], alpha=alpha,rescale_arrays=True)
# GT2_BLEND =  blend_images(image=CT_2[0], label=CT_2[1], alpha=alpha,rescale_arrays=True)
# PRE2_BLEND = blend_images(image=CT_2[0], label=CT_2[2], alpha=alpha,rescale_arrays=True)

    nrow = CT.shape[-1]
    ncol=s+1
    indexes=[]
    for i in range(nrow):
        if patientDict["gt"][scan][0, :, :, i].max()>0:
            indexes.append(i)
    indexes=indexes[0::everyX]
    nrow = 1 if len(indexes)==0 else len(indexes)
    nrow = nrow*2
    fig = plt.figure(figsize=(ncol*2, (nrow+2)*2),dpi=150) 
    gs = gridspec.GridSpec(nrow, ncol,
            wspace=0.0, hspace=0.0, 
            top=1.-0.5/(nrow+1), bottom=0.5/(nrow+1), 
            left=0.5/(ncol+1), right=1-0.5/(ncol+1))
    # plt.subplot(gs[0,0]).set_title(f"PlaningCT")
    # plt.subplot(gs[0,1]).set_title(f"GT1")
    # plt.subplot(gs[0,2]).set_title(f"PRE1")
    # plt.subplot(gs[0,3]).set_title(f"GT2")
    # plt.subplot(gs[0,4]).set_title(f"PRE2")
    nrow=2
    for i, index in enumerate(indexes):
        fig = plt.figure(figsize=(ncol*3, nrow*3),dpi=150) 
        gs = gridspec.GridSpec(nrow, ncol,
                wspace=0.0, hspace=0.0, 
                top=1.-0.5/(nrow+1), bottom=0.5/(nrow+1), 
                left=0.5/(ncol+1), right=1-0.5/(ncol+1))

        for j in range(ncol):
            plt.subplot(gs[0,j]).set_title(f"Scan {j}")
            plt.subplot(gs[0,j]).imshow(torch.moveaxis(torch.from_numpy(blendedG[j][:, :, :, index]), 0, -1))
            plt.subplot(gs[0,j]).set_yticks([])
            plt.subplot(gs[0,j]).set_xticks([])
            if j>0:
                plt.subplot(gs[1,j]).imshow(torch.moveaxis(torch.from_numpy(blendedP[j-1][:, :, :, index]), 0, -1))
            else:
                plt.subplot(gs[1,j]).imshow(torch.moveaxis(torch.from_numpy(blendedG[j][:, :, :, index]), 0, -1))
                plt.subplot(gs[0,j]).set_ylabel(f"GT-Index={index}")
                plt.subplot(gs[1,j]).set_ylabel(f"Pred-Index={index}")
            # position = (gs[c+1,j]).get_position(fig).__array__()
            # plt.subplot(gs[c+1,j]).set_position([position[0][0],position[1][0],position[0][1],position[1][1]])
            plt.subplot(gs[1,j]).set_xticks([])
            plt.subplot(gs[1,j]).set_yticks([])
        if sw:
            sw.add_figure(tag=patient,figure=fig,global_step=i)


        # for j in range(ncol):
        #     if j>0:
        #         plt.subplot(gs[i,j]).axis('off')
        #     plt.subplot(gs[i,j]).set_aspect('equal')
        # plt.subplot(gs[i,0]).set_ylabel(f"{index}")


    else:
        plt.show()

def makeBoxPlots(df,title,dpi=200):
    plt.figure(dpi=dpi)
    fig=sns.boxplot(data=df)
    fig.set_title(title)
    return fig
