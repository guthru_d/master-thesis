"""
network.py - The core of the neural network
Defines the structure and memory operations
Modifed from STM: https://github.com/seoungwugoh/STM

The trailing number of a variable usually denote the stride
e.g. f16 -> encoded features with stride 16
"""

import gc
import numpy as np
from collections import Counter
import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Optional, Sequence, Tuple, Union, List

import warnings
from src.stm.stcn.resnetSTCN import ResnetSTCN
class EvalResnetSTCN(ResnetSTCN):
    def __init__(self,
                in_channels,
                out_channels,
                decoder_channels,
                key_backbone,
                value_backbone,
                key_dimension=64,
    ) -> None:
        super(EvalResnetSTCN,self).__init__(
            in_channels,
            out_channels,
            decoder_channels,
            key_backbone,
            value_backbone,
            key_dimension,
        )
    
    def forward(self, imageLabelStack,num_scans,num_labels,eval_mode="first",top_k=None,index=None):
        imageLabelStack = imageLabelStack.unsqueeze(2) # [1,num_scans+num_labels,1,X,Y,Z]
        Fs = imageLabelStack[:,:-num_labels]
        Ms = imageLabelStack[:,-num_labels:]
        k16, kf16_thin, kf16, kf8, kf4 = self.encode_key(Fs)
        affinities = self.getAffinities(k16,eval_mode,top_k)
        if eval_mode=="first":
            print("Evaluating in mode: First!")
            out = self.firstSegmentation(Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities)
        elif eval_mode=="previous":
            print("Evaluating in mode: Previous!")
            out = self.previousSegmentation(Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index)
        elif eval_mode=="every":
            print("Evaluating in mode: Every!")
            out = self.everySegmentation(Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index)
        elif eval_mode=="firstAndPrevious":
            print("Evaluating in mode: firstAndPrevious!")
            out = self.firstAndPreviousSegmentation(Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index)
        for i in range(num_scans-1):
            out[f"scan_{i+1}"]=torch.cat(out[f"scan_{i+1}"],dim=1)

        return out

    def getAffinities(self,k16,eval_mode="first",top_k=None):
        affinities=[]
        if eval_mode=="first":
            print("Calc Affinities M: First")
            for i in range(k16.shape[2]-1):
                affinities.append(self.memory.get_affinity(k16[:,:,0:1], k16[:,:,i+1],top_k))
        elif eval_mode=="previous":
            print("Calc Affinities M: Previous")
            for i in range(k16.shape[2]-1):
                affinities.append(self.memory.get_affinity(k16[:,:,i:i+1], k16[:,:,i+1],top_k))
        elif eval_mode=="every":
            print("Calc Affinities M: Every")
            for i in range(k16.shape[2]-1):
                affinities.append(self.memory.get_affinity(k16[:,:,0:i+1], k16[:,:,i+1],top_k))
                
        elif eval_mode=="firstAndPrevious":
            print("Calc Affinities M: firstAndPrevious")
            inital=k16[:,:,0:1]
            for i in range(k16.shape[2]-1):
                previous=k16[:,:,i:i+1]
                if i>1:
                    mem=torch.cat([inital,previous],dim=2)
                else:
                    mem=inital
                affinities.append(self.memory.get_affinity(mem, k16[:,:,i+1],top_k))
                
        elif eval_mode=="twoPrevious":
            print("Calc Affinities M: twoPrevious")
            for i in range(k16.shape[2]-1):
                if i==0:
                    affinities.append(self.memory.get_affinity(k16[:,:,i:i+1], k16[:,:,i+1],top_k))
                else:
                    affinities.append(self.memory.get_affinity(k16[:,:,i-1:i+1], k16[:,:,i+1],top_k))

        return affinities
    
    #(Ms[:,j]==1).any().item()
    def firstSegmentation(self,Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities):
        out = {f"scan_{i+1}":[] for i in range(Fs.shape[1]-1)}
        for j in range(Ms.shape[1]):
            ref_v = self.encode_value(Fs[:,0], kf16[:,0], Ms[:,j])

            for i, affinity in enumerate(affinities):
                prob= self.segment( 
                                            kf16_thin[:,i+1], kf8[:,i+1], kf4[:,i+1],
                                            ref_v, affinity)
                # prob = torch.where(prob>0.3,1,0)
                out[f"scan_{i+1}"].append(prob)
        return out
        
    def previousSegmentation(self,Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index):
        out = {f"scan_{i+1}":[] for i in range(Fs.shape[1]-1)}
        for i, affinity in enumerate(affinities):
            masks=Ms[:,int(i*index):int((i+1)*index)]
            for j in range(masks.shape[1]):
                ref_v = self.encode_value(Fs[:,i], kf16[:,i], masks[:,j])
                prob = self.segment( 
                                            kf16_thin[:,i+1], kf8[:,i+1], kf4[:,i+1],
                                            ref_v, affinity)
                out[f"scan_{i+1}"].append(prob)
        return out

    def twoPreviousSegmentation(self,Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index):
        pass

    def everySegmentation(self,Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index):
        out = {f"scan_{i+1}":[] for i in range(Fs.shape[1]-1)}
        ref_values_list=[]         
        for i, affinity in enumerate(affinities):
            masks=Ms[:,int(i*index):int((i+1)*index)]
            for j in range(masks.shape[1]):
                ref_values_list.append(self.encode_value(Fs[:,i], kf16[:,i], masks[:,j]))
                
        for i, affinity in enumerate(affinities):
            for j in range(masks.shape[1]):
                values= torch.cat(ref_values_list[j::int(index)][0:i+1], 2)
                
                prob = self.segment( 
                                            kf16_thin[:,i+1], kf8[:,i+1], kf4[:,i+1],
                                            values, affinity)
                                    
                out[f"scan_{i+1}"].append(prob)
        return out

    

    def firstAndPreviousSegmentation(self,Fs,Ms,kf16,kf16_thin,kf8,kf4,affinities,index):
        out = {f"scan_{i+1}":[] for i in range(Fs.shape[1]-1)}
        ref_values_list=[]         
        for i, affinity in enumerate(affinities):
            masks=Ms[:,int(i*index):int((i+1)*index)]
            for j in range(masks.shape[1]):
                ref_values_list.append(self.encode_value(Fs[:,i], kf16[:,i], masks[:,j]))
        for i, affinity in enumerate(affinities):
            for j in range(masks.shape[1]):
                first=ref_values_list[j::int(index)][0]
                previous=ref_values_list[j::int(index)][-1]
                if i>1:
                    values= torch.cat([first,previous], 2)
                else:
                    values=first
                
                prev_logits = self.segment( 
                                            kf16_thin[:,i+1], kf8[:,i+1], kf4[:,i+1],
                                            values, affinity)                    
                out[f"scan_{i+1}"].append(torch.argmax(F.softmax(prev_logits,dim=1),dim=1))
        return out
    

    def segment(self, qv16, qf8, qf4, mv16, affinity): 
        decoder_outputs = self.decoder(self.memory.readout(affinity, mv16, qv16), [qf8,qf4])
        logits = self.segmentation_head(decoder_outputs)
        prob = torch.sigmoid(logits)
        # logits = self.aggregate(prob)
        return prob
    
