"""
network.py - The core of the neural network
Defines the structure and memory operations
Modifed from STM: https://github.com/seoungwugoh/STM

The trailing number of a variable usually denote the stride
e.g. f16 -> encoded features with stride 16
"""

import math
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

# from src.stm.stcn.modules import *
from typing import Optional, Sequence, Tuple, Union, List

import warnings
from monai.networks.nets import EfficientNetBNFeatures
from monai.networks.nets.flexible_unet import UNetDecoder,SegmentationHead,_get_encoder_channels_by_backbone


encoder_feature_channel = {
    "efficientnet-b0": (16, 24, 40, 112, 320),
    "efficientnet-b1": (16, 24, 40, 112, 320),
    "efficientnet-b2": (16, 24, 48, 120, 352),
    "efficientnet-b3": (24, 32, 48, 136, 384),
    "efficientnet-b4": (24, 32, 56, 160, 448),
    "efficientnet-b5": (24, 40, 64, 176, 512),
    "efficientnet-b6": (32, 40, 72, 200, 576),
    "efficientnet-b7": (32, 48, 80, 224, 640),
    "efficientnet-b8": (32, 56, 88, 248, 704),
    "efficientnet-l2": (72, 104, 176, 480, 1376),
}



class Decoder(UNetDecoder):
    def __init__(self,
                spatial_dims: int,
                encoder_channels: Sequence[int],
                decoder_channels: Sequence[int],
                act: Union[str, tuple],
                norm: Union[str, tuple],
                dropout: Union[float, tuple],
                bias: bool,
                upsample: str,
                pre_conv: Optional[str],
                interp_mode: str,
                align_corners: Optional[bool],
                is_pad: bool,
    ):
        super().__init__(
                spatial_dims = spatial_dims,
                encoder_channels=encoder_channels,
                decoder_channels=decoder_channels,
                act=act,
                norm=norm,
                dropout=dropout,
                bias=bias,
                upsample=upsample,
                pre_conv=pre_conv,
                interp_mode=interp_mode,
                align_corners=align_corners,
                is_pad=is_pad,
        )
    def forward(self, feature, skips: List[torch.Tensor]):
            skip_connect = len(skips)
            x = feature
            for i, block in enumerate(self.blocks):
                if i < skip_connect:
                    skip = skips[i]
                else:
                    skip = None
                x = block(x, skip)

            return x
class MemoryReader(nn.Module):
    def __init__(self):
        super().__init__()
 
    def get_affinity(self, mk, qk):
        B, CK, T, H, W, D = mk.shape
        mk = mk.flatten(start_dim=2)
        qk = qk.flatten(start_dim=2)

        # See supplementary material
        a_sq = mk.pow(2).sum(1).unsqueeze(2)
        ab = mk.transpose(1, 2) @ qk

        affinity = (2*ab-a_sq) / math.sqrt(CK)   # B, THWD, HWD # Stided 16
        
        # softmax operation; aligned the evaluation style
        maxes = torch.max(affinity, dim=1, keepdim=True)[0]
        x_exp = torch.exp(affinity - maxes)
        x_exp_sum = torch.sum(x_exp, dim=1, keepdim=True)
        affinity = x_exp / x_exp_sum 

        return affinity

    def readout(self, affinity, mv, qv):
        B, CV, T, H, W, D = mv.shape

        mo = mv.view(B, CV, T*H*W*D) 
        mem = torch.bmm(mo, affinity) # Weighted-sum B, CV, HWD
        mem = mem.view(B, CV, H, W, D)

        mem_out = torch.cat([mem, qv], dim=1)

        return mem_out


class EfficientSTCN(nn.Module):
    def __init__(self,
                in_channels,
                out_channels,
                decoder_channels,
                backbone,
                key_dimension=64,
                single_object: bool = True,
                norm: Union[str, tuple] = ("batch", {"eps": 1e-3, "momentum": 0.1}),
                act: Union[str, tuple] = ("relu", {"inplace": True}),
                dropout: Union[float, tuple] = 0.0,
                decoder_bias: bool = False,
                upsample: str = "nontrainable",
                interp_mode: str = "nearest",
                is_pad: bool = True,
    ) -> None:
        self.single_object= single_object
        super(EfficientSTCN,self).__init__()
        if backbone not in encoder_feature_channel:
            raise ValueError(f"invalid model_name {backbone} found, must be one of {encoder_feature_channel.keys()}.")

        encoder_channels = _get_encoder_channels_by_backbone(backbone, in_channels)[:-2]

        adv_prop = "ap" in backbone
        
        self.key_encoder = KeyEncoder(in_channels=in_channels,
                                      model_name=backbone,
                                      norm=norm,
                                      adv_prop=adv_prop,
                                      spatial_dims=3,
                                      )
        
        
        if single_object:
            self.value_encoder = ValueEncoderSO(in_channels=in_channels+1,
                                                model_name=backbone,
                                                norm=norm,
                                                adv_prop=adv_prop,
                                                spatial_dims=3,
                                                final_out_channel=encoder_channels[-1],
                                                ) 
        else:
            pass
        # Projection from f16 feature space to key space
        self.key_proj = KeyProjection(encoder_channels[-1], keydim=key_dimension)

        # Compress f16 a bit to use in decoding later on
        self.key_comp = nn.Conv3d(encoder_channels[-1], encoder_channels[-1]//2, kernel_size=3, padding=1)

        self.memory = MemoryReader()
        self.decoder = Decoder(
                        spatial_dims=3,
                        encoder_channels=encoder_channels,
                        decoder_channels=decoder_channels,
                        act=act,
                        norm=norm,
                        dropout=dropout,
                        bias=decoder_bias,
                        upsample=upsample,
                        interp_mode=interp_mode,
                        pre_conv=None,
                        align_corners=None,
                        is_pad=is_pad,
        )
        self.segmentation_head = SegmentationHead(
                    spatial_dims=3,
                    in_channels=decoder_channels[-1],
                    out_channels=out_channels,
                    kernel_size=3,
                    act=None,
                )
    def aggregate(self, prob):
        new_prob = torch.cat([
            torch.prod(1-prob, dim=1, keepdim=True),
            prob
        ], 1).clamp(1e-7, 1-1e-7)
        logits = torch.log((new_prob /(1-new_prob)))
        return logits

    def encode_key(self, frame): 
        # input: b*t*c*h*w*d
        b, t = frame.shape[:2]

        f4,f8,f16 = self.key_encoder(frame.flatten(start_dim=0, end_dim=1))
        k16 = self.key_proj(f16)
        f16_thin = self.key_comp(f16)

        # B*C*T*H*W*D
        k16 = k16.view(b, t, *k16.shape[-4:]).transpose(1, 2).contiguous()

        # B*T*C*H*W*D
        f16_thin = f16_thin.view(b, t, *f16_thin.shape[-4:])
        f16 = f16.view(b, t, *f16.shape[-4:])
        f8 = f8.view(b, t, *f8.shape[-4:])
        f4 = f4.view(b, t, *f4.shape[-4:])

        return k16, f16_thin, f16, f8, f4

    def encode_value(self, frame, kf16, mask, other_mask=None): 
        # Extract memory key/value for a frame
        if self.single_object:
            f16 = self.value_encoder(frame, kf16, mask)
        else:
            f16 = self.value_encoder(frame, kf16, mask, other_mask)
        return f16.unsqueeze(2) # B*512*T*H*W*D

    def segment(self, qk16, qv16, qf8, qf4, mk16, mv16, selector=None): 
        # q - query, m - memory
        # qv16 is f16_thin above
        affinity = self.memory.get_affinity(mk16, qk16)
        
        if self.single_object:
            decoder_outputs = self.decoder(self.memory.readout(affinity, mv16, qv16), [qf8,qf4])
            logits = self.segmentation_head(decoder_outputs)
            prob = torch.sigmoid(logits)
        else:
            logits = torch.cat([
                self.decoder(self.memory.readout(affinity, mv16, qv16), [qf8,qf4]),
                self.decoder(self.memory.readout(affinity, mv16, qv16), [qf8,qf4]),
            ], 1)

            prob = torch.sigmoid(logits)
            prob = prob * selector.unsqueeze(2).unsqueeze(2)

        logits = self.aggregate(prob)
        prob = F.softmax(logits, dim=1)[:, 1:]
        
        return logits, prob

    def forward(self, imageLabelStack):
        out = {}
        imageLabelStack = imageLabelStack.unsqueeze(2)
        Fs = imageLabelStack[:,:-1]
        Ms = imageLabelStack[:,-1]

        k16, kf16_thin, kf16, kf8, kf4 = self.encode_key(Fs)
  
        if self.single_object:

            ref_v = self.encode_value(Fs[:,0], kf16[:,0], Ms)
            prev_logits, prev_mask = self.segment( 
                                                k16[:,:,1], kf16_thin[:,1], kf8[:,1], kf4[:,1],
                                                k16[:,:,0:1], ref_v)
            prev_v = self.encode_value(Fs[:,1], kf16[:,1], prev_mask)
            values = torch.cat([ref_v, prev_v], 2)
            del ref_v
            this_logits, this_mask = self.segment( 
                                                k16[:,:,2], kf16_thin[:,2], kf8[:,2], kf4[:,2],
                                                k16[:,:,0:2], values)

            out['prev_logits'] = prev_logits
            out['this_logits'] = this_logits
        return torch.cat([prev_logits,this_logits],dim=1)
        # if mode == 'encode_key':
        #     return self.encode_key(*args, **kwargs)
        # elif mode == 'encode_value':
        #     return self.encode_value(*args, **kwargs)
        # elif mode == 'segment':
        #     return self.segment(*args, **kwargs)
        # else:
        #     raise NotImplementedError

class ValueEncoderSO(nn.Module):
    def __init__(self,
                 in_channels: int,
                 model_name: str,
                 norm: Union[str, tuple],
                 adv_prop: str,
                 final_out_channel: int,
                 spatial_dims: int=3,
    ):

        super(ValueEncoderSO, self).__init__()

        self.efficientEncoder = EfficientNetBNFeatures(
            model_name=model_name,
            in_channels=in_channels,
            spatial_dims=spatial_dims,
            norm=norm,
            adv_prop=adv_prop
        )
        self.efficientEncoder
        self.block0 = torch.nn.Sequential(*[self.efficientEncoder._conv_stem,self.efficientEncoder._conv_stem_padding,self.efficientEncoder._bn0])
        self.blocks = torch.nn.Sequential(*[self.efficientEncoder._blocks[i] for i in range(3)])
        self.fuser = FeatureFusionBlock(final_out_channel*2, final_out_channel//2)
        del self.efficientEncoder


    def forward(self, image, key_f16, mask):
        x = torch.cat([image,mask],1)
        x = self.block0(x)
        for block in self.blocks:
            x = block(x)
        x = self.fuser(x,key_f16) 
        return x
    
class KeyEncoder(nn.Module):
    def __init__(self,
                 in_channels: int,
                 model_name: str,
                 norm: Union[str, tuple],
                 adv_prop: str,
                 spatial_dims: int=3,
    ):

        super(KeyEncoder, self).__init__()

        self.efficientEncoder = EfficientNetBNFeatures(
            model_name=model_name,
            in_channels=in_channels,
            spatial_dims=spatial_dims,
            norm=norm,
            adv_prop=adv_prop
        )
        self.block0 = torch.nn.Sequential(*[self.efficientEncoder._conv_stem,self.efficientEncoder._conv_stem_padding,self.efficientEncoder._bn0])
        self.blocks = torch.nn.Sequential(*[self.efficientEncoder._blocks[i] for i in range(3)])
        del self.efficientEncoder

    def forward(self, x):
        features = []
        x = self.block0(x)
        for block in self.blocks:
            x = block(x)
            features.append(x)
        return features
    
    
class ResBlock(nn.Module):
    def __init__(self, indim, outdim=None):
        super(ResBlock, self).__init__()
        if outdim == None:
            outdim = indim
        if indim == outdim:
            self.downsample = None
        else:
            self.downsample = nn.Conv3d(indim, outdim, kernel_size=3, padding=1)
 
        self.conv1 = nn.Conv3d(indim, outdim, kernel_size=3, padding=1)
        self.conv2 = nn.Conv3d(outdim, outdim, kernel_size=3, padding=1)
 
    def forward(self, x):
        r = self.conv1(F.relu(x))
        r = self.conv2(F.relu(r))
        
        if self.downsample is not None:
            x = self.downsample(x)

        return x + r


class FeatureFusionBlock(nn.Module):
    def __init__(self, indim, outdim):
        super().__init__()

        self.block1 = ResBlock(indim, outdim)
        # self.attention = cbam.CBAM(outdim)
        self.block2 = ResBlock(outdim, outdim)

    def forward(self, x, f16):
        x = torch.cat([x, f16], 1)
        x = self.block1(x)
        x = self.block2(x)

        return x
    

class KeyProjection(nn.Module):
    def __init__(self, indim, keydim):
        super().__init__()
        self.key_proj = nn.Conv3d(indim, keydim, kernel_size=3, padding=1)

        nn.init.orthogonal_(self.key_proj.weight.data)
        nn.init.zeros_(self.key_proj.bias.data)
    
    def forward(self, x):
        return self.key_proj(x)