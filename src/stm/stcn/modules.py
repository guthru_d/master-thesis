"""
modules.py - This file stores the rathering boring network blocks.
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


from monai.networks.blocks import *
from monai.networks.nets import ResNet

from torchvision import models
models.resnet50
from monai.networks.blocks import Convolution


class ResBlock(nn.Module):
    def __init__(self, indim, outdim=None):
        super(ResBlock, self).__init__()
        if outdim == None:
            outdim = indim
        if indim == outdim:
            self.downsample = None
        else:
            self.downsample = nn.Conv3d(indim, outdim, kernel_size=3, padding=1)
 
        self.conv1 = nn.Conv3d(indim, outdim, kernel_size=3, padding=1)
        self.conv2 = nn.Conv3d(outdim, outdim, kernel_size=3, padding=1)
 
    def forward(self, x):
        r = self.conv1(F.relu(x))
        r = self.conv2(F.relu(r))
        
        if self.downsample is not None:
            x = self.downsample(x)

        return x + r


class FeatureFusionBlock(nn.Module):
    def __init__(self, indim, outdim):
        super().__init__()

        self.block1 = ResBlock(indim, outdim)
        # self.attention = cbam.CBAM(outdim)
        self.block2 = ResBlock(outdim, outdim)

    def forward(self, x, f16):
        x = torch.cat([x, f16], 1)
        x = self.block1(x)
        x = self.block2(x)

        return x


# Single object version, used only in static image pretraining
# This will be loaded and modified into the multiple objects version later (in stage 1/2/3)
# See model.py (load_network) for the modification procedure
class ValueEncoderSO(nn.Module):
    def __init__(self,
                spatial_dims,
                in_channels,
                block="basic",
                layers=[2,2,2,2],
                block_inplanes=[64,128,256,512],
                conv1_t_size=7,
                conv1_t_stride=2,
                ):
        super().__init__()

        # resnet = mod_resnet.resnet18(pretrained=True, extra_chan=2)
        # resnet = ResNet(block=block,
        #                 layers=layers,
        #                 block_inplanes=block_inplanes,
        #                 spatial_dims=spatial_dims,
        #                 n_input_channels=in_channels,
        #                 conv1_t_size=conv1_t_size,
        #                 conv1_t_stride=conv1_t_stride,
        #                 )
        # resnet = mod_resnet.resnet18(pretrained=True, extra_chan=1)
        # self.conv1 = resnet.conv1
        # self.bn1 = resnet.bn1
        # self.relu = resnet.relu  # 1/2, 64
        # self.maxpool = resnet.maxpool

        # self.layer1 = resnet.layer1 # 1/4, 64
        # self.layer2 = resnet.layer2 # 1/8, 128
        # self.layer3 = resnet.layer3 # 1/16, 256

        self.conv1 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=in_channels,
            out_channels=64,
            adn_ordering="ADN",
            strides=2
        )
        self.layer1 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=64,
            out_channels=256,
            adn_ordering="ADN",
            strides=2
        )
        self.layer2 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=256,
            out_channels=512,
            adn_ordering="ADN",
            strides=2
        )
        self.layer3 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=512,
            out_channels=1024,
            adn_ordering="ADN",
            strides=2
        )


        # self.fuser = FeatureFusionBlock(1024 + 256, 512)
        self.fuser = FeatureFusionBlock(2048, 512)

    def forward(self, image, key_f16, mask):
        # key_f16 is the feature from the key encoder

        f = torch.cat([image, mask], 1)

        x = self.conv1(f)
        # x = self.bn1(x)
        # x = self.relu(x)   # 1/2, 64
        # x = self.maxpool(x)  # 1/4, 64
        x = self.layer1(x)   # 1/4, 64
        x = self.layer2(x) # 1/8, 128
        x = self.layer3(x) # 1/16, 256

        x = self.fuser(x, key_f16)

        return x


# Multiple objects version, used in other times
class ValueEncoder(nn.Module):
    def __init__(self,
                spatial_dims,
                in_channels,
                block="basic",
                layers=[2,2,2,2],
                block_inplanes=[64,128,256,512],
                conv1_t_size=7,
                conv1_t_stride=2,
                ):
        super().__init__()

        # resnet = mod_resnet.resnet18(pretrained=True, extra_chan=2)
        # resnet = ResNet(block=block,
        #                 layers=layers,
        #                 block_inplanes=block_inplanes,
        #                 spatial_dims=spatial_dims,
        #                 n_input_channels=in_channels,
        #                 conv1_t_size=conv1_t_size,
        #                 conv1_t_stride=conv1_t_stride,
        #                 )
        # self.conv1 = resnet.conv1
        # self.bn1 = resnet.bn1
        # self.relu = resnet.relu  # 1/2, 64
        # self.maxpool = resnet.maxpool

        # self.layer1 = resnet.layer1 # 1/4, 64
        # self.layer2 = resnet.layer2 # 1/8, 128
        # self.layer3 = resnet.layer3 # 1/16, 256
        self.conv1 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=in_channels,
            out_channels=64,
            adn_ordering="ADN",
            strides=2
        )
        self.layer1 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=64,
            out_channels=256,
            adn_ordering="ADN",
            strides=2
        )
        self.layer2 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=256,
            out_channels=512,
            adn_ordering="ADN",
            strides=2
        )
        self.layer3 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=512,
            out_channels=1024,
            adn_ordering="ADN",
            strides=2
        )
        self.fuser = FeatureFusionBlock(1024 + 256, 512)

    def forward(self, image, key_f16, mask, other_masks):
        # key_f16 is the feature from the key encoder

        f = torch.cat([image, mask, other_masks], 1)

        x = self.conv1(f)
        # x = self.bn1(x)
        # x = self.relu(x)   # 1/2, 64
        # x = self.maxpool(x)  # 1/4, 64
        x = self.layer1(x)   # 1/4, 64
        x = self.layer2(x) # 1/8, 128
        x = self.layer3(x) # 1/16, 256

        x = self.fuser(x, key_f16)

        return x
 

class KeyEncoder(nn.Module):
    def __init__(self,
                 spatial_dims,
                 in_channels,
                 block="bottleneck",
                 layers=[3,4,6,3],
                 block_inplanes=[64,128,256,512],
                 conv1_t_size=7,
                 conv1_t_stride=2,
                ):
        super().__init__()
        resnet = ResNet(block=block,
                        layers=layers,
                        block_inplanes=block_inplanes,
                        spatial_dims=spatial_dims,
                        n_input_channels=in_channels,
                        conv1_t_size=conv1_t_size,
                        conv1_t_stride=conv1_t_stride,
                        )
        self.conv1 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=in_channels,
            out_channels=64,
            adn_ordering="ADN",
            strides=2
        )
        self.res2 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=64,
            out_channels=256,
            adn_ordering="ADN",
            strides=2
        )
        self.layer2 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=256,
            out_channels=512,
            adn_ordering="ADN",
            strides=2
        )
        self.layer3 = Convolution(
            spatial_dims=spatial_dims,
            in_channels=512,
            out_channels=1024,
            adn_ordering="ADN",
            strides=2
        )
        
        # self.conv1 = resnet.conv1
        # self.bn1 = resnet.bn1
        # self.relu = resnet.relu  # 1/2, 64
        # self.maxpool = resnet.maxpool

        # self.res2 = resnet.layer1 # 1/4, 256
        # self.layer2 = resnet.layer2 # 1/8, 512
        # self.layer3 = resnet.layer3 # 1/16, 1024

        
    def forward(self, f):
        x = self.conv1(f) 
        # x = self.bn1(x)
        # x = self.relu(x)   # 1/2, 64
        # x = self.maxpool(x)  # 1/4, 64
        f4 = self.res2(x)   # 1/4, 256
        f8 = self.layer2(f4) # 1/8, 512
        f16 = self.layer3(f8) # 1/16, 1024

        return f16, f8, f4


class UpsampleBlock(nn.Module):
    def __init__(self, skip_c, up_c, out_c, scale_factor=2):
        super().__init__()
        self.skip_conv = nn.Conv3d(skip_c, up_c, kernel_size=3, padding=1)
        self.out_conv = ResBlock(up_c, out_c)
        self.scale_factor = scale_factor

    def forward(self, skip_f, up_f):
        x = self.skip_conv(skip_f)
        x = x + F.interpolate(up_f, scale_factor=self.scale_factor, mode='trilinear', align_corners=False)
        x = self.out_conv(x)
        return x


class KeyProjection(nn.Module):
    def __init__(self, indim, keydim):
        super().__init__()
        self.key_proj = nn.Conv3d(indim, keydim, kernel_size=3, padding=1)

        nn.init.orthogonal_(self.key_proj.weight.data)
        nn.init.zeros_(self.key_proj.bias.data)
    
    def forward(self, x):
        return self.key_proj(x)