"""
network.py - The core of the neural network
Defines the structure and memory operations
Modifed from STM: https://github.com/seoungwugoh/STM

The trailing number of a variable usually denote the stride
e.g. f16 -> encoded features with stride 16
"""

import math
from numpy import block

import torch
import torch.nn as nn
import torch.nn.functional as F

from src.stm.stcn.modules import *
from torchvision import models
models.resnet50
class Decoder(nn.Module):
    def __init__(self,
                 out_channels):
        super().__init__()
        self.compress = ResBlock(1024, 512)
        self.up_16_8 = UpsampleBlock(512, 512, 256) # 1/16 -> 1/8
        self.up_8_4 = UpsampleBlock(256, 256, 256) # 1/8 -> 1/4

        self.pred = nn.Conv3d(256, out_channels, kernel_size=(3,3,3), padding=(1,1,1), stride=1)

    def forward(self, f16, f8, f4):
        x = self.compress(f16)
        x = self.up_16_8(f8, x)
        x = self.up_8_4(f4, x)

        x = self.pred(F.relu(x))
        
        x = F.interpolate(x, scale_factor=4, mode='trilinear', align_corners=False)
        return x


class MemoryReader(nn.Module):
    def __init__(self):
        super().__init__()
 
    def get_affinity(self, mk, qk):
        B, CK, T, H, W, D = mk.shape
        mk = mk.flatten(start_dim=2)
        qk = qk.flatten(start_dim=2)

        # See supplementary material
        a_sq = mk.pow(2).sum(1).unsqueeze(2)
        ab = mk.transpose(1, 2) @ qk

        affinity = (2*ab-a_sq) / math.sqrt(CK)   # B, THWD, HWD # Stided 16
        
        # softmax operation; aligned the evaluation style
        maxes = torch.max(affinity, dim=1, keepdim=True)[0]
        x_exp = torch.exp(affinity - maxes)
        x_exp_sum = torch.sum(x_exp, dim=1, keepdim=True)
        affinity = x_exp / x_exp_sum 

        return affinity

    def readout(self, affinity, mv, qv):
        B, CV, T, H, W, D = mv.shape

        mo = mv.view(B, CV, T*H*W*D) 
        mem = torch.bmm(mo, affinity) # Weighted-sum B, CV, HWD
        mem = mem.view(B, CV, H, W, D)

        mem_out = torch.cat([mem, qv], dim=1)

        return mem_out


class STCN(nn.Module):
    def __init__(self, 
                 single_object,
                 out_channels,
                 spatial_dims=3,
                 in_channels=1,
                 key_layers=[3, 4, 6, 3],
                 key_block = "bottleneck",
                 key_block_inplanes=[64,128,256,512]
                 ):
        super().__init__()
        self.single_object = single_object
        self.in_channels=in_channels
        self.spatial_dims=spatial_dims
        self.key_encoder = KeyEncoder(self.spatial_dims,
                                      self.in_channels,
                                      block=key_block,
                                      layers=key_layers,
                                      block_inplanes=key_block_inplanes,
                                      )
        if single_object:
            self.value_encoder = ValueEncoderSO(self.spatial_dims,self.in_channels+1) 
        else:
            self.value_encoder = ValueEncoder(self.spatial_dims,self.in_channels+2) 

        # Projection from f16 feature space to key space
        self.key_proj = KeyProjection(1024, keydim=64)

        # Compress f16 a bit to use in decoding later on
        self.key_comp = nn.Conv3d(1024, 512, kernel_size=3, padding=1)

        self.memory = MemoryReader()
        self.decoder = Decoder(out_channels)

    def aggregate(self, prob):
        new_prob = torch.cat([
            torch.prod(1-prob, dim=1, keepdim=True),
            prob
        ], 1).clamp(1e-7, 1-1e-7)
        logits = torch.log((new_prob /(1-new_prob)))
        return logits

    def encode_key(self, frame): 
        # input: b*t*c*h*w*d
        b, t = frame.shape[:2]

        f16, f8, f4 = self.key_encoder(frame.flatten(start_dim=0, end_dim=1))
        k16 = self.key_proj(f16)
        f16_thin = self.key_comp(f16)

        # B*C*T*H*W*D
        k16 = k16.view(b, t, *k16.shape[-4:]).transpose(1, 2).contiguous()

        # B*T*C*H*W*D
        f16_thin = f16_thin.view(b, t, *f16_thin.shape[-4:])
        f16 = f16.view(b, t, *f16.shape[-4:])
        f8 = f8.view(b, t, *f8.shape[-4:])
        f4 = f4.view(b, t, *f4.shape[-4:])

        return k16, f16_thin, f16, f8, f4

    def encode_value(self, frame, kf16, mask, other_mask=None): 
        # Extract memory key/value for a frame
        if self.single_object:
            f16 = self.value_encoder(frame, kf16, mask)
        else:
            f16 = self.value_encoder(frame, kf16, mask, other_mask)
        return f16.unsqueeze(2) # B*512*T*H*W*D

    def segment(self, qk16, qv16, qf8, qf4, mk16, mv16, selector=None): 
        # q - query, m - memory
        # qv16 is f16_thin above
        affinity = self.memory.get_affinity(mk16, qk16)
        
        if self.single_object:
            logits = self.decoder(self.memory.readout(affinity, mv16, qv16), qf8, qf4)
            # prob = torch.sigmoid(logits)
        else:
            logits = torch.cat([
                self.decoder(self.memory.readout(affinity, mv16[:,0], qv16), qf8, qf4),
                self.decoder(self.memory.readout(affinity, mv16[:,1], qv16), qf8, qf4),
            ], 1)

            prob = torch.sigmoid(logits)
            prob = prob * selector.unsqueeze(2).unsqueeze(2)

        # #prob = F.softmax(logits, dim=1)[:, 1:]
        # logitsA = self.aggregate(prob)
        prob = F.softmax(logits, dim=1)
        prob = torch.argmax(prob,1,keepdim=True)


        # prob = F.softmax(logits, dim=1)[:, 1:]

        
        return logits, prob

    def forward(self, imageLabelStack):
        out = {}
        Fs = imageLabelStack[:,0:imageLabelStack.shape[1]//2].unsqueeze(2)
        Ms = imageLabelStack[:,imageLabelStack.shape[1]//2:].unsqueeze(2)
        k16, kf16_thin, kf16, kf8, kf4 = self.encode_key(Fs)
  
        if self.single_object:

            ref_v = self.encode_value(Fs[:,0], kf16[:,0], Ms[:,0])
            prev_logits, prev_mask = self.segment( 
                                                k16[:,:,1], kf16_thin[:,1], kf8[:,1], kf4[:,1], 
                                                k16[:,:,0:1], ref_v)
            prev_v = self.encode_value(Fs[:,1], kf16[:,1], prev_mask)
            values = torch.cat([ref_v, prev_v], 2)
            del ref_v
            this_logits, this_mask = self.segment( 
                                                k16[:,:,2], kf16_thin[:,2], kf8[:,2], kf4[:,2], 
                                                k16[:,:,0:2], values)

            # out['mask_1'] = prev_mask
            # out['mask_2'] = this_mask
            # out['logits_1'] = prev_logits
            # out['logits_2'] = this_logits
            out['prev_logits'] = prev_logits
            out['this_logits'] = this_logits
        return torch.cat([*out.values()])
        # if mode == 'encode_key':
        #     return self.encode_key(*args, **kwargs)
        # elif mode == 'encode_value':
        #     return self.encode_value(*args, **kwargs)
        # elif mode == 'segment':
        #     return self.segment(*args, **kwargs)
        # else:
        #     raise NotImplementedError

