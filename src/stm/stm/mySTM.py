import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from typing import Optional, Sequence, Tuple, Union
import warnings
from monai.networks.blocks import Convolution, ResidualUnit, UpSample
from monai.networks.layers.factories import Act, Norm
from src.stm.stm.modules import *

class STM(nn.Module):
    def __init__(self,
                spatial_dims,
                in_channels,
                out_channels,
                channels,
                strides,
                kernel_size: Union[Sequence[int], int] = 3,
                num_res_units: int = 0,
                act: Union[Tuple, str] = Act.PRELU,
                norm: Union[Tuple, str] = Norm.INSTANCE,
                dropout: float = 0.0,
                bias: bool = True,
                adn_ordering: str = "NDA"
        ):        
        super(STM, self).__init__()

        self.encoderM = Encoder(
                        spatial_dims,
                        in_channels,
                        channels,
                        strides,
                        kernel_size,
                        num_res_units,
                        act,
                        norm,
                        dropout,
                        bias,
                        adn_ordering,
        )
        self.encoderQ = Encoder(
                        spatial_dims,
                        in_channels,
                        channels,
                        strides,
                        kernel_size,
                        num_res_units,
                        act,
                        norm,
                        dropout,
                        bias,
                        adn_ordering,
        )
        self.decoder = Decoder(
                        spatial_dims,
                        out_channels,
                        channels,
                        strides,
                        kernel_size,
                        num_res_units,
                        act,
                        norm,
                        dropout,
                        bias,
                        adn_ordering,
        )
        self.memoryUnit = MemoryStorageAndReader()


    def to_memory(self, x):
        for i in range(x.shape[1]):
            keyM, valueM, _, _, _, _ = self.encoderM(x[:, i, :, :, :, :])
            self.memoryUnit.record_to_memory(keyM, valueM)

    def segment(self, input):
        self.keyQ, self.valueQ, skip1, skip2, skip3, skip4 = self.encoderQ(input)
        read = self.memoryUnit.read(self.keyQ, self.valueQ)
        output = self.decoder(read, skip1, skip2, skip3, skip4)
        return output
        
    def forward(self,inputs):
        query = inputs[0]
        previous_data = inputs[1]

        torch.autograd.set_detect_anomaly(True)
        self.to_memory(previous_data)
        output = self.segment(query)
        return output

class MemoryStorageAndReader:

    def __init__(self):
        self.keysM = None
        self.valuesM = None

    def record_to_memory(self, key, value):
        if self.keysM is not None:
            self.keysM = torch.cat((self.keysM, key), dim=0)
            self.valuesM = torch.cat((self.valuesM, value), dim=0)
        else:
            self.keysM = key
            self.valuesM = value

    def reset_memory(self):
        self.keysM = None
        self.valuesM = None

    def read(self, keyQ, valueQ):
        # T - time, C - channels, H - height, W - width, D - depth
        # TODO: implement computation for n-size minibatch
        T, C_keys, H, W, D = self.keysM.size()
        _, C_values, _, _, _ = self.valuesM.size()

        # Compare keys
        keyQ = keyQ.view(C_keys, H*W*D)
        keyQ = keyQ.permute(1, 0)
        keysM = self.keysM.permute(1, 0, 2, 3, 4)
        keysM = keysM.reshape(C_keys, T*H*W*D)
        key_similarity = torch.mm(keyQ, keysM)

        # Normalize and apply softmax
        key_similarity = key_similarity/math.sqrt(C_keys)
        key_similarity = F.softmax(key_similarity, dim=0)

        # Extract appropriate value from memory
        valueM = self.valuesM.permute(0, 2, 3, 4, 1)
        valueM = valueM.reshape(T*H*W*D, C_values)
        extracted_value = torch.mm(key_similarity, valueM)
        extracted_value = extracted_value.view(H, W, D, C_values)

        # Concatenate with query value
        read = torch.cat([valueQ, extracted_value.permute(3, 0, 1, 2).unsqueeze_(0)], dim=1)

        return read



