import torch
import torch.nn as nn
import torch.nn.functional as F
import math


def conv_block(in_channels: int, 
               out_channels: int, 
               kernel_size: int,
               stride: int = 1, 
               padding: int = 2,
               ):
    return nn.Sequential(
        nn.Conv3d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
        nn.BatchNorm3d(num_features=out_channels),
        nn.PReLU()
    )


class ResBlock(nn.Module):
    def __init__(
        self,
        channels: list,
        kernel_size: int,
    ):
        super(ResBlock, self).__init__()
        
        conv_blocks = []
        for i in range(len(channels)-1):
            conv_blocks.append(conv_block(
                in_channels=channels[i],
                out_channels=channels[i+1],
                kernel_size=kernel_size,   
            )
                
            )
        self.conv_blocks = nn.Sequential(*conv_blocks)

    def forward(self, x, input_from_skipped_connection=None):
        if input_from_skipped_connection is not None:
            x_and_skip = torch.cat((x, input_from_skipped_connection), dim=1)
            conv_output = self.conv_blocks(x_and_skip)
        else:
            conv_output = self.conv_blocks(x)

        try:
            res_output = conv_output + x.expand(-1, conv_output.shape[1], -1, -1, -1)
        except RuntimeError:
            res_output = conv_output + x_and_skip.expand(-1, conv_output.shape[1], -1, -1, -1)
        return res_output


class DownsamplingConv(nn.Module):

    def __init__(
        self,
        in_channels_down: int,
        in_channels_res: int,
        out_channels_down: int,
        out_channels_res: int,
        kernel_size: int,
        stride: int,
        padding: int,
        num_res_units: int,
    ):
        super(DownsamplingConv, self).__init__()

        if num_res_units>0:
            channels = [in_channels_res for i in range(num_res_units+1)]
            channels.append(out_channels_res)
            self.residual_block = ResBlock(
                                    channels,
                                    kernel_size,
                                    )
        else:
            self.residual_block = conv_block(in_channels_res,
                                             out_channels_res,
                                             kernel_size,
                                             )


        self.downsample = conv_block(in_channels_down,
                                     out_channels_down,
                                     kernel_size=2,
                                     stride=stride,
                                     padding=0)
    def forward(self, x):
        res_output = self.residual_block(x)
        downsampled = self.downsample(res_output)
        return res_output, downsampled


class UpsamplingConv(nn.Module):

    def __init__(
        self,
        in_channels_up: int,
        in_channels_res: int,
        out_channels_up: int,
        out_channels_res: int,
        kernel_size: int,
        stride: int,
        padding: int,
        num_res_units: int,
    ):
        
        super(UpsamplingConv, self).__init__()

        channels = [in_channels_res for i in range(num_res_units+1)]
        channels.append(out_channels_res)
        self.residual_block = ResBlock(
                                channels,
                                kernel_size,
                                )


        self.upsample = nn.Sequential(
            torch.nn.ConvTranspose3d(
                in_channels_up,
                out_channels_up,
                kernel_size=2,
                stride=stride,
            ),
            nn.BatchNorm3d(num_features=out_channels_up),
            nn.PReLU()
        )
        
    def forward(self, x, input_skip):
        upsampled = self.upsample(x)
        res_output = self.residual_block(upsampled, input_skip)
        return res_output


class Encoder(nn.Module):
    def __init__(self,
                 in_channels: int,
                 channels: list,
                 kernel_size: int,
                 stride: int = 2,
                 padding: int = 0,
                 num_res_units: int = 2,
                 key_out_channels: int = 32,
                 value_out_channels: int = 128,
    ):

        super(Encoder, self).__init__()
        
        # self.inital_block = conv_block(
        #     in_channels=in_channels,
        #     out_channels=channels[0],
        #     kernel_size=kernel_size,
        # )
        self.inital_block = DownsamplingConv(
            in_channels_res=in_channels,
            in_channels_down=channels[0]//2,
            out_channels_down=channels[0],
            out_channels_res=channels[0]//2,
            kernel_size=kernel_size,
            stride=stride,
            num_res_units=0,
            padding=padding,
        )

        encode_blocks = []
        for i in range(len(channels)-1):
            encode_blocks.append(DownsamplingConv(
                in_channels_down=channels[i],
                in_channels_res=channels[i],
                out_channels_res=channels[i],
                out_channels_down=channels[i+1],
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                num_res_units=num_res_units,
            )
                
            )
        self.encode_blocks = nn.Sequential(*encode_blocks)
        
        
        self.last_block = ResBlock(
                        [channels[-1] for i in range(num_res_units)],
                        kernel_size,
        )
        
        self.key_encode = nn.Conv3d(in_channels=channels[-1],
                                    out_channels=key_out_channels,
                                    kernel_size=3,
                                    padding=1)
        self.value_encode = nn.Conv3d(in_channels=channels[-1],
                                      out_channels=value_out_channels,
                                      kernel_size=3,
                                      padding=1)

    def forward(self, inputs):
        skips = []
        skip,x= self.inital_block(inputs)
        skips.append(skip)
        for encode_block in self.encode_blocks:
            skip, x = encode_block(x)
            skips.append(skip)
        x = self.last_block(x)
        key = self.key_encode(x)
        value = self.value_encode(x)
        return key, value, skips


class Decoder(nn.Module):

    def __init__(self,
                 channels: list,
                 out_channels: int,
                 kernel_size: int,
                 stride: int = 2,
                 padding: int = 0,
                 num_res_units: int = 2,

    ):
        super(Decoder, self).__init__()
        self.inital_block = UpsamplingConv(
            in_channels_up=channels[0],
            in_channels_res=channels[0],
            out_channels_res=channels[0],
            out_channels_up=channels[1],
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            num_res_units=2,
        )
        decode_blocks = []
        for i in range(len(channels)-1):
            decode_blocks.append(UpsamplingConv(
                in_channels_up=channels[i],
                in_channels_res=channels[i+1]*3//2,
                out_channels_res=channels[i+1],
                out_channels_up=channels[i+1],
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                num_res_units=num_res_units
                ))

        self.decode_blocks = nn.Sequential(*decode_blocks)

        self.final_block = conv_block(
                                    channels[-1],
                                    out_channels,
                                    kernel_size=1,
                                    padding=0
                                    )
        self.softmax = F.softmax

    def forward(self, x, skips):
        x = self.inital_block(x,skips[0])
        for i, decode_block in enumerate(self.decode_blocks):
            x = decode_block(x, skips[i+1])
        x = self.final_block(x)
        # transform output to apply softmax voxel-wise
        batch, ch, h, w, d = x.shape
        out = x.permute(0, 2, 3, 4, 1).contiguous()
        out = out.view(out.numel()//2, 2)
        # out = self.softmax(out, dim=1)
        out = out.view(batch, h, w, d, ch)
        out = out.permute(0, 4, 1, 2, 3).contiguous()
        return out
    
    
class MemoryStorageAndReader:

    def __init__(self):
        self.keysM = None
        self.valuesM = None

    def record_to_memory(self, key, value):
        if self.keysM is not None:
            self.keysM = torch.cat((self.keysM, key), dim=0)
            self.valuesM = torch.cat((self.valuesM, value), dim=0)
        else:
            self.keysM = key
            self.valuesM = value

    def reset_memory(self):
        self.keysM = None
        self.valuesM = None

    def read(self, keyQ, valueQ):
        # T - time, C - channels, H - height, W - width, D - depth
        # TODO: implement computation for n-size minibatch
        T, C_keys, H, W, D = self.keysM.size()
        _, C_values, _, _, _ = self.valuesM.size()

        # Compare keys
        keyQ = keyQ.view(C_keys, H*W*D)
        keyQ = keyQ.permute(1, 0)
        keysM = self.keysM.permute(1, 0, 2, 3, 4)
        keysM = keysM.reshape(C_keys, T*H*W*D)
        key_similarity = torch.mm(keyQ, keysM)

        # Normalize and apply softmax
        key_similarity = key_similarity/math.sqrt(C_keys)
        key_similarity = F.softmax(key_similarity, dim=0)

        # Extract appropriate value from memory
        valueM = self.valuesM.permute(0, 2, 3, 4, 1)
        valueM = valueM.reshape(T*H*W*D, C_values)
        extracted_value = torch.mm(key_similarity, valueM)
        extracted_value = extracted_value.view(H, W, D, C_values)

        # Concatenate with query value
        read = torch.cat([valueQ, extracted_value.permute(3, 0, 1, 2).unsqueeze_(0)], dim=1)

        return read





