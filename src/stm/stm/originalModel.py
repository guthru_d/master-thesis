import torch
import torch.nn as nn
import torch.nn.functional as F
import math


def conv_block(in_channels, out_channels, kernel_size, stride=1, padding=2):
    return nn.Sequential(
        nn.Conv3d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
        nn.BatchNorm3d(num_features=out_channels),
        nn.PReLU()
    )


class ResBlock(nn.Module):
    def __init__(
        self,
        in_channel_planes,
        out_channel_planes,
        kernel_size,

    ):
        super(ResBlock, self).__init__()
        self.conv_blocks = nn.Sequential(*[conv_block(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size)
                                           for in_channels, out_channels in
                                           zip(in_channel_planes, out_channel_planes)])

    def forward(self, x, input_from_skipped_connection=None):
        if input_from_skipped_connection is not None:
            x_and_skip = torch.cat((x, input_from_skipped_connection), dim=1)
            conv_output = self.conv_blocks(x_and_skip)
        else:
            conv_output = self.conv_blocks(x)

        try:
            res_output = conv_output + x.expand(-1, conv_output.shape[1], -1, -1, -1)
        except RuntimeError:
            res_output = conv_output + x_and_skip.expand(-1, conv_output.shape[1], -1, -1, -1)
        return res_output


class DownsamplingConv(nn.Module):

    def __init__(
        self,
        res_in_channel_planes,
        res_out_channel_planes,
        res_kernel_size,
        down_in_channels,
        down_out_channels,
        down_kernel_size,
        down_stride,
    ):
        super(DownsamplingConv, self).__init__()

        if isinstance(res_in_channel_planes,list):
            self.residual_block = ResBlock(
                                    res_in_channel_planes,
                                    res_out_channel_planes,
                                    res_kernel_size,
                                    )
        else:
            self.residual_block = conv_block(res_in_channel_planes,
                                             res_out_channel_planes,
                                             res_kernel_size)


        self.downsample = conv_block(down_in_channels,
                                     down_out_channels,
                                     down_kernel_size,
                                     down_stride,
                                     padding=0)

    def forward(self, x):
        res_output = self.residual_block(x)
        downsampled = self.downsample(res_output)
        return res_output, downsampled


class UpsamplingConv(nn.Module):

    def __init__(
        self,
        res_in_channel_planes,
        res_out_channel_planes,
        res_kernel_size,
        up_in_channels,
        up_out_channels,
        up_kernel_size,
        up_stride,
    ):
        super(UpsamplingConv, self).__init__()
        
        if isinstance(res_in_channel_planes,list):
            self.residual_block = ResBlock(
                                    res_in_channel_planes,
                                    res_out_channel_planes,
                                    res_kernel_size,
                                    )
        else:
            self.residual_block = conv_block(res_in_channel_planes,
                                             res_out_channel_planes,
                                             res_kernel_size)


        self.upsample = nn.Sequential(
            torch.nn.ConvTranspose3d(
                up_in_channels,
                up_out_channels,
                up_kernel_size,
                up_stride,
                padding=0
            ),
            nn.BatchNorm3d(num_features=up_out_channels),
            nn.PReLU()
        )

    def forward(self, x, input_skip):
        upsampled = self.upsample(x)
        res_output = self.residual_block(upsampled, input_skip)
        return res_output


class Encoder(nn.Module):
    def __init__(self,
                 res_in_channel_planes_list,
                 res_out_channel_planes_list,
                 res_kernel_size_list,
                 down_in_channels_list,
                 down_out_channels_list,
                 down_kernel_size_list,
                 down_stride_list,
    ):

        super(Encoder, self).__init__()
        self.encode_blocks = nn.Sequential(*[DownsamplingConv(
                            res_in_channel_planes,
                            res_out_channel_planes,
                            res_kernel_size,
                            down_in_channels,
                            down_out_channels,
                            down_kernel_size,
                            down_stride)
                            for res_in_channel_planes, 
                            res_out_channel_planes, 
                            res_kernel_size, 
                            down_in_channels, 
                            down_out_channels, 
                            down_kernel_size, 
                            down_stride in
                            zip(res_in_channel_planes_list, 
                            res_out_channel_planes_list,
                            res_kernel_size_list,
                            down_in_channels_list,
                            down_out_channels_list,
                            down_kernel_size_list,
                            down_stride_list)])
       
        self.encode_blocks.append(ResBlock(
                        [256,256,256],
                        [256,256,256],
                        5,
        ))
        self.key_encode = nn.Conv3d(in_channels=256,
                                    out_channels=32,
                                    kernel_size=3,
                                    padding=1)
        self.value_encode = nn.Conv3d(in_channels=256,
                                      out_channels=128,
                                      kernel_size=3,
                                      padding=1)

    def forward(self, input):
        skip1, out1 = self.encode_blocks[0](input)
        skip2, out2 = self.encode_blocks[1](out1)
        skip3, out3 = self.encode_blocks[2](out2)
        skip4, out4 = self.encode_blocks[3](out3)
        out5 = self.encode_blocks[4](out4)
        key = self.key_encode(out5)
        value = self.value_encode(out5)
        return key, value, skip1, skip2, skip3, skip4


class Decoder(nn.Module):

    def __init__(self,
                 res_in_channel_planes_list,
                 res_out_channel_planes_list,
                 res_kernel_size_list,
                 up_in_channels_list,
                 up_out_channels_list,
                 up_kernel_size_list,
                 up_stride_list,):
        super(Decoder, self).__init__()

        self.decode_blocks = nn.Sequential(*[UpsamplingConv(
                            res_in_channel_planes,
                            res_out_channel_planes,
                            res_kernel_size,
                            up_in_channels,
                            up_out_channels,
                            up_kernel_size,
                            up_stride)
                            for res_in_channel_planes, 
                            res_out_channel_planes, 
                            res_kernel_size, 
                            up_in_channels, 
                            up_out_channels, 
                            up_kernel_size, 
                            up_stride in
                            zip(res_in_channel_planes_list, 
                            res_out_channel_planes_list,
                            res_kernel_size_list,
                            up_in_channels_list,
                            up_out_channels_list,
                            up_kernel_size_list,
                            up_stride_list)])



        self.decode_blocks.append(conv_block(
                                    32,
                                    2,
                                    1,
                                    padding=0))

        self.softmax = F.softmax

    def forward(self, x, skip1, skip2, skip3, skip4):
        out1 = self.decode_blocks[0](x, skip4)
        out2 = self.decode_blocks[1](out1, skip3)
        out3 = self.decode_blocks[2](out2, skip2)
        out4 = self.decode_blocks[3](out3, skip1)
        out = self.decode_blocks[4](out4)
        # transform output to apply softmax voxel-wise
        batch, ch, h, w, d = out.shape
        out = out.permute(0, 2, 3, 4, 1).contiguous()
        out = out.view(out.numel()//2, 2)
        # out = self.softmax(out, dim=1)
        out = out.view(batch, h, w, d, ch)
        out = out.permute(0, 4, 1, 2, 3).contiguous()
        return out


class MemoryStorageAndReader:

    def __init__(self):
        self.keysM = None
        self.valuesM = None

    def record_to_memory(self, key, value):
        if self.keysM is not None:
            self.keysM = torch.cat((self.keysM, key), dim=0)
            self.valuesM = torch.cat((self.valuesM, value), dim=0)
        else:
            self.keysM = key
            self.valuesM = value

    def reset_memory(self):
        self.keysM = None
        self.valuesM = None

    def read(self, keyQ, valueQ):
        # T - time, C - channels, H - height, W - width, D - depth
        # TODO: implement computation for n-size minibatch
        T, C_keys, H, W, D = self.keysM.size()
        _, C_values, _, _, _ = self.valuesM.size()

        # Compare keys
        keyQ = keyQ.view(C_keys, H*W*D)
        keyQ = keyQ.permute(1, 0)
        keysM = self.keysM.permute(1, 0, 2, 3, 4)
        keysM = keysM.reshape(C_keys, T*H*W*D)
        key_similarity = torch.mm(keyQ, keysM)

        # Normalize and apply softmax
        key_similarity = key_similarity/math.sqrt(C_keys)
        key_similarity = F.softmax(key_similarity, dim=0)

        # Extract appropriate value from memory
        valueM = self.valuesM.permute(0, 2, 3, 4, 1)
        valueM = valueM.reshape(T*H*W*D, C_values)
        extracted_value = torch.mm(key_similarity, valueM)
        extracted_value = extracted_value.view(H, W, D, C_values)

        # Concatenate with query value
        read = torch.cat([valueQ, extracted_value.permute(3, 0, 1, 2).unsqueeze_(0)], dim=1)

        return read



class STM(nn.Module):

    def __init__(self):
        super(STM, self).__init__()

        self.encoderM = Encoder(
                 res_in_channel_planes_list=[2,[32,32],[64,64,64],[128,128,128]],
                 res_out_channel_planes_list=[16,[32,32],[64,64,64],[128,128,128]],
                 res_kernel_size_list = [5,5,5,5],
                 down_in_channels_list= [16,32,64,128],
                 down_out_channels_list=[32,64,128,256],
                 down_kernel_size_list=[2,2,2,2],
                 down_stride_list=[2,2,2,2],
        )
        self.encoderQ = Encoder(
                 res_in_channel_planes_list=[[1],[32,32],[64,64,64],[128,128,128]],
                 res_out_channel_planes_list=[[16],[32,32],[64,64,64],[128,128,128]],
                 res_kernel_size_list = [5,5,5,5],
                 down_in_channels_list= [16,32,64,128],
                 down_out_channels_list=[32,64,128,256],
                 down_kernel_size_list=[2,2,2,2],
                 down_stride_list=[2,2,2,2],
        )
        self.decoder = Decoder(
                 res_in_channel_planes_list=[[256,256,256],[192,128,128],[96,64],[48]],
                 res_out_channel_planes_list=[[256,256,256],[128,128,128],[64,64],[32]],
                 res_kernel_size_list = [5,5,5,5],
                 up_in_channels_list= [256,256,128,64],
                 up_out_channels_list=[128,128,64,32],
                 up_kernel_size_list=[2,2,2,2],
                 up_stride_list=[2,2,2,2],
        )
        self.memoryUnit = MemoryStorageAndReader()


    def to_memory(self, x):
        for i in range(x.shape[1]):
            keyM, valueM, _, _, _, _ = self.encoderM(x[:, i, :, :, :, :])
            self.memoryUnit.record_to_memory(keyM, valueM)

    def segment(self, input):
        self.keyQ, self.valueQ, skip1, skip2, skip3, skip4 = self.encoderQ(input)
        read = self.memoryUnit.read(self.keyQ, self.valueQ)
        output = self.decoder(read, skip1, skip2, skip3, skip4)
        return output

    def forward(self,inputs):
        query = inputs[:,0].unsqueeze(1)
        previous_data = inputs[:,1:]
        previous_data = previous_data.unsqueeze(1).view(1,2,2,previous_data.shape[-3],previous_data.shape[-2],previous_data.shape[-1])

        # torch.autograd.set_detect_anomaly(True)
        self.to_memory(previous_data)
        output = self.segment(query)
        return output

    # def forward(self,query,previous_data):
    #         # query = inputs[0]
    #         # previous_data = inputs[1]

    #     torch.autograd.set_detect_anomaly(True)
    #     self.to_memory(previous_data)
    #     output = self.segment(query)
    #     return output


