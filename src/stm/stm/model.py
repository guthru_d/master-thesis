import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from typing import Optional, Sequence, Tuple, Union
import warnings
from monai.networks.blocks import Convolution, ResidualUnit, UpSample
from monai.networks.layers.factories import Act, Norm
from src.stm.stm.modules import *

class STM(nn.Module):
    def __init__(self,
                in_channels,
                out_channels,
                channels,
                strides: int = 2,
                kernel_size: int = 5,
                num_res_units: int = 1,
                padding: int = 0,
        ):        
        super(STM, self).__init__()


        self.encoderM = Encoder(
                        in_channels+1,
                        channels,
                        kernel_size,
                        strides,
                        padding,
                        num_res_units,
                        channels[-1]//8,
                        channels[-1]//2,

        )
        self.encoderQ = Encoder(
                        in_channels,
                        channels,
                        kernel_size,
                        strides,
                        padding,
                        num_res_units,
                        channels[-1]//8,
                        channels[-1]//2,
        )
        self.decoder = Decoder(
                        channels[::-1],
                        out_channels,
                        kernel_size,
                        strides,
                        padding,
                        num_res_units,
        )
        self.memoryUnit = MemoryStorageAndReader()


    def to_memory(self, x):
        for i in range(x.shape[1]):
            keyM, valueM, _ = self.encoderM(x[:, i, :, :, :, :])
            self.memoryUnit.record_to_memory(keyM, valueM)

    def segment(self, input):
        self.keyQ, self.valueQ, skips = self.encoderQ(input)
        read = self.memoryUnit.read(self.keyQ, self.valueQ)
        output = self.decoder(read, skips[::-1])
        return output
    
    
    def forward(self,inputs):
        query = inputs[:,0].unsqueeze(1) # [BS,C,H,W,D] e.g. [1,1,128,128,64]
        previous_data = inputs[:,1:] 
        previous_data = previous_data.unsqueeze(1).view(previous_data.shape[0],2,2,previous_data.shape[-3],previous_data.shape[-2],previous_data.shape[-1]) # [BS,N,M+I,H,W,D] e.g. [1,2,2,128,128,64]

        # torch.autograd.set_detect_anomaly(True)
        self.to_memory(previous_data)
        output = self.segment(query)
        return output
    
    # def forward(self,query,previous_data):
    #     # query = inputs[0]
    #     # previous_data = inputs[1]

    #     torch.autograd.set_detect_anomaly(True)
    #     self.to_memory(previous_data)
    #     output = self.segment(query)
    #     return output


