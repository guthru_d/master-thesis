from turtle import back
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from typing import Optional, Sequence, Tuple, Union
import warnings
from monai.networks.nets import EfficientNetBNFeatures
from monai.networks.nets.flexible_unet import UNetDecoder,SegmentationHead,_get_encoder_channels_by_backbone
from typing import List, Optional, Sequence, Tuple, Union

encoder_feature_channel = {
    "efficientnet-b0": (16, 24, 40, 112, 320),
    "efficientnet-b1": (16, 24, 40, 112, 320),
    "efficientnet-b2": (16, 24, 48, 120, 352),
    "efficientnet-b3": (24, 32, 48, 136, 384),
    "efficientnet-b4": (24, 32, 56, 160, 448),
    "efficientnet-b5": (24, 40, 64, 176, 512),
    "efficientnet-b6": (32, 40, 72, 200, 576),
    "efficientnet-b7": (32, 48, 80, 224, 640),
    "efficientnet-b8": (32, 56, 88, 248, 704),
    "efficientnet-l2": (72, 104, 176, 480, 1376),
}

class EfficientSTM(nn.Module):
    def __init__(self,
                in_channels,
                out_channels,
                decoder_channels,
                backbone,
                norm: Union[str, tuple] = ("batch", {"eps": 1e-3, "momentum": 0.1}),
                act: Union[str, tuple] = ("relu", {"inplace": True}),
                dropout: Union[float, tuple] = 0.0,
                decoder_bias: bool = False,
                upsample: str = "nontrainable",
                interp_mode: str = "nearest",
                is_pad: bool = True,
    ) -> None:        
        super(EfficientSTM, self).__init__()
        
        
        if backbone not in encoder_feature_channel:
            raise ValueError(f"invalid model_name {backbone} found, must be one of {encoder_feature_channel.keys()}.")

        encoder_channels = _get_encoder_channels_by_backbone(backbone, in_channels)

        adv_prop = "ap" in backbone
        
        self.encoderM = Encoder(
                 in_channels=in_channels+1,
                 model_name= backbone,
                 norm=norm,
                 adv_prop=adv_prop,
                 final_out_channel=encoder_channels[-1],
                 spatial_dims=3,
                 key_out_channels=encoder_channels[-1]//8,
                 value_out_channels=encoder_channels[-1]//2,
        )
         
        self.encoderQ = Encoder(
                 in_channels=in_channels,
                 model_name= backbone,
                 norm=norm,
                 adv_prop=adv_prop,
                 final_out_channel=encoder_channels[-1],
                 spatial_dims=3,
                 key_out_channels=encoder_channels[-1]//8,
                 value_out_channels=encoder_channels[-1]//2,
        )
        
        self.decoder = Decoder(
                        spatial_dims=3,
                        encoder_channels=encoder_channels,
                        decoder_channels=decoder_channels,
                        act=act,
                        norm=norm,
                        dropout=dropout,
                        bias=decoder_bias,
                        upsample=upsample,
                        interp_mode=interp_mode,
                        pre_conv=None,
                        align_corners=None,
                        is_pad=is_pad,
        )
        self.segmentation_head = SegmentationHead(
            spatial_dims=3,
            in_channels=decoder_channels[-1],
            out_channels=out_channels,
            kernel_size=3,
            act=None,
        )
        
        self.memoryUnit = MemoryStorageAndReader()


    def to_memory(self, x):
        for i in range(x.shape[1]):
            keyM, valueM, _ = self.encoderM(x[:, i, :, :, :, :])
            self.memoryUnit.record_to_memory(keyM, valueM)

    def segment(self, input):
        self.keyQ, self.valueQ, skips = self.encoderQ(input)
        read = self.memoryUnit.read(self.keyQ, self.valueQ)
        output = self.decoder(read, skips[:-1][::-1])
        return output
    
    
    def forward(self,inputs):
        query = inputs[:,0].unsqueeze(1) # [BS,C,H,W,D] e.g. [1,1,128,128,64]
        previous_data = inputs[:,1:] 
        previous_data = previous_data.unsqueeze(1).view(previous_data.shape[0],2,2,previous_data.shape[-3],previous_data.shape[-2],previous_data.shape[-1]) # [BS,N,M+I,H,W,D] e.g. [1,2,2,128,128,64]

        # torch.autograd.set_detect_anomaly(True)
        self.to_memory(previous_data)
        decoder_output = self.segment(query)
        x_seg = self.segmentation_head(decoder_output)
        return x_seg
    

class Encoder(nn.Module):
    def __init__(self,
                 in_channels: int,
                 model_name: str,
                 norm: Union[str, tuple],
                 adv_prop: str,
                 final_out_channel: int,
                 spatial_dims: int=3,
                 key_out_channels: int = 32,
                 value_out_channels: int = 128,
    ):

        super(Encoder, self).__init__()

        self.efficientEncoder = EfficientNetBNFeatures(
            model_name=model_name,
            in_channels=in_channels,
            spatial_dims=spatial_dims,
            norm=norm,
            adv_prop=adv_prop
        )
        
        self.key_encode = nn.Conv3d(in_channels=final_out_channel,
                                    out_channels=key_out_channels,
                                    kernel_size=3,
                                    padding=1)
        self.value_encode = nn.Conv3d(in_channels=final_out_channel,
                                      out_channels=value_out_channels,
                                      kernel_size=3,
                                      padding=1)

    def forward(self, inputs):
        features = self.efficientEncoder(inputs) 
        key = self.key_encode(features[-1])
        value = self.value_encode(features[-1])
        return key, value, features
    
    
class Decoder(UNetDecoder):
    def __init__(self,
                spatial_dims: int,
                encoder_channels: Sequence[int],
                decoder_channels: Sequence[int],
                act: Union[str, tuple],
                norm: Union[str, tuple],
                dropout: Union[float, tuple],
                bias: bool,
                upsample: str,
                pre_conv: Optional[str],
                interp_mode: str,
                align_corners: Optional[bool],
                is_pad: bool,
    ):
        super().__init__(
                spatial_dims = spatial_dims,
                encoder_channels=encoder_channels,
                decoder_channels=decoder_channels,
                act=act,
                norm=norm,
                dropout=dropout,
                bias=bias,
                upsample=upsample,
                pre_conv=pre_conv,
                interp_mode=interp_mode,
                align_corners=align_corners,
                is_pad=is_pad,
        )
    def forward(self, features, skips: List[torch.Tensor]):
            skip_connect = len(skips)

            x = features
            for i, block in enumerate(self.blocks):
                if i < skip_connect:
                    skip = skips[i]
                else:
                    skip = None
                x = block(x, skip)

            return x       

class MemoryStorageAndReader:

    def __init__(self):
        self.keysM = None
        self.valuesM = None

    def record_to_memory(self, key, value):
        key = key.unsqueeze(1)
        value = value.unsqueeze(1)
        if self.keysM is not None:
            self.keysM = torch.cat((self.keysM, key), dim=1)
            self.valuesM = torch.cat((self.valuesM, value), dim=1)
        else:
            self.keysM = key
            self.valuesM = value

    def reset_memory(self):
        self.keysM = None
        self.valuesM = None

    def read(self, keyQ, valueQ):
        # T - time, C - channels, H - height, W - width, D - depth
        # TODO: implement computation for n-size minibatch
        B, T, C_keys, H, W, D = self.keysM.size()
        _,_, C_values, _, _, _ = self.valuesM.size()

        # Compare keys
        keyQ = keyQ.view(B, C_keys, H*W*D)
        keyQ = keyQ.permute(0, 2, 1)
        keysM = self.keysM.permute(0, 2, 1, 3, 4, 5)
        keysM = keysM.reshape(B, C_keys, T*H*W*D)
        key_similarity = torch.bmm(keyQ, keysM)

        # Normalize and apply softmax
        key_similarity = key_similarity/math.sqrt(C_keys)
        key_similarity = F.softmax(key_similarity, dim=0)

        # Extract appropriate value from memory
        valueM = self.valuesM.permute(0,1, 3, 4, 5, 2)
        valueM = valueM.reshape(B,T*H*W*D, C_values)
        extracted_value = torch.bmm(key_similarity, valueM)
        extracted_value = extracted_value.view(B, H, W, D, C_values)

        # Concatenate with query value
        read = torch.cat([valueQ, extracted_value.permute(0,4, 1, 2, 3)], dim=1)

        return read





