# -*- coding: utf-8 -*-
import os
import argparse
import dotenv
import gc
from config import *
from variables import *
from tqdm import tqdm
import numpy as np
from Utils.conversion import convertPIDToUUID
from Utils.ioUtils import*
from Utils.scan import Scan
from Utils.structureSet import StructureSet
from genericpath import exists
from src.utils.makedata import *
import torch
from monai.transforms import *
#TODO: ADD STRUCT_CONVERSION FILE DIRECTLY
def main():
    if args.patientList:
        patientIDs = args.patientList
    else:
        patientIDs = os.listdir(args.sourcePath)
    for patientID in tqdm(patientIDs):
        key = convertPIDToUUID(patientID,conversionFile=PID_CONVERSION_FILE)
        fileList = getFileList(os.path.join(args.sourcePath,patientID),args)
        targetFolderName = os.path.join(args.targetPath, key)
        if len(fileList) > 0:
            joinAndMakeFolder(args.targetPath, key)
        for imageFiles,structFile,scanID in fileList:
            print(f"Processing patient {patientID} scan {scanID}",end="\r")
            if exists(os.path.join(targetFolderName,f"{IMAGE}_{scanID}.npz")):
                print("Preprocessing already done, skipping...",end="\r")
            else:
                try:
                    scan = Scan(imageFiles)
                    if args.dataset!=NSCLC: scan._setOrigin([0, 0, 0])
                    structures = StructureSet(structFile[0],scan)
                    if args.dataset==NSCLC: scan._setOrigin([0, 0, 0])
                    structures._setOrigin([0, 0, 0])
                    availableLabels = [label for label in args.labelsToRetain if any(label in oldLabel for oldLabel in structures.getStructureNames())]
                    if len(availableLabels)>0:
                        # label = getLabel(structures,args.labelsToRetain)
                        image = scan.getPixelArray()
                        labelPath = os.path.join(targetFolderName,f"{LABEL}_{scanID}")
                        imagePath = os.path.join(targetFolderName,f"{IMAGE}_{scanID}")
                        os.mkdir(labelPath)
                        labels={}
                        labelsList =[]
                        if args.dataset==HNC or args.dataset==NSCLC:
                            wantedLabels=structures.getStructureNames()
                        else:
                            wantedLabels=args.labelsToRetain
                        for labelName in wantedLabels:
                            if checkIfLabelInStructureSet(
                                labelName, structures.getStructureNames()
                            ):
                                label=np.any(
                                structures.getPixelArray(structureName=labelName),0,keepdims=True)
                                np.savez_compressed(os.path.join(labelPath,labelName),label)
                                labels[labelName]=label
                                # labelsList.append(label)
                                del label
                            else:
                                pass
                                # labels.append(np.zeros_like(structures.getPixelArray()[0:1]))
                        # fullLabel = np.concatenate(labelsList, axis=0)

                        # if args.toSingleChannel:
                        #     label = np.expand_dims(np.argmax(label,axis=0),axis=0)
                        # saveAsNii(image,label,imagePath,labelPath)
                        np.savez_compressed(labelPath,**labels)
                        np.savez_compressed(imagePath,image.astype(np.float16))
                        del image
                    else:
                        print("No useful labels found, moving on!")
                    del scan, structures
                except:
                    print("Preprocessing failed",end="\r")
                    if exists(getTargetImageName(targetFolderName, scanID)):
                        os.remove(getTargetImageName(targetFolderName, scanID))
                    if exists(getTargetLabelName(targetFolderName, scanID)):
                        os.remove(getTargetLabelName(targetFolderName, scanID))
            gc.collect()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--dataset', type = str, default = NSCLC)
    parser.add_argument('--labelDict', type = str, default = ALL)
    parser.add_argument('--toSingleChannel', choices=('True','False'), default = True)
    parser.add_argument('--patientList', nargs='+', required=False, default= ["Patient_1","Patient_3","Patient_5","Patient_9","Patient_13"]
)

    args = parser.parse_args()
    args = prepareMakeDataset(args)
    dotenv.load_dotenv(DOTENV_PATH)
    structureDict = {}
    main()




# from monai.transforms import RandomizableTransform

# class RandomLabel(RandomizableTransform):
#     def randomize(self,labels):
#         super().randomize(None)
#         self.randomLabel = self.R.choice(labels)

#     def __call__(self, labels):
#         self.randomize(labels)
#         if not self._do_transform:
#             return labels[0]  
#         return self.randomLabel

# tra = RandomLabel()


# class LoadLabelNames(MapTransform):
#     def __call__(self, data):
#         for key in self.keys:
#             if key in data:
#                 # update output data with some_transform_function(data[key]).
#             else:
#                 # raise exception unless allow_missing_keys==True.
#         return data

