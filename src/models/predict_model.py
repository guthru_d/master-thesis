#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 21.09.2022
# =============================================================================
"""
Filename:           predict_model.py
Description:        Predict masks e.g. do inference
Main Libs:          Pytorch, Wandb, monai, A.smoldeCodebase
Long Description:   First configurations and model need to be initalized. Best way to
                    do this is via argparser arguments. Provide e.g.
                    predict_model.py --mode evaluate folderName yourModel --model UNet
                    All other parameters are retrieved from wandbTrainRun, but you can still
                    change everything if you pass different config file or argparser arguments
                    Please consider the argparser arguments at the bottom of the page.
                    All Results but images are saved to your wandbRun as evaluation subsection. 
                    All plots are saved to your specific run e.g. folderName as tensorboard files
State:              dev
"""
# =============================================================================
# Imports
# =============================================================================
import os
import dotenv
from config import DOTENV_PATH
import wandb
from variables import *
from src.utils.general import *
from variables import *
from config import *
from src.utils.evaluator import modelInference
from omegaconf import OmegaConf,open_dict

# os.environ["CUDA_VISIBLE_DEVICES"]="1"

#0.05,0.1,0.2,0.25,0.33,5,75    
    
@hydra.main(config_path="/psi/home/guthru_d/master-thesis/configs/", config_name="defaults")
def main(new: omegaconf.DictConfig)->None:
    # new.wandb.setup.id = "2qa6y9fg"
    # new.wandb.setup.job_type="evaluate"
    # new.wandb.setup.name=""
    # new.inference.checkpoint="last"
    run=wandb.init(**new.wandb.setup)
    cfg = omegaconf.OmegaConf.create(dict(run.config))
    OmegaConf.set_struct(cfg, True)
    with open_dict(cfg):
        cfg.inference.checkpoint = new.inference.checkpoint
        cfg.metric = new.metric
        cfg.wandb.setup.job_type=new.wandb.setup.job_type
        cfg.validation.device=new.validation.device
        cfg.validation.overlap=new.validation.overlap
        cfg.inference.eval_mode=new.inference.eval_mode
        cfg.inference.plot_mode=new.inference.plot_mode
        cfg.validation.num_components=1
        cfg.validation.connectivity=3
        cfg.inference.dtype = "float32"
        cfg.inference.top_k = None
        cfg.wandb.setup.id = new.wandb.setup.id
        # cfg.dataset.test=["1c8d5b43-8db7-47f9-8c59-877d1e09791c",
        # "3e111204-3e97-43fe-a310-f1ed4c261a03",
        # "257b1098-f793-4a71-b657-9c08518d0296",]
        # cfg.dataset.validation_labels='test2'
        # cfg.wandb.setup.job_type="evaluate"

    print(f"Evaluate Model: {cfg.model.name}")
    print(f"Session: {cfg.paths.session}")
    pprint.pprint(cfg,indent=3)
    _, dictTable = modelInference(cfg,dataType=TEST)
    
    run.log({"NInference"+f"Metrics_{cfg.inference.eval_mode}":dictTable})
    run.finish()
if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    main()