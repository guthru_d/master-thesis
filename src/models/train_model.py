#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 21.09.2022
# =============================================================================
"""
Filename:           train_model.py
Description:        Train segmentation model or do hyperparmetersweeps
Main Libs:          Pytorch, Wandb,monai, smoldeCodebase
Long Description:   First configurations need to be initalized. There are multiple
                    ways to initalize a config.
                    1.  New config file (see readme or go to configs folder) which 
                        can be passed to argparser e.g --config CTData512/main/default
                    2.  Select default yaml file as basis and make your own new config
                        file test.yaml and only adust the parameters you want. All
                        parameters that are passed directly via argparser --... over
                        write the parameters of the default yaml file
                    3.  To initalize a hyperparmeter sweep the yaml file needs to be
                        setup a bit differently (see readme or configs folder). 
                        Provide within the sweep.yaml file a default yaml via config
                        value and change the parameters you wish the run 
                        "wandb sweep path/to/sweep.yaml".
                    4.  You may also just change single parameters via the argparser
                        arguments e.g python path/to/train_model.py --model UNetPlusPlus
State:              dev
"""
# =============================================================================
# Imports
# =============================================================================
import os
import dotenv
from config import *
import wandb
WANDB_DIR = WANDB_DIR
WANDB_CACHE_DIR = WANDB_CACHE_DIR
# os.environ.pop("QT_QPA_PLATFORM_PLUGIN_PATH")
import traceback
from torch import autograd
from variables import *
from src.utils.general import *
from src.utils.dataset import DatasetWrapper
from src.utils.model import ModelWrapper
from src.utils.compile import CompileWrapper
from src.utils.trainer import TrainerWrapper
from monai.data import set_track_meta
from monai.utils import set_determinism
torch.backends.cudnn.benchmark = True
import pprint
import omegaconf
import hydra



# =============================================================================
# Main
# =============================================================================
@hydra.main(config_path="/psi/home/guthru_d/master-thesis/configs/", config_name="defaults")
def main(cfg: omegaconf.DictConfig)->None:
    SESSION_DIR = os.path.join(MODEL_DIR,f"{cfg.model.name}/{cfg.dataset.name}/{cfg.wandb.setup.job_type}_{cfg.wandb.setup.name}")

    cfg.wandb.setup.name=f"{cfg.model.name}_{cfg.dataset.name}_{cfg.wandb.setup.job_type}_{cfg.wandb.setup.name}"
    if cfg.wandb.setup.job_type==EVALUATE:
        print(f"Evaluate Model: {cfg.model.name}")
        run=wandb.init(**cfg.wandb.setup)
        SESSION_DIR=run.config.paths.session
        cfg = omegaconf.OmegaConf.create(dict(run.config))
        cfg.paths={
            "session": SESSION_DIR,
            "best": os.path.join(SESSION_DIR,f"{run.id}_best.pt"),
            "last": os.path.join(SESSION_DIR,f"{run.id}_last.pt"),
            "base": cfg.paths.base,
        }
    elif cfg.wandb.setup.job_type==RESUME:
        print(f"Resume Model: {cfg.model.name}")
        run=wandb.init(id=cfg.wandb.setup.id,
                       resume="must")
        print(run.config)
        SESSION_DIR=run.config.paths["session"]
        cfg.paths={
            "session": SESSION_DIR,
            "best": os.path.join(SESSION_DIR,f"{run.id}_best.pt"),
            "last": os.path.join(SESSION_DIR,f"{run.id}_last.pt"),
            "base": cfg.paths.base,
        }
        cfg = omegaconf.OmegaConf.create(dict(run.config))
        pprint.pprint(cfg,indent=3)
        print(f"Your run is resumed with Session: {SESSION_DIR}")
    elif cfg.wandb.setup.job_type==TRAIN or cfg.wandb.setup.job_type==SWEEP:
        id = wandb.util.generate_id()
        cfg.paths={
        "session": SESSION_DIR,
        "best": os.path.join(SESSION_DIR,f"{id}_best.pt"),
        "last": os.path.join(SESSION_DIR,f"{id}_last.pt"),
        "base": cfg.paths.base,
    }
        print(f"Train Model from scratch: {cfg.model.name}")
    elif cfg.wandb.setup.job_type==FINETUNING:
        print(f"Finetune Model: {cfg.model.name} from {cfg.paths.base}")
        id = wandb.util.generate_id()
        cfg.paths={
        "session": SESSION_DIR,
        "best": os.path.join(SESSION_DIR,f"{id}_best.pt"),
        "last": os.path.join(SESSION_DIR,f"{id}_last.pt"),
        "base": cfg.paths.base,
    }
    wandb_cfg = omegaconf.OmegaConf.to_container(
            cfg, resolve=True, throw_on_missing=True)
    run=wandb.init(**cfg.wandb.setup, config=wandb_cfg)
    if 'tmp' in cfg.paths.session:
        replaceFolder(cfg.paths.session)
    else:
        makeFolderMaybeRecursive(cfg.paths.session)  
    if cfg.wandb.setup.job_type==SWEEP:
        set_determinism(seed=0)
        pprint.pprint(cfg,indent=3)
    cpu_count=os.cpu_count()        
    print(f"Your CPU Count: {cpu_count}!")
        
        
    trainDS = DatasetWrapper(cfg,TRAIN).getDataset()
    trainLoader= DatasetWrapper(cfg,TRAIN).getDataLoader(trainDS)
    # validationDS = DatasetWrapper(cfg,VALIDATION).getDataset()
    # validationLoader= DatasetWrapper(cfg,VALIDATION).getDataLoader(validationDS)


    model = ModelWrapper(cfg)
    compile = CompileWrapper(cfg,model)
    trainer = TrainerWrapper(model,
                             trainDS,
                            #  validationDS,
                             trainLoader,
                            #  validationLoader,
                             cfg,
                             compile,
                             run,
                             )
    trainer.train()
if __name__ == '__main__':
    #loads env. variables
    dotenv.load_dotenv(DOTENV_PATH)
    try:
        main()
        wandb.finish()
        print("You finished sucessfully")
    except:
        traceback.print_exc()
        wandb.finish()
        print("You fucked up")
        traceback.print_exc()
    


