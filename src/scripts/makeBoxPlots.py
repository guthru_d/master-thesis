import wandb
import seaborn as sns
import pandas as pd
import dotenv
from variables import *
from config import *
import argparse
import numpy as np
from matplotlib import pyplot as plt

sns.set(style='whitegrid')

METRICS=[DICE_METRIC,"SurfaceDiceSmolde","robustHausdorffSmolde"]
METRICS_ABBREVIATED=['Dice','SurfaceDice','HD95']
# METRICS=["SurfaceDiceSmolde"]
# METRICS_ABBREVIATED=['SurfaceDice',]
NSCLC_LABELS = ['heart','lung_R','lung_L','esophagus','spinal_cord','CTV']
NSCLC_LABELS_A = ['Heart','Lung right','Lung left','Esophagus','Spinal cord','CTV']

HNC_LABELS = ['eye_L','eye_R','parotid_L','parotid_R','thyroid','spinal_cord','CTV_5400']
HNC_LABELS_A= ['Eye left','Eye right','Parotid left','Parotid right','Thyroid','Spinal cord','CTV 5400']
# SMOLDE_MODELS = ["Deformable registration","Patient specific segmentation","Commercial segmentation"]
SMOLDE_MODELS = ["Patient specific segmentation"]
# SMOLDE_MODELS=[]
def mergeTables(run,ids,name,version):
    tables=[]
    for id in ids:
        tables.append(wandb.use_artifact(f"run-{id}-NInferenceMetrics_{version}:latest",type='run_table').get(f"NInferenceMetrics_{version}"))
    mainTable = wandb.Table(columns=tables[0].columns)
    for table in tables:
        for i,row in table.iterrows():
            mainTable.add_data(*row)
    run.log({name: mainTable})
    return mainTable

def loadSmoldeResults(args):
    if args.dataset==NSCLC:
        smoldeResults = pd.read_pickle(os.path.join(RESULTS_DIR,'smolde/smoldeNSCLCResults.pkl'))
        labels = NSCLC_LABELS
        labelsA= NSCLC_LABELS_A
    elif args.dataset==HNC:
        smoldeResults = pd.read_pickle(os.path.join(RESULTS_DIR,'smolde/smoldeHNCResults.pkl'))
        labels = HNC_LABELS
        labelsA = HNC_LABELS_A
    return smoldeResults,labels,labelsA



def main():
    mainTables = []
    for i,ids in enumerate(args.ids):
        name=args.models[i]
        version=args.version[i]
        print(name)
        print(ids)
        print(version)
        mainTables.append(mergeTables(run,ids,name,version))
    smoldeResults,labels,labelsA = loadSmoldeResults(args)

    fig, axs =  plt.subplots(ncols=1,nrows=3,figsize=(12,7),dpi=200,)
    for m, metric in enumerate(METRICS):
        df = pd.DataFrame()
        for t, table in enumerate(mainTables):
            for l,label in enumerate(labels):
                tmpdf = pd.DataFrame()
                try:
                    tmpdf["value"]=table.get_column(metric+label)
                    tmpdf["label"]=[label]*len(tmpdf["value"])
                    tmpdf["metric"]=[METRICS_ABBREVIATED[m]]*len(tmpdf["value"])
                    tmpdf["model"]=[args.models[t]]*len(tmpdf["value"])
                    tmpdf["value"]
                    df=df.append(tmpdf)
                except:
                    print(f"{label} in {args.models[t]} not availabel. Filling everything NaN ")
                    tmpdf["value"]=[np.nan]
                    tmpdf["label"]=[np.nan]
                    tmpdf["metric"]=[np.nan]
                    tmpdf["model"]=[args.models[t]]*len(tmpdf["value"])
        tmpResults = smoldeResults[(smoldeResults['metric']==METRICS_ABBREVIATED[m]) & (smoldeResults['label'].isin(labels)) & (smoldeResults['model'].isin(SMOLDE_MODELS))]
        tmpResults.pop("patient")
        tmpResults.pop("scan")
        df=df.append(tmpResults)
        l = True if m==0 else False
        flierprops = dict(marker='o', markerfacecolor='None', markersize=5,  markeredgecolor='black')
        s=sns.boxplot(data=df,x="label",y="value",hue="model",palette="tab10",saturation=1,flierprops=flierprops,ax=axs[m])
        if metric==HAUSDORFF_DISTANCE_METRIC or metric=="robustHausdorffSmolde":
            s.set_ylim(0, 20)
        else:
            mi = df["value"].min()
            mi = int(mi*10)/10
            s.set_yticks(np.arange(mi,1.1,0.1))
            s.set_ylim(mi,1)
        s.legend_.set_title(None)
        if m!=0: s.legend_.remove()
        s.set_xlabel(None)
        if "HD" in METRICS_ABBREVIATED[m]:
            s.set_ylabel(f"{METRICS_ABBREVIATED[m]} [mm]")
        else:
            s.set_ylabel(f"{METRICS_ABBREVIATED[m]} [-]")
        if m==2: 
            s.set_xticklabels(labelsA)
        
        else:
            # s.set_xticklabels(labelsA)
            s.set_xticklabels([])
        # s.yaxis.grid(True) 
        # s.xaxis.grid(False)

    plt.tight_layout()
    wandb.log({f"BoxPlot{metric}":wandb.Image(fig)})








if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ids', 
        nargs="+",
        # default = [["iy3m3b0o"],["14ps5ya6"]],
        # default=[["2ofhfj4c","3g3w9ed0","1hlsujte","3fknk32n","2l762u3c"]]*2,#NSCLC
        # default=[["1a74vr1m","14ps5ya6"]]*4,
        # default = [["1w2u21et","2qa6y9fg","1ed3bh8r","12jze6dr","1zy14lsd"]],#HNC
        #            ["38htojox","24cnn25u","2qvnp38t","yxacojdc","281bfok2"]],#HNC_all
        
        default = [["1w2u21et","2qa6y9fg","1ed3bh8r","12jze6dr","1zy14lsd"]]*3,#HNC
        #            ["geu0wnos"]],
        # default = [["38htojox","24cnn25u","2qvnp38t","yxacojdc","281bfok2"],#HNC-all
        #            ["q771y1vl"]],

        # default = [["q771y1vl"],["geu0wnos"]],
        # default = [["3ef1loly","1k3ea9pj","2f9qpqu7","760sns99","3mucqanj"]]*2,#NSCLC LONG
        # default = [["35q3afgo","2v0ewi2x","3arip9gj","18m5k6iu","2v7056ff"],
        # ["2vtraj9r"]],#NSCLC
        # default = [["35q3afgo","2v0ewi2x","3arip9gj","18m5k6iu","2v7056ff"]]*3,
        #            ["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58","4jek92pi"]],

        # default = [["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58","4jek92pi"],
        # ["llv6e7w2"]],#NSCLC
        # default = [["2vtraj9r"],["llv6e7w2"]],
        # default = [["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58","4jek92pi"]]*3,#NSCLCL-All
        #[llv6e7w2]

        # default = [["35q3afgo","2v0ewi2x","3arip9gj","18m5k6iu","2v7056ff"]]*2,
        #             ["2vtraj9r"]],
        # default = [["yxacojdc","2qvnp38t","24cnn25u","38htojox"],
        #            ["q771y1vl"]],#HNC-All

        # default = [["1ed3bh8r","12jze6dr","1zy14lsd"]],
        #            ["yxacojdc","2qvnp38t","24cnn25u","38htojox"]],
        # default = [["18m5k6iu","3arip9gj","2v0ewi2x","2v7056ff"], #"35q3afgo"
        #            ["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58"]#NSCLC-All "4jek92pi"
        # ],
                #    ["2vtraj9r"]],#NSCLC

        help="Use generated Wandb IDs")
    parser.add_argument(
        '--version', 
        nargs="+",  
        # default=["first","first","previous","previous","every","every"]*3,
        # default = ["first","previous","every"]*3,
        # default=["first","previous","every"],          
        default=["first","previous","every"],
        help="Define which version to take")
    parser.add_argument(
        '--models',
        nargs='+',
            # default=["STCN-Every"],
            # default = ["First-Specific","First-All","Previous-Specific","Previous-All","Every-Specific","Every-All"],
            default = ["STCN (first)","STCN (previous)","STCN (every)"], 

            # default = ["STCN-Specific segmentation","STCN-General segmentation"],
            # default=["STCN-First"],
    )
    parser.add_argument(
        '--name',
        type=str,
        # default="BoxPlots-HNC Syn-Real"
        default="Box-tmp",
    )
    parser.add_argument(
        '--dataset',
        type=str,
        default='HNC',
    )
    args = parser.parse_args()

    run=wandb.init(
        resume=False,
        name=args.name,
    )
    main()


    #G: #7EC5B0
    #O: #FAB07E
    #B: #9394C8
    #R: #F59799