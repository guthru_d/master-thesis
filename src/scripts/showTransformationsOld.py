from monai.transforms import *
import dotenv
from matplotlib import pyplot as plt
from transformers import BLENDERBOT_PRETRAINED_MODEL_ARCHIVE_LIST
from config import *
from variables import *
import monai
# =============================================================================
# Imports
# =============================================================================
import dotenv
from tqdm import tqdm
from tensorboardX import SummaryWriter
from config import *
WANDB_DIR = WANDB_DIR
WANDB_CACHE_DIR = WANDB_CACHE_DIR
from src.utils.dataset import getAugmentations
from variables import *
from src.utils.general import *
from monai.utils import set_determinism
from monai.data import ThreadDataLoader,DataLoader

# set_determinism(seed=0)
EXP_NAME = "distortion"
SAVE_PATH = os.path.join("/psi/home/guthru_d/master-thesis/reports/figures/transformations",EXP_NAME)
imagePath = "/psi/home/guthru_d/master-thesis/data/processed/NSCLC/all/each/1c8d5b43-8db7-47f9-8c59-877d1e09791c/image_0.npz"
labelPath = "/psi/home/guthru_d/master-thesis/data/processed/NSCLC/all/each/1c8d5b43-8db7-47f9-8c59-877d1e09791c/label_0/CTV.npz"
pathDict = [{IMAGE:imagePath, LABEL:labelPath}]

train =  [1,              1,           [128,128,64],      [1,1,1]]
baseTransforms =[
            LoadImaged(keys=[IMAGE, LABEL]),
            
            Orientationd(keys=[IMAGE, LABEL], axcodes="RAS"),
            Spacingd(
                    keys=[IMAGE, LABEL],
                    pixdim=[2,2,2],
                    mode=("bilinear", "nearest")),
            ScaleIntensityRanged(
                keys=[IMAGE],
                a_min=-180,
                a_max=220,
                b_min=0.0,
                b_max=1.0,
                clip=True),
            EnsureTyped(keys=[LABEL], device="cpu",track_meta=False,dtype=torch.bool),
            EnsureTyped(keys=[IMAGE], device="cpu",track_meta=False,dtype=torch.float16),
        ]
base = Compose(baseTransforms)
listOfAugmentations=[
  
    [["distortion",1,5,[-0.2,0.2],["image","label"]]],
    [["distortion",1,5,[-0.3,0.3],["image","label"]]],
    [["distortion",1,5,[-0.4,0.4],["image","label"]]],
    [["distortion",1,5,[-0.5,0.5],["image","label"]]],
    [["distortion",1,5,[-0.6,0.6],["image","label"]]],
    [["distortion",1,5,[-0.7,0.7],["image","label"]]],





    ]
datasetBase = monai.data.Dataset(pathDict, transform=base)
tmpDataset = datasetBase 

img_segBase = tmpDataset[0]

for i,augmentations in tqdm(enumerate(listOfAugmentations)):
    transforms=[]
    for augmentation in augmentations:
        transforms = getAugmentations(transforms,
                                    augmentation,
                                    train[2],
                                    train[1])
    tra = Compose(transforms)
    every_n = 12
    frame_dim=-1
    frames_per_row=3
    
    
    for j in range(6):
        sw = SummaryWriter(log_dir=os.path.join(SAVE_PATH,str(augmentations)))
        img_segAug = tra(img_segBase)
        # img_segAug= [{IMAGE:tra(img_segBase[IMAGE]),LABEL:tra(img_segBase[LABEL])}]
        fig = plt.figure(figsize=(10, 10),dpi=100)#title=str([str(name).split("nary.")[1].split('object')[0] for name in baseTransforms]))
        plt.subplot(2, 2, 1)
        fig_img = monai.visualize.matshow3d(img_segBase[IMAGE], title="Original", fig=plt.gca(), every_n=every_n,frame_dim=-1,frames_per_row=frames_per_row)
        plt.subplot(2, 2, 2)
        fig_img = monai.visualize.matshow3d(img_segAug[IMAGE], title=str(augmentations), fig=plt.gca(), every_n=every_n,frame_dim=-1,frames_per_row=frames_per_row)
        plt.subplot(2, 2, 3)
        fig_seg = monai.visualize.matshow3d(img_segBase[LABEL], fig=plt.gca(), every_n=every_n,frame_dim=-1,frames_per_row=frames_per_row)
        plt.subplot(2, 2, 4)
        fig_seg = monai.visualize.matshow3d(img_segAug[LABEL], fig=plt.gca(), every_n=every_n,frame_dim=-1,frames_per_row=frames_per_row)
        plt.tight_layout()
        sw.add_figure(tag=EXP_NAME+str(augmentations),global_step=j, figure=fig)
        print(j)
        del img_segAug
    
    
    
    # [["elastic",1,0.0,0.0,0.0,0.0,[5,8],[5,25],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[5,8],[5,25],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[5,8],[25,50],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[5,8],[50,75],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[5,8],[75,100],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.1,0.1,0.1,0.1,[8,10],[25,50],["image","label"]]],
    # [["elastic",1,0.01,0.01,0.01,0.01,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.05,0.05,0.05,0.05,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.2,0.2,0.2,0.2,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.15,0.15,0.15,0.15,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.3,0.3,0.3,0.3,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.4,0.4,0.4,0.4,[3,5],[25,50],["image","label"]]],
    # [["elastic",1,0.5,0.5,0.5,0.5,[3,5],[25,50],["image","label"]]],


    # [["affine",1,0.01,0.01,0.01,0.01]],
    # [["affine",1,0.05,0.05,0.05,0.05]],
    # [["affine",1,0.1,0.1,0.1,0.1]],
    # [["affine",1,0.15,0.15,0.15,0.15]],
    # [["affine",1,0.2,0.2,0.2,0.2]],
    # [["affine",1,0.3,0.3,0.3,0.3]],
    # [["affine",1,0.2,0.0,0.0,0.0]],
    # [["affine",1,0.0,0.2,0.0,0.0]],
    # [["affine",1,0.0,0.0,0.2,0.0]],
    # [["affine",1,0.0,0.0,0.0,0.2]],


    # [["gaussianNoise",1,0.01,0]], 
    # [["gaussianNoise",1,0.05,0]],
    # [["gaussianNoise",1,0.1,0]],
    # [["gaussianNoise",1,0.12,0]],
    # [["gaussianNoise",1,0.15,0]],
    # [["gaussianNoise",1,0.2,0]],
    # [["gaussianNoise",1,0.05,0.05]],
    # [["gaussianNoise",1,0.05,0.1]],
    # [["gaussianNoise",1,0.05,0.5]],
    # [["gaussianNoise",1,0.05,1]],
    
   
 
    # [["adjustContrast",1,[0.1,0.2]]],
    # [["adjustContrast",1,[0.2,0.3]]],
    # [["adjustContrast",1,[0.4,0.8]]],
    # [["adjustContrast",1,[0.8,1]]],
    # [["adjustContrast",1,[1.5,2]]],
    # [["adjustContrast",1,[2,3]]],
    # [["adjustContrast",1,[3,4]]],
       
    # [["gaussianSmooth",1,[0.1,0.2]]],
    # [["gaussianSmooth",1,[0.2,0.3]]],
    # [["gaussianSmooth",1,[0.3,0.4]]],
    # [["gaussianSmooth",1,[0.4,0.5]]],
    # [["gaussianSmooth",1,[0.5,0.6]]],
    # [["gaussianSmooth",1,[0.6,0.7]]],
    # [["gaussianSmooth",1,[0.7,0.8]]],
    # [["gaussianSmooth",1,[0.8,0.9]]],
   
    
 
    # [["histogramShift",1,3]],
    # [["histogramShift",1,4]],
    # [["histogramShift",1,5]],
    # [["histogramShift",1,6]],
    # [["histogramShift",1,7]],
    # [["histogramShift",1,8]],

    # [["coarseDropout",1,200,20]],
    # [["coarseDropout",1,100,20]],
    # [["coarseDropout",1,50,20]],
    # [["coarseDropout",1,200,300]],
    # [["coarseDropout",1,200,200]],
    # [["coarseDropout",1,200,100]],
    # [["coarseDropout",1,200,75]],
    # [["coarseDropout",1,200,50]],

    # [["coarseShuffled",1,200,20]],
    # [["coarseShuffled",1,100,20]],
    # [["coarseShuffled",1,50,20]],
    # [["coarseShuffled",1,200,300]],
    # [["coarseShuffled",1,200,200]],
    # [["coarseShuffled",1,200,100]],
    # [["coarseShuffled",1,200,75]],
    # [["coarseShuffled",1,200,50]],
    
    
    # [["flip",1]],
    # [["flip",1]],
    # [["flip",1]],
    # [["flip",1]],
    # [["flip",1]],
    # [["flip",1]],

    # [["flip",1]],
    # [["rotate",1,[0.1,0.1]]],
    # [["rotate",1,[0.4,0.4]]],
    # [["rotate",1,[0.7,0.7]]],
    # [["zoom",1,[0.8,1.2]]],
    # [["zoom",1,[0.5,1.5]]],
    
    # [["elastic",1,[5,7],[50,100]]],
    # [["elastic",1,[2,3],[50,100]]],
    # [["elastic",1,[1,2],[50,100]]],
    # [["elastic",1,[9,12],[50,100]]],
    # [["elastic",1,[5,7],[10,50]]],
    # [["elastic",1,[5,7],[50,100]]],
    # [["elastic",1,[5,7],[100,150]]],
    # [["elastic",1,[14,15],[250,300]]],
    
    
    # [["distortion",1,4,[-0.1,0.1],["image","label"]]],
    # [["distortion",1,4,[-0.2,0.2],["image","label"]]],
    # [["distortion",1,4,[-0.3,0.3],["image","label"]]],
    # [["distortion",1,1,[-0.2,0.2],["image","label"]]],
    # [["distortion",1,2,[-0.2,0.2],["image","label"]]],
    # [["distortion",1,3,[-0.2,0.2],["image","label"]]],
    # [["distortion",1,44,[-0.2,0.2],["image","label"]]],
    
        # [["elastic",1,[5,8],[25,50],0.3,0.15,["image","label"]]],
    # [["elastic",1,[5,8],[50,100],0.3,0.15,["image","label"]]],
    # [["elastic",1,[5,8],[25,50],0.1,0.1,["image","label"]]],
    # [["elastic",1,[5,8],[50,100],0.1,0.1,["image","label"]]],
    # [["elastic",1,[5,8],[25,50],0.1,0.1,["image","label"]]],
    # [["elastic",1,[5,8],[50,100],0.05,0.15,["image","label"]]]