import wandb
import seaborn as sns
import pandas as pd
import dotenv
from variables import *
from config import *
import argparse
import numpy as np
from matplotlib import pyplot as plt

sns.set(style='whitegrid')

METRICS=[DICE_METRIC,"SurfaceDiceSmolde","robustHausdorffSmolde"]
METRICS_ABBREVIATED=['Dice','SurfaceDice','HD95']

NSCLC_LABELS = ['heart','lung_R','lung_L','esophagus','spinal_cord','CTV']
NSCLC_LABELS_A = ['Heart','Lung right','Lung left','Esophagus','Spinal cord','CTV']

HNC_LABELS = ['eye_L','eye_R','parotid_L','parotid_R','thyroid','spinal_cord','CTV_5400']
HNC_LABELS_A= ['Eye left','Eye right','Parotid left','Parotid right','Thyroid','Spinal cord','CTV 5400']
# SMOLDE_MODELS = ["Deformable registration","Patient specific segmentation","Commercial segmentation"]
SMOLDE_MODELS=[]
def mergeTables(run,ids,name,version):
    tables=[]
    for id in ids:
        tables.append(wandb.use_artifact(f"run-{id}-InferenceMetrics_{version}:latest",type='run_table').get(f"InferenceMetrics_{version}"))
    mainTable = wandb.Table(columns=tables[0].columns)
    for table in tables:
        for i,row in table.iterrows():
            mainTable.add_data(*row)
    run.log({name: mainTable})
    return mainTable

def loadSmoldeResults(args):
    if args.dataset==NSCLC:
        smoldeResults = pd.read_pickle(os.path.join(RESULTS_DIR,'smolde/smoldeNSCLCResults.pkl'))
        labels = NSCLC_LABELS
        labelsA= NSCLC_LABELS_A
    elif args.dataset==HNC:
        smoldeResults = pd.read_pickle(os.path.join(RESULTS_DIR,'smolde/smoldeHNCResults.pkl'))
        labels = HNC_LABELS
        labelsA = HNC_LABELS_A
    return smoldeResults,labels,labelsA



def main():
    mainTables = []
    for i,ids in enumerate(args.ids):
        name=args.models[i]
        version=args.version[i]
        print(name)
        print(ids)
        print(version)
        mainTables.append(mergeTables(run,ids,name,version))
    smoldeResults,labels,labelsA = loadSmoldeResults(args)

    for m, metric in enumerate(METRICS):
        df = pd.DataFrame()
        for t, table in enumerate(mainTables):
            for l,label in enumerate(labels):
                tmpdf = pd.DataFrame()
                try:
                    tmpdf["scan"]= table.get_column("scan")
                    tmpdf["value"]=table.get_column(metric+label)
                    tmpdf["label"]=[label]*len(tmpdf["value"])
                    tmpdf["metric"]=[METRICS_ABBREVIATED[m]]*len(tmpdf["value"])
                    tmpdf["model"]=[args.models[t]]*len(tmpdf["value"])
                    df=df.append(tmpdf)
                except:
                    print(f"{label} in {args.models[t]} not availabel. Filling everything NaN ")
                    tmpdf["scan"]=[np.nan]
                    tmpdf["value"]=[np.nan]
                    tmpdf["label"]=[np.nan]
                    tmpdf["metric"]=[np.nan]
                    
                    tmpdf["model"]=[args.models[t]]*len(tmpdf["value"])
        tmpResults = smoldeResults[(smoldeResults['metric']==METRICS_ABBREVIATED[m]) & (smoldeResults['label'].isin(labels)) & (smoldeResults['model'].isin(SMOLDE_MODELS))]
        tmpResults.pop("patient")
        tmpResults.pop("scan")
        df=df.append(tmpResults)
        df=df.dropna()
        l = True if m==0 else False
        plt.figure()
        df_mean = df.groupby(["label", "model", "scan"])["value"].median().reset_index()
        df_mean["label"] = pd.Categorical(df_mean["label"], categories=labels, ordered=True)
        df_mean.sort_values(["label","scan"], inplace=True)
        b = sns.FacetGrid(col="label",hue="model",data=df_mean,hue_order=args.models)
        b.map(plt.plot, "scan","value",marker="o")
        if metric==HAUSDORFF_DISTANCE_METRIC or metric=="robustHausdorffSmolde":
            b.set(ylim=(0,20))
        else:
            mi = df["value"].min()
            mi = int(mi*10)/10
            b.set(yticks=np.arange(mi,1.1,0.1))
            b.set(ylim=(mi,1))
        for ax, title in zip(b.axes.flat, labelsA):
            ax.set_title(title)
        b.set_xlabels("Scan")
        if "HD" in METRICS_ABBREVIATED[m]:
            b.set_ylabels(f"{METRICS_ABBREVIATED[m]} [mm]")
        else:
            b.set_ylabels(f"{METRICS_ABBREVIATED[m]} [-]")
        b.add_legend(title='')
        b.fig.set_dpi(200)

        # s.yaxis.grid(True) 
        # s.xaxis.grid(False)
        plt.tight_layout()
        wandb.log({f"TimeSeris{metric}":wandb.Image(b.fig)})








if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ids', 
        nargs="+",
        # default = [["iy3m3b0o"],["14ps5ya6"]],
        # default=[["2ofhfj4c","3g3w9ed0","1hlsujte","3fknk32n","2l762u3c"]]*4,#NSCLC
        # default=[["1a74vr1m","14ps5ya6"]]*4,
        # default = [["1w2u21et","2qa6y9fg","1ed3bh8r","12jze6dr","1zy14lsd"],#HNC
        #            ["38htojox","24cnn25u","2qvnp38t","yxacojdc","281bfok2"]],#HNC_all
        # default = [["1w2u21et","2qa6y9fg","1ed3bh8r","12jze6dr","1zy14lsd"]]*3,#HNC
                #    ["geu0wnos"]],


        default = [["35q3afgo","2v0ewi2x","3arip9gj","18m5k6iu","2v7056ff"]]*3,#NSCLC
        # ["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58","4jek92pi"]],#NSCLCL-All

        # default = [["35q3afgo","2v0ewi2x","3arip9gj","18m5k6iu","2v7056ff"],
        #             ["2vtraj9r"]],
        # default = [["yxacojdc","2qvnp38t","24cnn25u","38htojox"],
        #            ["q771y1vl"]],#HNC-All

        # default = [["1ed3bh8r","12jze6dr","1zy14lsd"]],
        #            ["yxacojdc","2qvnp38t","24cnn25u","38htojox"]],
        # default = [["18m5k6iu","3arip9gj","2v0ewi2x","2v7056ff"], #"35q3afgo"
        #            ["k2hfuri8","34wiwuak","unetwb5u","1wtn2m58"]#NSCLC-All "4jek92pi"
        # ],
                #    ["2vtraj9r"]],#NSCLC

        help="Use generated Wandb IDs")
    parser.add_argument(
        '--version', 
        nargs="+",  
        # default=["first","first","previous","previous","every","every"]*3,
        # default = ["first","previous","every"]*3,
        default=["first","previous","every"],           
        help="Define which version to take")
    parser.add_argument(
        '--models',
        nargs='+',
            # default=["STCN-Every"],
            # default = ["First-Specific","First-All","Previous-Specific","Previous-All","Every-Specific","Every-All"],
            default = ["First","Previous","Every"], 

            # default = ["STCN-First"],
            # default=["STCN-First"],
    )
    parser.add_argument(
        '--name',
        type=str,
        # default="BoxPlots-HNC Syn-Real"
        default="TimeSeries-NSCLC-Memorybanks",
    )
    parser.add_argument(
        '--dataset',
        type=str,
        default='NSCLC',
    )
    args = parser.parse_args()

    run=wandb.init(
        resume=False,
        name=args.name,
    )
    main()