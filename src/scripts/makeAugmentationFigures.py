import os
import numpy as np
import os
import wandb
from monai.data import NumpyReader
from monai.transforms import *
from matplotlib import pyplot as plt
imageList = ["/psi/home/guthru_d/master-thesis/data/processed/NSCLC/3e111204-3e97-43fe-a310-f1ed4c261a03/image_3.npz",
            "/psi/home/guthru_d/master-thesis/data/processed/HNC/769089b4-4bd5-4b14-9843-45a7feedee96/image_3.npz",
            "/psi/home/guthru_d/master-thesis/data/processed/CTData512/0eb19902-6e36-45d1-bb03-d65b10c6b7f2/image_0.npz",
            ]
loader = LoadImage(reader='numpyreader',
        dtype=np.float32,
        ensure_channel_first=False)
orienter=Orientation(axcodes="RAS")
scaler=ScaleIntensityRange(
      a_min=-180,
      a_max=200,
      b_min=0.0,
      b_max=1.0,
      clip=True,
   )

augmentations = [
   RandAffine(
      rotate_range=0.1,
      prob=1,
      mode="bilinear"
   ),
   RandAffine(
      shear_range=0.1,
      prob=1,
      mode="bilinear"

   ),
   RandAffine(
      scale_range=0.1,
      prob=1,
      mode="bilinear"

      ),
   Rand3DElastic(
      magnitude_range=(149,150),
      sigma_range=[4,7],
      prob=1,
      mode="bilinear"

   ),
   Rand3DElastic(
      magnitude_range=(50,150),
      sigma_range=[4,7],
      rotate_range=0.1,
      shear_range=0.1,
      scale_range=0.1,
      prob=1,

   ),]



# augmentations = [
#     RandHistogramShift(
#         num_control_points=5,
#         prob=1,
#         ),
#     RandGaussianNoise(
#         std=0.02,
#         prob=1,
#         ),
#     RandGridDistortion(
#         distort_limit=(-0.2,0.2),
#         num_cells=5,
#         mode="bilinear",
#         prob=1,
#         ),
#     RandGaussianSmooth(
#         prob=1,
#     ),
#     RandCoarseShuffle(
#     holes=50,
#     spatial_size=50,
#     prob=1,
#     ),]


images = []
for img_path in imageList:
   image = loader(img_path)[0]
   image = orienter(image)
   image = scaler(image)
   images.append(image)
index=100
n_augs = len(augmentations)
fig, axs = plt.subplots(len(images),n_augs+1, figsize=(20,10))

for j in range(len(images)):
    for i in range(n_augs+1):
        image= images[j]

        if i==0:
            axs[j,i].imshow(image[0,:,:,index],cmap="gray")
        else:
            augmented_image = augmentations[i-1](image)
            axs[j,i].imshow(augmented_image[0,:,:,index],cmap="gray")
            del augmented_image,image
        axs[j,i].set_xticks([])
        axs[j,i].set_yticks([])
plt.tight_layout()
plt.gca().set_axis_off()
plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
            hspace = 0, wspace = 0)
plt.margins(0,0)
plt.gca().xaxis.set_major_locator(plt.NullLocator())
plt.gca().yaxis.set_major_locator(plt.NullLocator())
plt.savefig(fname=f"Augmentations_3.png",dpi=200,bbox_inches='tight',
    pad_inches = 0)






print("Finished!")