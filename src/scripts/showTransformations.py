#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 21.09.2022
# =============================================================================
"""
Filename:           train_model.py
Description:        Train segmentation model or do hyperparmetersweeps
Main Libs:          Pytorch, Wandb,monai, smoldeCodebase
Long Description:   First configurations need to be initalized. There are multiple
                    ways to initalize a config.
                    1.  New config file (see readme or go to configs folder) which 
                        can be passed to argparser e.g --config CTData512/main/default
                    2.  Select default yaml file as basis and make your own new config
                        file test.yaml and only adust the parameters you want. All
                        parameters that are passed directly via argparser --... over
                        write the parameters of the default yaml file
                    3.  To initalize a hyperparmeter sweep the yaml file needs to be
                        setup a bit differently (see readme or configs folder). 
                        Provide within the sweep.yaml file a default yaml via config
                        value and change the parameters you wish the run 
                        "wandb sweep path/to/sweep.yaml".
                    4.  You may also just change single parameters via the argparser
                        arguments e.g python path/to/train_model.py --model UNetPlusPlus
State:              dev
"""
# =============================================================================
# Imports
# =============================================================================
import dotenv
from config import *
import wandb
WANDB_DIR = WANDB_DIR
WANDB_CACHE_DIR = WANDB_CACHE_DIR

from torch import autograd
from variables import *
from src.utils.general import *
from src.utils.dataset import DatasetWrapper
from src.utils.model import ModelWrapper
from src.utils.compile import CompileWrapper
from src.utils.trainer import TrainerWrapper
from monai.data import set_track_meta
from monai.utils import set_determinism

def main():
    # Keeping all runs comparable
    set_determinism(seed=0)
    dsWrapper = DatasetWrapper(config)
    trainDS = dsWrapper.getDataset(TRAIN)
    trainLoader= dsWrapper.getDataLoader(trainDS,TRAIN)
    for batch_data in trainLoader:
        step += 1
        inputs, labels = (
        batch_data[IMAGE],
        batch_data[LABEL],
            )
        
if __name__ == '__main__':
    #loads env. variables
    dotenv.load_dotenv(DOTENV_PATH)

    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type = str, default = 'ResnetSTCNet', help="Options: UNet, BasicUNet, smoldeUNet, SwinUNETR, VNet || "
                                                                      "Desc: To specifiy modelArguments consider --YourModel")
    parser.add_argument('--config', type = str, default = 'CTData512/investigations/stcn', help="Options: consider configsPath || "
                                                                                                   "Desc: Path relative to 'master-thesis/configs', e.g --config CTData512/main/default without yaml")
    parser.add_argument('--overwrite', choices=('True','False'), default = False, help="Desc: Only relevant if resume, you can adjust parameters of resumed, Run through argparser or a new config yaml file")
    parser.add_argument('--folderName', type = str, default = 'tmp', help="Desc: Your Session folderName, for folder here and wandbRunName tmp is default but will only exist once, e.g. replaced if run two times")
    parser.add_argument('--mode', type = str, default = 'train', help="Options: train, evaluate, resume, sweep || "
                                                                      "Desc: What is the purpose of this run, sweep, will not save anything but logs")
    parser.add_argument('--checkpoint', type = str, default = 'last', help="Options: last, best")
    parser.add_argument('--train', nargs="+", type = asLiteral,default=None, help="Options: [TrainBatchSize, SubCropNumber, ROI, Spacing] || "
                                                                                  "Desc: [Int, Int, [H,W,D], [float,float,float]], Where ROI is the cubeSize and spacing defines if you want to interpolate")
    parser.add_argument('--generalData', type = asLiteral, nargs="+", default=None, help="E.g: [CTData512,  smolde,   singleChannel] || "
                                                                                         "Desc:[Dataset,    labeling, savedAs]")
    parser.add_argument('--dataSplit', type = asLiteral, nargs="+", default=None, help="E.g: [0.8,0.1,0.1] || " 
                                                                                        "Desc: [TrainSplit,ValSplit,TestSplit]")
    args = getArgsFromParser(parser)
    args = makeSessionFolder(args)
    wandbRun = initiateWandb(args)
    config = wandbRun.config
    main()
    


