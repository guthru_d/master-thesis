import wandb
import numpy as np
import dotenv
from src.utils.general import openJson
from src.utils.labels import getLabelsToSegment
import hydra
from monai.metrics import *
from src.utils.metrics import *
from monai.transforms import *
from config import *
from omegaconf import OmegaConf,open_dict
from tqdm import tqdm
from skimage.measure import label
from src.scripts.PPEvaluator import openNPZ
from skimage.segmentation import find_boundaries
from monai.visualize import blend_images, matshow3d, plot_2d_or_3d_image
from matplotlib import pyplot as plt
from matplotlib import gridspec
from tensorboardX import SummaryWriter
from monai.utils.type_conversion import convert_to_dst_type
import matplotlib
from matplotlib.lines import Line2D
from collections import Counter
CONTOUR_SAVE_PATH ="/psi/home/guthru_d/master-thesis/reports/figures/contour_plots"

default_config = {
    'run_ids': ['35q3afgo'],
    'names': ['STCN-Real'],
    'eval_modes': ['first','previous'],
    'scans': [1,2,3,4,5,6,7,8],
    'indexes': np.linspace(20,180,40).astype(int).tolist(),
    'patient': "fcf4cc92-c31e-40ab-8246-8e4a64721d1c",
    'keeprs': [1,2,3,4],
     }

def getPred(run_id,eval_mode,scan,patient,keeprs):
    data_path = [x[0] for x in os.walk(PREDICTIONS_DIR) if run_id in x[0] and x[0].endswith(eval_mode) and patient in x[0]][0]
    # patient = data_path.split("/")[-4]
    # labels = openJson(f"{data_path.split('PREDS')[0]}/labels.json")
    pred_path = f"{data_path}/scan_{scan}/preds.npz"
    if os.path.exists(pred_path):
        preds = openNPZ(pred_path)
        preds = scanToSingleChannel(preds,keeprs)
    else:
        preds = None
    return preds

def scanToSingleChannel(labels,keeprs):
    makeSingle = AsDiscrete(argmax=True)
    labelFilterer = LabelFilter(applied_labels=keeprs)
    singleChannel = makeSingle(labels[0])
    singleChannel = labelFilterer(singleChannel)
    contours = np.expand_dims(find_boundaries(singleChannel[0].astype(np.uint8),connectivity=3,mode="inner",background=0),0)
    return contours

def getResultsDict(run):
    results = {}
    scans = run.config["scans"]
    patient = run.config["patient"]
    for scan in tqdm(scans):
        results[scan]={}
        for r, run_id in enumerate(run.config["run_ids"]):
            results[scan][run_id]={}
            for eval_mode in run.config["eval_modes"]:   
                results[scan][run_id][eval_mode]=getPred(run_id,eval_mode,scan,patient,run.config["keeprs"]) 
                
        results[scan]["gt"]=scanToSingleChannel(openNPZ(f"{PREDICTIONS_DIR}/{patient}/GTS/scan_{scan}/gts.npz"),run.config["keeprs"])
    results["cts"]=openNPZ(f"{PREDICTIONS_DIR}/{patient}/CTS/cts.npz")
    return results
def get_label_rgb(cmap,label):
    label=rescale_array(label)
    _cmap = plt.get_cmap(cmap)
    label_np, *_ = convert_data_type(label, np.ndarray)
    label_rgb_np = _cmap(label_np[0])
    label_rgb_np = np.moveaxis(label_rgb_np, -1, 0)[:3]
    label_rgb, *_ = convert_to_dst_type(label_rgb_np, label)
    return label_rgb


def blender(ct,labels,num,cmap="hsv",alpha=1):
    blendeScan = blend_images(image=ct, label=labels, alpha=alpha,rescale_arrays=True,cmap=cmap)
    # c =  Counter(get_label_rgb(cmap, labels).numpy().flatten().tolist())
    color_codes=[]
    _cmap=plt.get_cmap(cmap)
    for i in rescale_array(np.arange(num)):
        rgba = _cmap(i)
        color_codes.append(matplotlib.colors.rgb2hex(rgba))
    # common = c.most_common()
    # for i in range(num):
    #     rgb=[]
    #     for j in range(3):
    #         rgb.append(common[j+(i*3)][0])
    #     print(rgb)
    #     color_codes.append(matplotlib.colors.rgb2hex(rgb))
    return blendeScan,color_codes[1:]
def getBlendedResults(results,names):
    makeSingle = AsDiscrete(argmax=True)
    blended = {}
    legends = {}
    for s, scan in enumerate([*results.keys()][:-1]):
        legends[scan]={}
        legend = []
        blendList = [np.zeros(results[scan]["gt"].shape)]
        blendList.append(results[scan]["gt"])
        legend.append("Groundtruth")
        for r, run_id in enumerate(run.config["run_ids"]):
            for eval_mode in [*results[scan][run_id].keys()]:
                data = results[scan][run_id][eval_mode]
                if data is not None:
                    blendList.append(data)
                    legend.append(f"{names[r]}-{eval_mode}")
        print(len(blendList))
        labels = makeSingle(np.concatenate(blendList,axis=0).astype(np.int32))
        legends[scan]["names"]=legend
        ct = np.expand_dims(results["cts"][0,s],0)
        blended[scan],color_codes=blender(ct,labels,len(blendList))
        legends[scan]["cc"]=color_codes

    return blended,legends

def threshold_at_one(x):
    return x > 0.01

def plotter(blended_results,indexes,sw,legends,k_divisible=64):
    cF = CropForeground(select_fn=threshold_at_one,k_divisible=k_divisible)
    nrow=1
    ncol=len(blended_results.keys())

    for i, index in enumerate(indexes):
        fig = plt.figure(figsize=(ncol*8, nrow*8),dpi=150) 
        gs = gridspec.GridSpec(nrow, ncol,
                wspace=0.0, hspace=0.0, 
                top=1.-0.5/(nrow+1), bottom=0.5/(nrow+1), 
                left=0.5/(ncol+1), right=1-0.5/(ncol+1))
    
    
        plt.subplot(gs[0,0]).set_ylabel(f"Index={index}")
        for j,scan in enumerate([*legends.keys()]):
            custom_lines = []
            for color in legends[scan]["cc"]:
                custom_lines.append(Line2D([0], [0], color=color, lw=4))
            slice = blended_results[scan][:,:,:,index]
            plt.subplot(gs[0,j]).set_title(f"Scan {scan}")
            plt.subplot(gs[0,j]).imshow(torch.moveaxis(torch.from_numpy(slice[:, :, :]), 0, -1))
            plt.subplot(gs[0,j]).set_yticks([])
            plt.subplot(gs[0,j]).set_xticks([])
            leg=plt.legend(custom_lines, legends[scan]['names'],framealpha=0)
            for i,text in enumerate(leg.get_texts()):
                text.set_color(legends[scan]["cc"][i])
        sw.add_figure(tag="something",figure=fig,global_step=index)

    

def main():
    results = getResultsDict(run)
    blended_results,legends = getBlendedResults(results,run.config["names"])
    sw_path = f"{CONTOUR_SAVE_PATH}/{run.config['patient']}"
    if not os.path.exists(sw_path):
        os.mkdir(sw_path)
    sw = SummaryWriter(log_dir=sw_path)
    plotter(blended_results,run.config["indexes"],sw,legends)
    print("hi")




        

if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    run = wandb.init(config=default_config,
                     name="tmp")
    main()
