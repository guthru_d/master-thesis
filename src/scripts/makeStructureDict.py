import dotenv
from config import *
from tqdm import tqdm
WANDB_DIR = WANDB_DIR
WANDB_CACHE_DIR = WANDB_CACHE_DIR
from variables import *
from src.utils.general import *
from Utils.ioUtils import*
torch.backends.cudnn.benchmark = True
from monai.data.dataset import CacheDataset,Dataset,PersistentDataset,SmartCacheDataset,CacheNTransDataset
from monai.transforms import *
from monai.data import ThreadDataLoader,DataLoader

def main():
    
    imageFileNames= [os.path.join(path,name) for path, subdirs, files in os.walk(dataPath) for name in files if IMAGE in name]
    labelFileNames = [imageFilename.replace(IMAGE,LABEL) for imageFilename in imageFileNames]
    pathList=[]
    for imf, laf in zip(imageFileNames,labelFileNames):
        pathList.append({
            IMAGE:imf,LABEL:laf
        })
    transforms=Compose([
        LoadImageD(keys=[IMAGE,LABEL]),
    ])
    dataset=Dataset(data=pathList, transform=transforms)
    dataLoader = DataLoader(
                dataset,
                batch_size=1,
                shuffle=False,
                num_workers=2,
                pin_memory=True,
            )

    structureDict = {}
    step = 0
    for batch_data in tqdm(dataLoader):
        step +=1
        _, labels = (
            batch_data[IMAGE],
            batch_data[LABEL],
        )
        availLabels = []
        for i in [*LABEL_DICT.values()]:
            if i in labels:
                availLabels.append([*LABEL_DICT.keys()][i])
        folderName = batch_data['label_meta_dict'][FILENAME_OR_OBJ][0].split("/")[-2]
        scanName = batch_data['label_meta_dict'][FILENAME_OR_OBJ][0].split("/")[-1]
        if folderName in structureDict:
            structureDict[folderName][scanName]=availLabelsString
        else:
            structureDict[folderName]={}
            structureDict[folderName][scanName]=availLabelsString
        
        
    writeJson(os.path.join(dataPath.split('/single')[0],"structureDict.json"),structureDict)

if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)

    dataPath='/asm/home/guthru_d/master-thesis/data/processed/HNC/all/singleChannel'
    labelDict=ALL_LABELS
    main()