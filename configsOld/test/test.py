import logging
import pathlib
import pprint

import omegaconf
import torch
import torchvision
import tqdm
from monai.transforms import *
import hydra
import wandb

@hydra.main(config_path="/psi/home/guthru_d/master-thesis/configs/hydra/configs/", config_name="defaults")
def run_experiment(cfg: omegaconf.DictConfig)->None:
    wandb_cfg = omegaconf.OmegaConf.to_container(
        cfg, resolve=True, throw_on_missing=True
    )
    pprint.pprint(wandb_cfg)
    wandb.init(**cfg.wandb.setup, config=wandb_cfg)
    augmentation = []

    if "augmentation" in cfg:
        for _, conf in cfg.augmentation.items():
            if "_target_" in conf:
                augmentation.append(hydra.utils.instantiate(conf))
    augis = Compose(augmentation)

    print("hoi")
if __name__ == '__main__':
    run_experiment()
    