#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu-short
#SBATCH --gpus=1
#SBATCH --job-name=test
#SBATCH --time=00:03:00
#SBATCH --cpus-per-task=4

#SBATCH --output=scratch/models/logs/$1testoutput.log
#SBATCH --error=scratch/models/logs/$1testerror.log

module load Python/3.8.12
module load cuda/11.5.1
env
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun --cpus-per-task=\$SLURM_CPUS_PER_TASK python src/scripts/test.py $1
EOT
