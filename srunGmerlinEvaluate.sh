#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu-short
#SBATCH --account=merlin
#SBATCH --gpus=1
#SBATCH --job-name=$1_$2_$3_$4Evaluate
#SBATCH --time=00:30:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=24G
#SBATCH --exclude=merlin-g-0[01-10]
#SBATCH --output=scratch/models/logs/$4_$1_$3_outputEvaluate.log
#SBATCH --error=scratch/models/logs/$4_$1_$3_errorEvaluate.log
module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun --cpus-per-task=\$SLURM_CPUS_PER_TASK python src/models/predict_model.py wandb.setup.id=$1 inference.checkpoint=$2 wandb.setup.job_type=evaluate metric=inference validation.device=cpu inference.dtype=float32 inference.eval_mode=$3 validation.overlap=$4 wandb.setup.name=$5
EOT
