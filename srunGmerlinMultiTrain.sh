#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu
#SBATCH --account=merlin
#SBATCH --gpus=2                 
#SBATCH --ntasks-per-gpu=1       
#SBATCH --cpus-per-task=6
#SBATCH --mem-per-cpu=8G
#SBATCH --constraint=gpumem_11gb
#SBATCH --job-name=$1$2
#SBATCH --time=24:00:00
#SBATCH --hint=multithread
#SBATCH --output=scratch/models/logs/output$1$2.log
#SBATCH --error=scratch/models/logs/error$1$2.log
#SBATCH --mail-user=daniel.guthruf@psi.ch
#SBATCH --mail-type=FAIL,END

module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun python src/models/train_model.py --mode $1 --folderName $2 --config $3 --model $4

EOT
