#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu-short
#SBATCH --account=merlin
#SBATCH --gpus=1
#SBATCH --constraint=gpumem_11gb
#SBATCH --job-name=Sweep$1
#SBATCH --time=02:00:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=6
#SBATCH --nodelist=merlin-g-014
#SBATCH --mem-per-cpu=12G
#SBATCH --output=scratch/models/logs/outputTrain$1$2.log
#SBATCH --error=scratch/models/logs/errorTrain$1$2.log

module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun wandb agent dguthruf/master-thesis/$1 --count $2
EOT
