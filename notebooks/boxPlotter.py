import wandb
import seaborn as sns
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from variables import *
sns.set(rc={"figure.dpi":300,})
run=wandb.init(
    resume=False,
    name="NSCLC_BAD_1c8d5b43",
)
ids = ["3m8ob9bs","1givl2ow"]
models = ["STCN"]
metrics=[HAUSDORFF_DISTANCE_METRIC,SURFACE_DICE,DICE_METRIC]
niceMetrics=['HD95','SurfaceDice','Dice']
tables=[]
smoldeResults = pd.read_pickle("/psi/home/guthru_d/master-thesis/data/external/smoldeNSCLCResults.pkl")
smoldeModels = ["Deformable registration","Patient specific segmentation"]
for id in ids:
    tables.append(wandb.use_artifact(f"run-{id}-InferenceMetrics:v0",type='run_table').get("InferenceMetrics"))

tables=[wandb.JoinedTable(tables[0],tables[1], "id")]


labels = ['heart','lung_R','lung_L','esophagus','spinal_cord','CTV']
#niceLabels = ['Eye-L','Eye-R','Parotid-L',"Parotid-R","Spinalcord","Thyroid"]
for m,metric in enumerate(metrics):
    df = pd.DataFrame()
    for t, table in enumerate(tables):
        for l,label in enumerate(labels):
            tmpdf = pd.DataFrame()
            try:
                tmpdf["value"]=table.get_column(metric+label)
                tmpdf["label"]=[label]*len(tmpdf["value"])
                tmpdf["metric"]=[niceMetrics[m]]*len(tmpdf["value"])
                tmpdf["model"]=[models[t]]*len(tmpdf["value"])
                df=df.append(tmpdf)
            except:
                print(f"{label} in {models[t]} not availabel. Filling everything NaN ")
                tmpdf["value"]=[np.nan]
                tmpdf["label"]=[np.nan]
                tmpdf["metric"]=[np.nan]
                tmpdf["model"]=[models[t]]*len(tmpdf["value"])
    tmpResults = smoldeResults[(smoldeResults['metric']==niceMetrics[m]) & (smoldeResults['label'].isin(labels)) & (smoldeResults['model'].isin(smoldeModels))]
    tmpResults.pop("patient")
    tmpResults.pop("scan")
    df=df.append(tmpResults)
    plt.figure()
    s=sns.boxplot(data=df,x="label",y="value",hue="model")
    if metric==HAUSDORFF_DISTANCE_METRIC:
        plt.ylim(0, 15)
    s.set_title(metric)
    s.legend_.set_title(None)
    s.set_xlabel(None)
    s.set_ylabel(None)
    wandb.log({f"BoxPlot{metric}":wandb.Image(s)})
    plt.show()

print("Done")
