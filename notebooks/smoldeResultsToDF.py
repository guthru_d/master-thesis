import numpy as np
import pandas as pd
import seaborn as sns

array_path= "/psi/home/guthru_d/master-thesis/data/external/allSegmentationMetricsUpdate.npy"
data = np.load(array_path)

metrics = ["IoU", "Dice", "SurfaceDice", "MSD", "HD", "HD95"]
models = [
    "Rigid registration",
    "Pretrained segmentation",
    "Commercial segmentation",
    "Patient specific segmentation",
    "Deformable registration",
]
labels = [
    "spinal_cord",
    "thyroid",
    "brainstem",
    "chiasm",
    "eye_L",
    "eye_R",
    "parotid_L",
    "parotid_R",
    "optic_nerve_L",
    "optic_nerve_R",
    "cochlea_L",
    "cochlea_R",
    "hippocampus_L",
    "hippocampus_R",
    "lacrimal_gland_L",
    "lacrimal_gland_R","CTV_5400", "CTV_7200"]
patients= [2,3,5,6,7]
scans = [0,1,2,3,4,5,6,7]
df = pd.DataFrame()
for me, metric in enumerate(metrics):
    for mo, model in enumerate(models):
        for p, patient in enumerate(patients):
            for s, scan in enumerate(scans):
                tmpdf = pd.DataFrame()
                for l, label in enumerate(labels):
                    tmpdf['value']=[data[me,mo,p,s,l]]
                    tmpdf['metric']=[metric]*len(tmpdf['value'])
                    tmpdf['model']=[model]*len(tmpdf['value'])
                    tmpdf['patient']=[patient]*len(tmpdf['value'])
                    tmpdf['scan']=[scan]*len(tmpdf['value'])
                    tmpdf['label']=[label]*len(tmpdf['value'])
                    df=df.append(tmpdf)

print(df.head())
