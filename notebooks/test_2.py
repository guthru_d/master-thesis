import os
import pickle
from src.utils.general import openPickle
import numpy as np
from skimage.measure import label
from skimage.segmentation import find_boundaries



def floodFill(mask, percentage=0.6):
    labels = label(mask)
    binSizes = np.bincount(labels.flat)[1:]
    indices = np.argsort(binSizes)[::-1]
    if len(indices) == 0:
        return mask
    maskTmp = labels == indices[0] + 1
    mask = np.copy(maskTmp)
    if len(indices) < 2:
        return mask
    sizeInitial = binSizes[indices[0]]
    i = 1
    maskTmp = labels == indices[i] + 1
    while binSizes[indices[i]] > percentage * sizeInitial:
        mask[maskTmp] = 1
        if len(indices) <= i + 1:
            return mask
        i += 1
        maskTmp = labels == indices[i] + 1
    return mask

def postProcessBinaryMaps(binaryMap):
    newMaps = []
    for i in range(binaryMap.shape[1]):
        newMaps.append(
            postProcessBinaryMap((binaryMap[0,i]))
        )
    binaryMap = np.stack(newMaps, 1)
    return binaryMap

def postProcessBinaryMap(binaryMap):
    binaryMap = np.expand_dims(floodFill(binaryMap), 0)

    return binaryMap
def openNPZ(dataPath):
    with np.load(dataPath) as data:
        d = data["arr_0"]
    return d

p = "/psi/home/guthru_d/master-thesis/data/predictions/1c8d5b43-8db7-47f9-8c59-877d1e09791c/PREDS/e9l55irk/first/scan_2/preds.npz"
d = openNPZ(p).astype(np.uint8)


a = postProcessBinaryMaps(d)
print("Finished!")