import dotenv
from config import DOTENV_PATH
import wandb
import os
from variables import *
from src.utils.general import *
from omegaconf import OmegaConf,open_dict
from variables import *
from config import *
from src.utils.evaluator import modelInference

@hydra.main(config_path="/psi/home/guthru_d/master-thesis/configs/", config_name="defaults")
def main(cfg: omegaconf.DictConfig)->None:
    metrics= [*hydra.utils.instantiate(cfg.metric).values()]
    print(f"Evaluate Model: {cfg.model.name}")
    run=wandb.init(**cfg.wandb.setup)

    cfg = omegaconf.OmegaConf.create(dict(run.config))
    OmegaConf.set_struct(cfg, True)
    with open_dict(cfg):
        cfg.inference = {"eval_mode":"first","checkpoint":"last","dtype":"float16"}
        cfg.validation = {"batch_size": 1, "window_batch_size": 1, "device": "cpu" ,"eval_mode": "first", "overlap": 0.25, "connectivity": 3, "num_components": 2,
        "crop_size_x":256, "crop_size_y":256, "crop_size_z":96}
        cfg.metric = {}
    metricDict, dictTable = modelInference(cfg)
    run.log(metricDict)
    run.log({"Inference"+"Metrics":dictTable})
    run.finish()
if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    main()





