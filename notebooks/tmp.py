import os
import shutil
import torch
from monai.transforms import LoadImage
from tqdm import tqdm
import pandas as pd
from collections import Counter
dataPath = "/psi/home/guthru_d/master-thesis/data/processed/NSCLC"
savePath = "/psi/home/guthru_d/master-thesis/reports/csv/oar_count.xlsx"
listoflistsoflabels = []
for patient in tqdm(os.listdir(dataPath)):
    patientPath = os.path.join(dataPath,patient)
    listImages = [os.path.join(patientPath,image) for image in os.listdir(patientPath) if 'image' in image]
    for image in listImages:
        labelfolder = os.path.join(patientPath,image.replace("image","label").replace(".npz",""))
        listoflistsoflabels.extend([label.replace(".npz","").replace("_L"," left").replace("_R"," right").replace("_"," ").capitalize() for label in os.listdir(labelfolder)])

NSCLC = ["Lung left","Lung right", "Heart", "Esophagus", "Spinal cord", "Gtv", "Ctv"]        
HNC = ["Eye left","Eye right", "Parotid left","Parotid right", "Thyroid", "Spinal cord", "Ctv", "Gtv"]        

c = Counter(listoflistsoflabels)
df = pd.DataFrame(c.most_common(),columns=["OAR","Annotated scans"])
df_nsclc = df[df["OAR"].isin(NSCLC)]
df_hnc = df[df["OAR"].isin(HNC)]
with pd.ExcelWriter(savePath) as writer:
    df.to_excel(writer,sheet_name="All")
    df_nsclc.to_excel(writer,sheet_name="NSCLC")
    df_hnc.to_excel(writer,sheet_name="HNC")

a = 4
