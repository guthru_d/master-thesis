import wandb
import numpy as np
import dotenv
from src.utils.general import openJson
from src.utils.labels import getLabelsToSegment
import hydra
from monai.metrics import *
from src.utils.metrics import *
from monai.transforms import *
from config import *
from omegaconf import OmegaConf,open_dict
from tqdm import tqdm
from skimage.measure import label

default_config = {
    'run_id': '1w2u21et',
    'eval_mode': 'first',
    'num_components': 1,
    'connectivity': 3,
    'min_size': None,
    'fill_holes': True,
    'smolde': False,
     }



def floodFill(mask, percentage=0.6):
    labels = label(mask)
    binSizes = np.bincount(labels.flat)[1:]
    indices = np.argsort(binSizes)[::-1]
    if len(indices) == 0:
        return mask
    maskTmp = labels == indices[0] + 1
    mask = np.copy(maskTmp)
    if len(indices) < 2:
        return mask
    sizeInitial = binSizes[indices[0]]
    i = 1
    maskTmp = labels == indices[i] + 1
    while binSizes[indices[i]] > percentage * sizeInitial:
        mask[maskTmp] = 1
        if len(indices) <= i + 1:
            return mask
        i += 1
        maskTmp = labels == indices[i] + 1
    return mask

def postProcessBinaryMaps(binaryMap):
    newMaps = []
    for i in range(binaryMap.shape[1]):
        newMaps.append(
            postProcessBinaryMap((binaryMap[0,i]))
        )
    binaryMap = np.stack(newMaps, 1)
    return binaryMap

def postProcessBinaryMap(binaryMap):
    binaryMap = np.expand_dims(floodFill(binaryMap), 0)

    return binaryMap

def postProcessing(num_componets=None,connectivity=None,min_size=None,fill_holes=None):
    PPList=[]
    if num_componets:
        PPList.append(KeepLargestConnectedComponent(num_components=num_componets,connectivity=connectivity))
    elif min_size:
        PPList.append(RemoveSmallObjects(min_size=min_size,connectivity=connectivity))
    if fill_holes:
        PPList.append(FillHoles(connectivity=connectivity))
    PPList.append(ToTensor(dtype=torch.bool))
    return Compose(PPList)


def inferencePostProcessor(results,post_processing,smolde=False):
    for patient in [*results.keys()]:
        for scan in tqdm([*results[patient]["pred"].keys()]):
            if smolde:
                results[patient]["pred"][scan]=torch.from_numpy(postProcessBinaryMaps(results[patient]["pred"][scan]))
            else:
                results[patient]["pred"][scan]=post_processing(results[patient]["pred"][scan][0]).unsqueeze(0)
    return results

def openNPZ(dataPath):
    with np.load(dataPath) as data:
        d = data["arr_0"]
    return d


def getRawResults(data_path):
    results={}
    patient = data_path.split("/")[-4]
    labels = openJson(f"{data_path.split('PREDS')[0]}/labels.json")
    results[patient]={}
    results[patient]["pred"]={}
    results[patient]["gt"]={}
    results[patient]["missing"]={}
    for scan in os.listdir(data_path):
        preds = openNPZ(f"{data_path}/{scan}/preds.npz")
        gtPath = f"{data_path.split('PREDS')[0]}/GTS/{scan}/gts.npz"
        gts   = openNPZ(gtPath)
        results[patient]["pred"][scan]=preds
        results[patient]["gt"][scan]=torch.from_numpy(gts.astype(np.bool_))
    results[patient]["missing"] = labels["missing"]
    wantedLabels  = labels["wanted"]
    return results, wantedLabels

def initateMetrics():
    include_background = False
    reduction = "none"
    get_not_nans= False
    voxel_spacing=[0.9765625, 0.9765625, 2.0]
    tolerance = 2
    percentile= 95

    metrics = [DiceMetric(include_background=include_background,
                          reduction=reduction,
                          get_not_nans=get_not_nans),
            #    SurfaceDiceSmolde(include_background=include_background,
            #                      get_not_nans=get_not_nans,
            #                      reduction=reduction,
            #                      voxel_spacing=voxel_spacing,
            #                      tolerance=tolerance),
               robustHausdorffSmolde(include_background=include_background,
                                     get_not_nans=get_not_nans,
                                     reduction=reduction,
                                     voxel_spacing=voxel_spacing,
                                     percentile=percentile)
    ]

    return metrics
def calculateMetrics(metrics,results,labels):
    columns = ['id','scan']
    for metric in metrics:
        metricName=metric.__class__.__name__
        columns.append("Mean"+metricName)
        columns.extend([metricName+label for label in labels])
    dictTable = wandb.Table(columns=columns)

    for patient in [*results.keys()]:
        if not results[patient]: continue
        for scan_key in tqdm([*results[patient]["pred"].keys()]):
            metricValuesList = [patient,scan_key[-1]]
            for metric in metrics:
                metric(y_pred   = results[patient]["pred"][scan_key],
                       y        = results[patient]["gt"][scan_key],
                )
                metricValues = metric.aggregate()
                metricValues = metricValues[0].cpu().numpy().tolist()
                if len(results[patient]["missing"])>0:
                    for missingLabel in results[patient]["missing"]:
                        metricValues.insert(labels.index(missingLabel),np.nan)
                metricValuesList.append(np.nanmean(metricValues))
                metricValuesList.extend(metricValues)
                metric.reset()
            dictTable.add_data(*metricValuesList)
    return dictTable

def main():
    print(run.config)
    paths = [x[0] for x in os.walk(PREDICTIONS_DIR) if run.config["run_id"] in x[0] and x[0].endswith(run.config["eval_mode"])]
    print(paths)
    metrics= initateMetrics()
    meanDice=[]
    meanHaus=[]
    for p in paths:
        results,allWantedLabels = getRawResults(p) 
        post_processing = postProcessing(num_componets=run.config["num_components"],
                                         connectivity=run.config["connectivity"],
                                         fill_holes=run.config["fill_holes"],
                                         min_size=run.config["min_size"])
        PPResults=inferencePostProcessor(results=results,post_processing=post_processing,smolde=run.config.smolde)
        dictTable = calculateMetrics(metrics,PPResults,allWantedLabels)
        metricDictionary={label:np.nanmean(dictTable.get_column(label)) for label in dictTable.columns[2:]}
        wandb.log({f"Metrics_{p.split('/')[-4]}":dictTable})
        meanDice.append(metricDictionary[[*metricDictionary.keys()][0]])
        meanHaus.append(metricDictionary[[*metricDictionary.keys()][len(allWantedLabels)+1]])
    wandb.log({f"MeanDice": np.nanmean(meanDice)})
    wandb.log({f"MeanHaus": np.nanmean(meanHaus)})

if __name__ == '__main__':
    dotenv.load_dotenv(DOTENV_PATH)
    run = wandb.init(config=default_config)
    main()
