import os
import shutil
import torch
from monai.transforms import LoadImage
from tqdm import tqdm
dataPath = "/psi/home/guthru_d/master-thesis/data/processed/CTData512"
savePath = "/psi/home/guthru_d/master-thesis/data/processed/SCT"
loader=LoadImage(reader='numpyreader')
def common_elements(lists):
  # Convert all the lists to sets
  sets = [set(lst) for lst in lists]
  # Take the intersection of all the sets
  common = sets[0]
  for s in sets[1:]:
    common = common.intersection(s)
  # Return the common elements as a list
  return list(common)

for patient in tqdm(os.listdir(dataPath)):
    patientPath = os.path.join(dataPath,patient)
    newPatientPath = os.path.join(savePath,patient)
    listImages = [os.path.join(patientPath,image) for image in os.listdir(patientPath) if 'image' in image]
    listoflistsoflabels = []
    for image in listImages:
        labelfolder = os.path.join(patientPath,image.replace("image","label").replace(".npz",""))
        listoflistsoflabels.append([label for label in os.listdir(labelfolder)])
    if len(listImages)>1:
        images = [loader(i)[0] for i in listImages]
        Trues = []
        for i in range(len(images)-1):
            print(images[i].shape)
            print(images[i+1].shape)
            print("NEXT")
            Trues.append(images[i].shape==images[i+1].shape)
        if all(Trues):
            shutil.copytree(patientPath, newPatientPath)
            goodLabels = common_elements(listoflistsoflabels)
            listImages = [image for image in os.listdir(newPatientPath) if 'image' in image]
            for image in listImages:
                labelfolder = os.path.join(newPatientPath,image.replace("image","label").replace(".npz",""))
                for label in os.listdir(labelfolder):
                    if label in goodLabels:
                        pass
                    else:
                        os.remove(os.path.join(labelfolder,label))
            
            
        print("BIIIIG NEEEEEEEEEEEEEEXT")
