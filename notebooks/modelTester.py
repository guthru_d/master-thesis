from torchsummary import summary
import torch
from variables import *
from src.stm.stm.model import STM
from src.stm.stm.efficientSTM import EfficientSTM
from src.stm.stcn.efficientSTCN import EfficientSTCN
from DL.Model.unet3d.model import UNet3D
from monai.networks import nets
from src.stm.stcn.resnetSTCN import ResnetSTCN

# stm = STM()
# stm.to(device='cuda:0')
# summary(stm,[(1,1,128,128,64), (1,1,2,128,128,64)], dtypes=[torch.float, torch.float],batch_dim=None)
# print(
#     f"in_channels={1}\n",
#     f"out_channels={2}\n",
#     f"f_maps={32}\n",
#     f"layer_order=gcr\n",
#     f"num_groups={8}\n",
#     f"conv_padding=same\n",
#     f"conv_kernel_size={3}\n",
#     f"pool_kernel_size={2}\n",
# )
# model = UNet3D(
#     in_channels=1,
#     out_channels=2,
#     f_maps=32,
#     layer_order="gcr",
#     num_groups=8,
#     conv_padding="same",
#     conv_kernel_size=3,
#     pool_kernel_size=2,
# )
# model = getattr(nets,UNET)(
#             spatial_dims=3,
#             in_channels=1,
#             out_channels=2,
#             channels = (32,64,128,256),
#             strides= (2,2,2)
#             )

model = EfficientSTCN(in_channels=1,
                   out_channels=1,
                   decoder_channels=(256,128,64,32),
                   backbone="efficientnet-b0",
)
model.to(device='cuda:0')
summary(model,(1,4,64,64,64), batch_dim=None)
