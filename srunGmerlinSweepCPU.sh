#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=merlin6
#SBATCH --partition=hourly
#SBATCH --account=merlin
#SBATCH --job-name=Sweep$1
#SBATCH --time=01:00:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=6
#SBATCH --mem-per-cpu=8G
#SBATCH --output=scratch/models/logs/outputTrain$1$2.log
#SBATCH --error=scratch/models/logs/errorTrain$1$2.log

module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun --cpus-per-task=\$SLURM_CPUS_PER_TASK wandb agent dguthruf/master-thesis/$1 --count $2
EOT
