import os



#PATHS
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DOTENV_PATH = os.path.join(ROOT_DIR,'.env')
CT_DATA_DIR = os.path.join(ROOT_DIR,'data/processed/')
SCRATCH_DIR = os.path.join(ROOT_DIR,'scratch')
MODEL_DIR = os.path.join(SCRATCH_DIR,'models')
MODEL_CONFIG_DIR = os.path.join(ROOT_DIR,'configs')
DEFAULT_PARAMETER_DICT = os.path.join(MODEL_CONFIG_DIR,"defaultParameters.yaml")
CACHE_DIR = os.path.join(SCRATCH_DIR,'cache')
DATA_DIR = os.path.join(ROOT_DIR,'data')
DATA_PROCESSED_DIR = os.path.join(DATA_DIR,'processed')
RAW_DATA_DIR = os.path.join(DATA_DIR,'raw')
PROFILER_PATH = os.path.join(DATA_DIR,'profiler')
WEIGHT_DIR = os.path.join(ROOT_DIR,'weights')
FILES_DIR = os.path.join(ROOT_DIR,'files')
PID_CONVERSION_FILE = os.path.join(FILES_DIR,'pidEncoding.json')
WANDB_DIR = os.path.join(SCRATCH_DIR,'wandb')
WANDB_CACHE_DIR = os.path.join(SCRATCH_DIR,'wandb')
RESULTS_DIR = os.path.join(ROOT_DIR,'results')
PREDICTIONS_DIR = os.path.join(DATA_DIR,'predictions')



# DATA_DICTS
LABEL_DICT={
            'background':0,
            'brainstem':1,
            'chiasm':2,
            'cochlea_L':3,
            'cochlea_R':4,
            'esophagus':5,
            'eye_L':6,
            'eye_R':7,
            'heart':8,
            'hippocampus_R':9,
            'hippocampus_L':10,
            'lacrimal_gland_L':11,
            'lacrimal_gland_R':12,
            'lung_L':13,
            'lung_R':14,
            'optic_nerve_L':15,
            'optic_nerve_R':16,
            'parotid_L':17,
            'parotid_R':18,
            'spinal_cord':19,
            'thyroid':20,
            'liver':21,
            'PTV':22,
            'kidney_L':23,
            'kidney_R':24,
            'lense_L':25,
            'lense_R':26,
            'retina_L':27,
            'retina_R':28,
            'macula_L':29,
            'macula_R':30,
            'brainstem_center':31,
            'tmj_L':32,
            'tmj_R':33,
            'temporal_lobe_L':34,
            'temporal_lobe_R':35,
            'pituitary_gland':36,
            'oral_cavity':37,
            'mandibula':38,
            'larynx':39,
            'pharynx_constrictor':40,
            'submandibular_gland_L':41,
            'submandibular_gland_R':42,
            'brain':43,
            'femur_head_L':44,
            'femur_head_R':45,
            'hypothalamus':46,
            'medulla':47,
            'bladder':48,
            'bowel':49,
            'rectum':50,
            'spleen':51,
            # 'breast_L':25,
            # 'breast_R':26,
            # 'spinal_canal':34,
            # 'kidneys': 22,
            # 'anal_canal':51,
            # 'anus':52,
            # 'vertebra':57,
            # 'prostate':58,
            # 'ovary_L':59,
            # 'ovary_R':60,
            # 'pancreas':61,
            # 'penis':62,
            # 'gall_bladder':63,
            # 'bowel_R':64,
            # 'bowel_L':65,
            # 'bowel_bag':66,
            # 'small_bowel':67,
            # 'large_bowel':68,

            'GTV':52,
            'CTV':53,
            'CTV_5400':54,
            'CTV_6000_correct':55,
            'CTV_7200':56,
            'TECHPTV':57,

            'cochlea':[3,4],
            'eye':[6,7],
            'hippocampus':[9,10],
            'lacrimal_gland':[11,12],
            'lung':[13,14],
            'optic_nerve':[15,16],
            'parotid':[17,18],
            'HNC': [6,7,17,18,19,20,52,53,54,56],
            'test': [6,19,18,20],
            'test2': [5,13],
            'R_HNC': [6,7,17,18,19,20],
            'All':  [1,2,3,4,5,6,7,8,9,10,
                     11,12,13,14,15,16,17,18,19,20,
                     21,22,23,24,25,26,27,28,29,30,
                     31,32,33,34,35,36,37,38,39,40,
                     41,42,43,44,45,46,47,48,49,50,
                     51,52,53,54,55,56,57],
            'NSCLC':[5,8,13,14,19,52,53,54,56],
            'eyestuff': [6,7,15,16,27,28,29,30],
            'small': [2,3,4,6,7,11,12,15,16,17,18,19,20,27,28,29,30,31,32,35,36,39,69,70,71,72,73],
            'large': [1,8,9,10,13,14,22,23,24,25,26,37,38,46,47,48,49,53,54,56,61,64,65,67,68,69,70,71,72,73,74,75],
            'R_Sweep':[5,8],
            }

REVERSED_LABEL_DICT={v: k for k, v in LABEL_DICT.items() if type(v) is int}


NSCLC_PATIENTS = ["Patient_1","Patient_3","Patient_5","Patient_9","Patient_13"]
HNC_PATIENTS = ["P2","P3","P5","P6","P7"]