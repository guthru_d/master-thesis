#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --cluster=gmerlin6
#SBATCH --partition=gpu
#SBATCH --account=merlin
#SBATCH --gpus=1
#SBATCH --constraint=gpumem_11gb
#SBATCH --job-name=Train$1
#SBATCH --time=96:00:00
#SBATCH --hint=multithread
#SBATCH --cpus-per-task=3
#SBATCH --exclude=merlin-g-0[01-10]
#SBATCH --mem-per-cpu=17G
#SBATCH --output=scratch/models/logs/$1_output.log
#SBATCH --error=scratch/models/logs/$1_error.log
env
module load Python/3.8.12
module load cuda/11.5.1
source /data/user/guthru_d/venvs/env38mt/bin/activate
srun --cpus-per-task=\$SLURM_CPUS_PER_TASK python src/models/train_model.py wandb.setup.job_type=finetune wandb.setup.name=$1 dataset=$2 model=stcn_base paths.base=/psi/home/guthru_d/master-thesis/scratch/models/stcn_base/synthetic/train_Balanced-HNC/1dud4ort_last.pt dataset.train_labels=$3 optimiser.lr=0.000008 train.interval=400 train.epochs=8000
EOT
