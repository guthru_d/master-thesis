#GENERAL
IMAGE = "image"
LABEL = "label"
IMAGE_0 = "image_0"
IMAGE_1 = "image_1"
LABEL_0 = "label_0"
LABEL_1 = "label_1"
STRUCTURES = "structures"
RESUME = 'resume'
EVALUATE = 'evaluate'
SWEEP = 'sweep'
FINETUNING="finetune"
#DATA
TRAIN ="train"
VALIDATION ="validation"
TEST ="test"
SMOLDE ='smolde'
ALL='all'
CT_DATA_G2 = "CTDataG2"
CT_DATA_512 = "CTData512"
SCT = "SCT"
NSCLC = "NSCLC"
HNC= "HNC"
CACHE = "ache"
#MONAI DatasetTypes
REGULAR_DATASET = "RegularDataset"
CACHE_DATASET = "CacheDataset"
SMART_CACHE_DATASET = "SmartCacheDataset"
CACHE_NT_DATASET = "CacheNTDataset"
PERSISTENT_DATASET = "PersistentDataset"

#Metrics
VALIDATION_LOSS = "validationLoss"
TRAIN_DURATION = 'trainDuration'
VALIDATION_DURATION = 'validationDuration'
BEST = 'best'
LAST = 'last'
DICE_METRIC = "DiceMetric"
AVERAGE_SURFACE_DISTANCE = "SurfaceDistanceMetric"
HAUSDORFF_DISTANCE_METRIC = "HausdorffDistanceMetric"
SURFACE_DICE = "SurfaceDice"
MEAN_IOU = "MeanIoU"
#Loss
DICE_CE_LOSS = "DiceCELoss"
DICE_FOCAL_LOSS = "DiceFocalLoss"
DICE_LOSS = "DiceLoss"
SMOLDE_CELOSS = "SmoldeCELoss"
BOOTSTRAP_DICE_CE_LOSS="bootstrapDiceCELoss"
#Optimiser
ADAM_W = "AdamW"
ADAM = "Adam"

#Scheduler
PLATEAU_DECAY_SCHEDULER = "PlateauDecayScheduler"

#Train
EPOCH = 'epoch'
TRAIN_LOSS = 'trainLoss'
LEARNING_RATE = 'learningRate'
NONE = 'none'
MEAN = 'mean'

#What?
IMAGE_META_DICT ="image_meta_dict"
FILENAME_OR_OBJ="filename_or_obj"
ONE_HOT = "oneHot"
SINGLE_CHANNEL = "singleChannel"

#FileExtensions
PKL_FILE_EXTENSION = 'pkl'
NII_FILE_EXTENSION = 'nii'

#Model
SWIN_UNETR = "SwinUNETR"
BASIC_UNET = "BasicUNet"
UNET = "UNet"
VNET = "VNet"
BASIC_UNET_PLUS_PLUS = "BasicUNetPlusPlus"
SMOLDE_UNET = "smoldeUNet"
STCNET = "STCNet"
STMNET = "STMNet"
EFFICIENT_STM = "EfficientSTMNet"
FLEXIBLE_UNET = "FlexibleUNet"
EFFICIENT_STCN = "EfficientSTCNet"
RESNET_STCN = "ResnetSTCNet"